# Texte, outils et pièges

Ce chapitre traite des paramètres de caractères et de paragraphes moins courants que ceux dont il était question dans les chapitres précédents. 
Ces paramètres ont une utilisation limitée ou spécialisée et pour la plupart, ils nécessitent un examen attentif avant d’être utilisés. 

Certains semblaient raisonnables il y a des décennies, lorsque Writer est sorti pour la première fois, mais ils sont devenus obsolètes — et n’ont jamais été (pour rester poli) dans la meilleure tradition typographique. D’autres encore sont obscurs ou difficiles à utiliser et les mêmes résultats peuvent souvent être obtenus en choisissant une autre méthode plus appropriée.
 
## Les bordures

Les bordures sont les lignes qui entourent un objet des quatre côtés. Toutes les applications LibreOffice intègrent un onglet ``Bordures`` identique sur au moins un de leurs styles. Dans ``Draw`` et ``Impress``, la fonction similaire est appelée ``Ligne``.

![L’onglet des bordures dans Writer.](Captures/CH6-ongletbordures.png)

### Ajouter des bordures

Pour ajouter des bordures, voici une procédure&nbsp;:

1. Sous ``Disposition des lignes``, utilisez les préréglages ou les réglages définis par l’utilisateur pour choisir les côtés concernés. 
2. Définissez le style, la largeur et la couleur de la ligne. Dans la plupart des cas, choisissez le style le plus mince et le plus simple possible. Vous devrez peut-être choisir une ligne plus épaisse pour qu’elle soit détectable sur des imprimantes bas de gamme. 
3. Dans la section ``Remplissage``, réglez l’espacement par rapport au contenu. Un espacement généreux augmente la lisibilité. Lorsque la case ``Synchroniser`` est cochée, vous pouvez coordonner automatiquement les valeurs des différents côtés.
4. Si vous voulez un ombrage sur une partie de la bordure, définissez sa position, sa distance par rapport à la bordure et sa couleur. 

![Bordures&nbsp;: en haut un espacement à 0 pt, en bas 5 pts.](Captures/CH6-bordures.png)

<div style="attention">
Attention

L’ombre sur une bordure peut aider à séparer une image de l’arrière-plan. Cependant, si vous ne pouvez pas la justifier clairement, vous ne devriez pas l’utiliser. Les ombres étaient tellement en vogue au milieu des années 1990 qu’aujourd’hui elles peuvent paraître démodées.
</div>

### Les bordures dans les styles de caractères et de paragraphes

Beaucoup de débutants n’aiment pas les espaces vides. Pour eux, c’est un gaspillage. Par conséquent, ils sont tentés d’y remédier en plaçant des bordures autour de tout. Il faut éviter cela.

Dans les documents textuels et les diapositives de présentation, l’utilisation des bordures est limitée. Les utilisations les plus évidentes sont par exemple des rectangles vides à la place des réponses dans un quiz, ou des encarts dans des documents comme un bulletin d’information ou un manuel.

![Exemple d’encart.](Captures/CH6-encartdanscolonne.png)

## Surlignage et arrière-plan

Le surlignage est disponible dans les styles de caractères. Il est particulièrement utile pour mettre en valeur des passages dans des documents informels.

Les paramètres de l’arrière-plan sont disponibles dans les styles de paragraphes. Les options ont les mêmes caractéristiques que le surlignage, sauf que ce dernier est disponible uniquement en tant que couleur, tandis qu’un arrière-plan peut être un dégradé, une hachure ou un motif.

![Onglet Arrière-plan.](Captures/CH6-arriereplan.png)

Si vous choisissez une couleur, assurez-vous qu’elle soit disponible parmi les couleurs définies par LibreOffice. Si ce n’est pas le cas, allez dans ``Outils > Options > LibreOffice > Couleurs`` pour ajouter des couleurs personnalisées ou effectuez des ajustements depuis l’onglet ``Transparence``. De même, avant d’ajouter une image en arrière-plan, préparez ses dimensions et sa transparence dans un logiciel de graphisme tel GIMP.

![À propos de contraste.](Captures/CH6-clairsombre.png)

Pour tous les arrière-plans, la règle de base est la suivante&nbsp;: combiner un texte de couleur claire avec un fond foncé et un texte de couleur foncée avec un fond clair. Sans un contraste marqué, votre document perd sa lisibilité. Le texte foncé sur un fond clair est plus facile à lire pour beaucoup de gens, et c’est le plus couramment utilisé.

Attention aussi à ne pas utiliser de fonds avec des couleurs différentes&nbsp;: vous perdriez en lisibilité.

<div style="astuce">
Astuce

Si vous utilisez des arrière-plans et que vous destinez votre document à l’impression, assurez-vous du contraste sur une imprimante noir et blanc et sur une imprimante couleur (même si vous utilisez des nuances de gris). Les rendus ne sont pas les mêmes.
</div>

## Définir la transparence

L’onglet ``Transparence`` a été créé pour les besoins graphiques, et a été ajouté aux styles de paragraphe dans Writer à partir de la version 4.4.

On peut ajouter de la transparence lorsqu’une sélection est effectuée dans l’onglet ``Arrière-plan``. 0% annule la transparence, 100% crée une transparence totale.

En outre, vous pouvez&nbsp;:

- Créer une transparence pour ajouter rapidement (et approximativement) une autre couleur sans définir formellement cette couleur dans ``Outils > Options > LibreOffice > Couleurs``. 
- Modifier la transparence d’un arrière-plan pour améliorer le contraste.
- Créer un dégradé en utilisant des degrés de transparence.

![L’onglet pour régler la transparence.](Captures/CH6-onglettransparence.png)



## Les tabulations

Les tabulations sont des positions définies sur une ligne. Un taquet de tabulation est l’endroit où le chariot s’arrête pour éventuellement sauter ensuite à la prochaine position de tabulation.

Normalement, les positions de tabulation doivent être des multiples de l’espacement des lignes et être maintenues au minimum nécessaire. 

Les tabulations sont parfois utilisées pour créer des colonnes de texte, mais il est préférable d’utiliser un tableau. Si vous utilisez une indentation automatique de première ligne, dans la plupart des cas, vous n’aurez pas vraiment de raison d’utiliser des tabulations sauf peut-être dans la table des matières.

![L’onglet des tabulations.](Captures/CH6-tabulations.png)

Il y a quatre types de tabulations&nbsp;:

- Gauche&nbsp;: place le bord gauche de la colonne de texte à la position de tabulation, en étendant le texte vers la droite. C’est la configuration la plus couramment utilisée.
- Droite: aligne le bord droit du texte en étendant ce dernier vers la gauche. L’utilisation la plus courante consiste à placer une colonne de texte contre la marge droite.
- Centré: place le centre du texte à la position de tabulation, en l’étendant à la fois à gauche et à droite. Ce type de tabulation peut être remplacé en réglant l’alignement de la ligne sur ``Centré``.
- Décimal: place la décimale à la position droite, avec les nombres entiers et le texte à gauche. Vous pouvez définir le caractère décimal en fonction de la langue locale. Par exemple, pour indiquer l’heure en langue anglaise.

![Les 4 positions de tabulations.](Captures/CH6-tabulmontre.png)

Même si vous réduisez les tabulations au strict minimum, vous ne pouvez pas éviter les utilisations intégrées de LibreOffice. Par exemple, LibreOffice utilise des tabulations pour positionner le texte par rapport aux puces et aux nombres dans les styles de liste. Elles sont aussi utilisées dans les options de mise en page des tables des matières ou dans le cas d’un style conditionnel.

Essayez de choisir entre les tabulations ou d’autres solutions. Les cadres ou les tables avec des bordures invisibles sont souvent un choix plus stable.


| Bon usage des tabulations    |  Mauvais usage des tabulations    |
|------------------------------|-----------------------------------|
|Définissez-les dans les styles de paragraphes par défaut, ou à tout le moins, aussi haut que possible dans la hiérarchie des styles. Sinon, vous devrez définir des tabulations séparément pour chaque style de paragraphe. | Les utiliser pour indiquer le début des nouveaux paragraphes. Créez plutôt un retrait de première ligne et cochez la case ``Automatique`` dans l’onglet ``Retraits et espacement``. |
| Les positions de tabulation doivent être des multiples de l’interligne. | Les utiliser pour positionner des caractères dans un bloc de texte, comme un en-tête ou un pied de page. Utilisez plutôt un tableau avec des bordures invisibles et des largeurs de colonnes soigneusement ajustées. |
| Réglez les positions de tabulation le plus tard possible dans votre mise en page. Sinon, les modifications comme changer la police ou la taille de police vous obligera à les modifier. | Utiliser des caractères de remplissage dans les espaces vides entre les tabulations. 




## Les lettrines

Les lettrines sont des lettres agrandies qui marquent le début d’un nouveau chapitre ou d’une nouvelle section. L’onglet ``Lettrines`` pour un style de paragraphe permet en un coup de créer un bloc de texte (la lettrine) et de définir le flux de texte autour de ce bloc.

Les lettrines sont plus courantes dans les ouvrages de fiction que dans les essais, les magazines ou les ouvrages scientifiques. Les enluminures que l’on trouve dans les manuscrits du Moyen Âge font exception.

Avant de créer des lettrines, considérez les autres indicateurs d’en-tête de chapitre que vous avez dans votre mise en forme. Si votre style de première page commence plus bas que le reste de vos pages, ou si le début est marqué par un motif récurrent ou par des chiffres, alors les lettrines risquent de surcharger votre page.


![L’onglet des lettrines.](Captures/CH6-ongletLettrines.png)



Pour créer des lettrines, suivez cette procédure&nbsp;:

1. Choisissez la police de caractères pour les lettrines. Il peut s’agir de la même police que pour le corps de texte — peut-être avec une graisse différente — une police décorative, ou même un symbole.
2. Utilisez le style de caractère ``Lettrines`` pour définir la police.
3. Créez un style de paragraphe ``MesLettrines``. Très probablement, il s’agira d’un enfant de ``Corps de texte``, qui ne diffère que par le réglage de lettrine.
4. Dans l’onglet ``Lettrines``, sous ``Paramètres``, sélectionnez ``Afficher des lettrines`` de manière à ouvrir les options et l’aperçu sur la droite.
5. Sous ``Contenu > Style de caractère``, sélectionnez ``Lettrines``.
6. Définissez le ``Nombre de caractères`` ou cochez ``Mot entier`` pour définir la longueur du bloc. Vous pouvez utiliser jusqu’à 9 caractères.
7. Réglez la hauteur de votre lettrine en fonction du nombre de ``Lignes``.
8. Réglez l’espacement d’avec le texte. À moins que le bloc ne soit extrêmement grand, votre nombre magique est probablement trop élevé, alors essayez d’abord la moitié.


<div style="astuce">
Astuce

Positionner une ligne entière dans un style de police différent est un choix de mise en page très courant. Si vous voulez essayer cette solution, n’utilisez pas de lettrines mais créez un style de première ligne à la place.
</div>

![Exemples de lettrines. Le dernier exemple propose la première ligne en italique.](Captures/CH6-exemplesdelettrines.png)





## Élaboration de listes

Les listes sont assez complexes pour faire l’objet d’un type de style distinct dans Writer.  Plus précisément, dans l’onglet ``Plan & numérotation`` de la fenêtre de style de paragraphe, vous pouvez&nbsp;:

- Associer des styles de liste avec des styles de paragraphe pour qu’ils puissent être appliqués automatiquement. Le même style de liste peut être associé à plusieurs styles de paragraphe.
- Créer une liste n’utilisant qu’un seul style de paragraphe.
- Ajouter un style de paragraphe aux styles de liste par défaut. Être inclus dans les styles de liste signifie qu’un paragraphe utilisant ce style est listé dans le Navigateur, et utilisé automatiquement dans des fonctions comme les tables des matières.

<div style="astuce">
Astuce

Une liste non ordonnée est l’autre nom de la liste à puces, et une liste ordonnée est l’autre nom de la liste numérotée.
</div>

![L’onglet Plan et numérotation.](Captures/CH6-ongletPlanetnumerotation.png)

### Réinitialiser la numérotation des paragraphes

Vous n’avez pas besoin de créer un style de liste distinct pour chaque liste numérotée dans un document.

Pour redémarrer la numérotation dans n’importe quelle liste numérotée, sélectionnez ``Recommencer la numérotation`` dans le menu contextuel d’un paragraphe (clic-droit).

![Clic-droit au niveau de la numérotation d’une liste.](Captures/CH6-reprendrelanumerotation.png)



### Listes imbriquées

Une liste imbriquée —&nbsp;une liste à l’intérieur d’une liste&nbsp;— est plus courante dans un texte destiné à un affichage écran, où l’espace n’a pas d’importance et où un texte structuré avec des listes et des tableaux améliore la lisibilité.

Pour imbriquer une liste, vous avez deux possibilités.

La première consiste à créer un style de liste et à configurer deux ou plusieurs niveaux de liste avec des choix de formatage différents. L’avantage des niveaux de liste est que chaque niveau peut être mis en forme séparément, mais tous les niveaux restent connectés. Vous pouvez passer au niveau suivant en appuyant sur la touche ``Tab`` ou passer au niveau précédent avec ``Maj + Tab``.

Le volet de prévisualisation peut vous aider à configurer chaque niveau de liste, et le style de liste personnalisé est associé à un style de paragraphe à utiliser.

Pour passer à un niveau de liste inférieur tout en utilisant le style de paragraphe associé, appuyez sur la touche ``Tab`` avant d’entrer le contenu&nbsp;; pour passer à un niveau de liste supérieur, appuyez sur ``Maj + Tab``.

La seconde possibilité consiste à créer deux styles de liste distincts, puis associer chaque style de liste à un style de paragraphe particulier.

Aucune des deux possibilités n’a d’avantage sur l’autre, puisque vous avez toujours les mêmes options. Dans les deux cas, chaque liste imbriquée est généralement plus indentée que le niveau de liste au-dessus. En règle générale, chaque niveau de liste utilisera un style de puces ou un système de numérotation différent.

Les noms de style comme ``Puce 1``, ``Puce 2``, ``Numérotation 123`` vous aideront à vous rappeler la relation entre les styles de liste. Pour plus de commodité, utilisez les mêmes noms pour les styles ``Paragraphe`` et ``Liste``, car ils ne peuvent pas être confondus par le logiciel.

![Exemple de liste imbriquée.](Captures/CH6-exemplelisteimbriquee.png)


### Niveaux et styles

LibreOffice a plusieurs manières de déterminer les niveaux en utilisant les styles de paragraphes. Avec ``Outils > Numérotation des lignes``, vous pouvez choisir un style de numérotation pour chaque style de paragraphe. Vous pouvez également numéroter des titres sans utiliser  ``Outils > Numérotation des chapitres`` et, à la place, associer chaque style de titre à un style de liste distinct configuré dans la fenêtre des ``Styles``.


Pour utiliser le style de paragraphe (numéroté), appuyez sur ``Entrée + Tab`` pour ajouter un paragraphe de sous-niveau. Le paragraphe de sous-niveau utilise automatiquement le modèle de numérotation du style de liste. Pour augmenter le niveau d’un style de paragraphe, appuyez sur ``Entrée + Tab + Maj``.


![Prédéfinition des niveaux dans la fenêtre des styles de liste.](Captures/CH6list-style-plan.png)




#### Création de plan de liste avec un style de paragraphe 

Pour définir un style de paragraphe destiné à un plan de liste&nbsp;:

1. Créez un style de liste et associez-le à l’un des formats prédéfinis dans l’onglet ``Plan``.
2. Sélectionnez ou créez un style de paragraphe. Vous ne pourrez pas utiliser les styles de la rubrique 1-10. Cette restriction permet d’éviter la confusion entre votre style et les niveaux prédéfinis.
3. Dans l’onglet ``Gestionnaire`` du style de paragraphe, définissez le style à utiliser comme style de suite.
4. Affectez le style de liste au style de paragraphe à l’aide du champ ``Style de numérotation`` dans l’onglet ``Plan & numérotation`` du style de paragraphe.


#### Ajouter un style aux niveaux de plan

Un niveau de plan est un concept utilisé dans LibreOffice pour automatiser les fonctions avancées. Par exemple, les niveaux hiérarchiques déterminent les styles  affichés par défaut dans le Navigateur sous ``Titres`` et dans une table des matières.

Par défaut, les niveaux de plan sont affectés aux styles dans les niveaux 1 à 10. Le niveau de plan 1 est affecté à Titre 1, et ainsi de suite. 

Vous pouvez modifier ces relations ou ajouter un style que vous auriez créé à un niveau de plan dans le champ ``Niveau de plan`` de l’onglet ``Plan & numérotation``.

#### Sauter un paragraphe dans une liste

Dans les listes, chaque paragraphe est un élément de liste et il est donc numéroté. Cependant, vous avez parfois besoin de briser une liste avec un paragraphe non numéroté ou sans puce, de manière à donner plus de détails sur un élément de la liste ou ajouter un contenu n’entrant pas dans la liste. Cela permet aussi d’éviter qu’un élément de liste se transforme en un long paragraphe, réduisant ainsi la lisibilité.

Pour créer un style pour de tels paragraphes, vous pouvez duppliquer le style de paragraphe avec niveau de liste en lui donnant un mise en forme identique. Dans l’onglet ``Plan & numérotation``, faites ces changements&nbsp;:

- ``Niveau de plan`` est réglé sur ``Corps de texte``.
- Le style de numérotation est réglé sur ``Aucun``.
- ``Inclure les lignes de ce paragraphe`` n’est pas coché.

Si vous n’avez que quelques indentations horizontales, ce style peut être utilisable avec plusieurs listes. L’indentation du corps du texte est un style de paragraphe prédéfini que vous pouvez utiliser à cette fin.

<div style="astuce">
Astuce

Si vous voulez numéroter des paragraphes en tant que lignes dans un poème, utilisez ``Outils > Numérotation des lignes``. 
Cet outil est plus complet que le formatage disponible à partir des configurations de style. Il permet une sélection de styles de caractères et le positionnement exact des nombres par rapport au texte.
</div>

## Multi-langues

LibreOffice supporte plus de 110 langues, et bien d’autres «&nbsp;locales&nbsp;». Les locales sont des variantes d’une langue dont le vocabulaire et l’orthographe sont uniques. Par exemple, dans la langue anglaise, l’orthographe de *voisin* est *neighbour* au Royaume-Uni, alors qu’aux États-Unis, c’est *neighbor*. Une locale complète se compose de dictionnaires séparés pour la vérification orthographique, la césure et les thésaurus.

De nombreux utilisateurs n’utilisent que la langue par défaut livrée par la version de LibreOffice qu’ils ont téléchargée. Cependant, vous avez deux possibilités pour ajouter la prise en charge d’un plus grand nombre de langues et de locales.

La façon la plus courante d’ajouter la prise en charge d’autres langues est de les sélectionner dans la liste déroulante ``Outils > Options > Paramètres linguistiques > Linguistique > Dictionnaires définis par l’utilisateur > Nouveau``. 

![Les paramètres linguistiques.](Captures/CH6-paramlinguistique.png)

De plus, le dépôt d’extensions de LibreOffice propose également des paquets pour plusieurs langues, dont le grec ancien, le finnois et le basque.  Vous pouvez mettre à jour ou ajouter des extensions avec ``Outils > Gestionnaire des extensions ``.

## Autres fonctions linguistiques

L’ajout de locales peut n’être que la première étape dans l’utilisation d’une autre langue. Il se peut que vous ayez besoin de savoir ceci&nbsp;: 

- Le cas échéant, vous pouvez sélectionner une disposition de clavier selon la langue, en particulier si vous écrivez dans une autre langue que celle pour laquelle votre clavier est au départ optimisé. 
- Installez une police pour une langue. Un style grec est d’une utilité limitée si votre installation de LibreOffice n’a pas de police grecque installée.
- Ajustez les paramètres pour les langues asiatiques ou bidirectionnelles dans ``Outils > Options > Paramètres linguistiques > Langues``.
- Créez plusieurs styles avec des noms similaires dans un document multilingue. Par exemple, vous pouvez avoir des styles de paragraphe appelés Corps de texte - Anglais et Corps de texte - Français.
- Désactivez ``Outils > Correction orthographique`` automatique dans un document multilingue. Si vous n’utilisez pas l’auto-correction, décochez les cases ``Activer l’insertion automatique`` et ``Collecter les mots`` dans ``Outils > Autocorrection > Options d’autocorrection > Insertion automatique``.

## Bloc de citation

Toutes les compositions académiques ont un format spécial pour les citations longues —&nbsp;c’est-à-dire les citations qui remplissent plus de trois lignes ou qui comptent plus de 100 mots environ.

Ces citations sont présentées dans un bloc afin d’en faciliter la lecture. L’hypothèse est qu’une longue citation ne serait pas utilisée à moins qu’elle ne soit importante. 

Typiquement, le style de paragraphe pour une citation est un enfant du style corps du texte. En règle générale&nbsp;:

- N’utilisez pas de guillemets, sauf si quelqu’un est cité directement ou parle.
- Utilisez la même police et la même taille de police que pour le corps du texte. La réduction de la taille de la police ne fait que rendre le bloc plus difficile à lire.
- Utilisez une indentation égale sur les côtés gauche et droit du paragraphe, en fonction de l’interligne. Habituellement, 40 à 50 points de chaque côté sont à peu près la norme, la largeur exacte dépendant de la taille de la police.
- Indiquez les nouveaux paragraphes par un retrait de première ligne ou un interligne augmenté entre les paragraphes. L’indicateur n’est pas nécessairement le même que celui utilisé dans le reste du corps du texte. 

Writer inclut un style de paragraphe de citations par défaut, bien que vous puissiez préférer un style personnalisé avec un nom comme ``Mon bloc de citation`` pour plus de clarté.

![Exemple de bloc de citation.](Captures/CH6-bloccit.png)



## Au-delà des paramètres

Si vous lisez ce livre du début à la fin, à ce stade, tous les aspects des styles de caractères et de paragraphes que vous pourriez  utiliser régulièrement ont été couverts.

Le chapitre suivant traite de certaines fonctions avancées —&nbsp;celles qui ne sont pas strictement nécessaires à votre mise en page, mais qui peuvent automatiser votre composition et rendre les modèles plus efficaces.





