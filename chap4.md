# Polices, couleur et nombre magique

La première étape dans la réalisation esthétique d’un document est de choisir ses polices de caractères. Ce choix est crucial&nbsp;: posez-vous systématiquement la question suivante&nbsp;: dans quelle mesure cette police convient-elle à votre sujet et à vos exigences&nbsp;? 

Souvent, la réponse relève de l’imagination. La typographie a peut-être élaboré un ensemble de principes généraux, mais elle reste un art plus qu’une science. Par exemple, une police géométrique moderne peut sembler futuriste, et donc plus appropriée pour la science-fiction, mais est-elle adaptée à une lecture confortable&nbsp;?

D’autres fois, la réponse sera exacte. Par exemple, certaines polices ont des caractères si fins qu’ils sont invisibles pour une lecture écran ou avec une imprimante basse résolution. D’autres peuvent être si gris qu’ils sont difficiles à lire, ou si gras que leur effet est accablant sur la page.

Cependant, quelles que soient les conditions, les polices que vous choisissez ne doivent jamais être plus visibles que le contenu. Le choix des polices de caractères a pour but d’améliorer votre contenu, et non de faire de la mise en page le centre de l’attention.

## Chercher des polices, les utiliser

De nombreux utilisateurs ne s’aventurent jamais au-delà des polices de caractères installées sur leur ordinateur. Il n’y a pas de mal à faire ce choix, mais ce n’est pas forcément la meilleure stratégie. 

En utilisant des polices de caractères que tout le monde a vues plusieurs fois, vous augmentez les chances de créer une impression de fadeur. Des polices de caractères familières comme *Times Roman* ou *Helvetica* peuvent jouer contre vous, parce qu’elles encouragent les lecteurs à être moins attentifs ou mettre inconsciemment en doute l’originalité de vos idées.

Traditionnellement, beaucoup de polices informatiques sont conçues pour la vente comme n’importe quel autre logiciel. Des entreprises spécialisées comme Adobe en vendent des centaines, y compris des versions officielles de polices célèbres comme *Gill Sans* ou *Didot*. 

Pendant des années, l’alternative aux polices payantes consistait à  utiliser des polices issues du domaine public, qui étaient souvent de mauvaise qualité. Mais aujourd’hui, des centaines de polices sous licence libre sont disponibles (il existe même une licence spécialement dédiée&nbsp;: *Open Font License*). Parfois conçues par toute une communauté et souvent par amour de la typographie plutôt que pour l’argent, les meilleures polices de caractères libres rivalisent largement avec les polices commerciales.

De nombreuses distributions GNU/Linux intègrent des polices sous licence libre dans leurs dépôts de paquets. Dans l’annexe C, vous pourrez trouver des liens vers des sources de polices sous licence libre.

### Installer des polices pour LibreOffice

LibreOffice et Apache OpenOffice prennent en charge les formats de police PostScript (``.pfb``), TrueType (``.ttf``) et OpenType (``.otf``). Il existe d’autres formats de police, qui peuvent être pris en charge par votre système d’exploitation, mais leur sélection et leur qualité peuvent être limitées.

Si vous disposez de privilèges d’administration, vous pouvez installer des polices supplémentaires via votre système d’exploitation. Sinon, vous pouvez installer des polices uniquement pour LibreOffice en plaçant leurs fichiers dans le dossier ``/share/fonts`` dans le chemin d’accès du système répertorié dans ``Outils > Options > Chemins`` et en redémarrant LibreOffice. 

### Fonctions avancées pour les polices

Pendant des années, les fonctionnalités typographiques les plus avancées ont été difficiles à utiliser dans LibreOffice. Il fallait installer et utiliser des polices spéciales pour pouvoir écrire des fractions, des petites capitales ou des ligatures. Une extension a vu le jour (Typography Toolbar) pour rendre ces fonctions plus accessibles, mais seulement pour une poignée de polices.

Ces limitations s’appliquent toujours aux versions de LibreOffice antérieures à la version 5.3 et à Apache OpenOffice. Cependant, à partir de la version 5.3 de LibreOffice, le moteur de rendu de polices utilise automatiquement des fonctions avancées pour les polices OpenType si la police les supporte. Par exemple, au lieu d’utiliser deux lettres ``f`` séparées, LibreOffice utilise la ligature.
 
Si une fonction avancée est disponible dans un fichier de police, elle apparaîtra dans la boîte de dialogue ``Insertion > Caractères spéciaux``. Dans la fenêtre de dialogue d’un style de caractère, vous pouvez forcer son utilisation (ou le désactiver) en tapant un code écrit en minuscules directement après le nom de la police, sans espace entre les deux. Par exemple, ``:smcp`` ou ``:smcp=1`` transformera automatiquement les lettres minuscules en petites capitales. 

Plusieurs de ces petits bouts de code peuvent être ajoutés l’un après l’autre, de sorte que ``:liga:onum:cpsp`` force l’utilisation de ligatures standard, de chiffres de style ancien et de crénage pour les capitales, à condition que la police supporte ces caractéristiques.
 
Dans certains cas, seule la forme complète peut être utilisée parce qu’il faut renseigner une variable. Par exemple, ``:frac=1`` force l’utilisation de fractions avec un séparateur diagonal, tandis que ``:frac=2`` force l’utilisation de fractions avec un séparateur horizontal.

![Forcer les petites capitales avec Linux Libertine G.](Captures/CH4-tessmcp.png)

Les fichiers de polices ne supportent pas tous les mêmes fonctions. Vous devrez donc expérimenter pour apprendre ce que vous pouvez faire avec chaque police. De même, la liste complète des codes de caractères est susceptible de contenir des entrées dont l’utilisation n’est pas immédiatement évidente. Certaines des caractéristiques les plus utiles sont énumérées dans le tableau ci-dessous&nbsp;:

| Code     | Fonction                            |
|----------|-------------------------------------|
|&nbsp;:smcp    | Convertit les lettres majuscules en petites capitales. |
|&nbsp;:liga   | Applique les ligatures standard. |
|&nbsp;:pnum    | Remplace les chiffres à l’aide de glyphes de largeur proportionnelle. |
|&nbsp;:onum    | Convertit tous les chiffres en chiffres de style ancien (old-style). |
|&nbsp;:frac=1  | Convertit les fractions en fractions de plus petite taille avec une ligne diagonale. |
|&nbsp;:frac=2  | Convertit les fractions en fractions de petite taille avec une ligne horizontale. |
|&nbsp;:kern    | Crénage des caractères. |

Une liste exhaustive de ces fonctionnalités typographiques des polices OpenType est trouvable sur la page Wikipédia&nbsp;: ``https://en.wikipedia.org/wiki/List_of_typographic_features``.

Vous pouvez ignorer ces fonctions si vous préférez. Cependant, si vous voulez que vos documents aient la touche la plus professionnelle possible, elles sont faciles à ajouter. Placez-les dans vos styles et modèles de paragraphes et de caractères, et vous pourrez les utiliser sans jamais avoir à y penser à nouveau.



## Choisir des polices

Pour choisir une police à appliquer pour un style de paragraphe ou de caractère, utilisez l’onglet ``Police`` dans la fenêtre de configuration du style.

![Gestion des polices pour les styles.](Captures/CH4-choixpolice.png)

Dans l’onglet ``Police``, trois éléments composent le choix des caractéristiques&nbsp;: la famille de police, la fonte (son style), sa taille. Il est aussi possible de jouer sur ces caractéristiques en y ajoutant des effets depuis l’onglet ``Effets de caractères`` (voir plus loin dans ce chapitre).

### Familles de polices

Les polices sont apparentées, c’est pourquoi on les distingue en plusieurs familles selon leurs formes. Sur ce sujet, on peut se reporter à l’encyclopédie Wikipédia (article «&nbsp;polices d’écriture&nbsp;»).

En résumé, on peut distinguer les polices avec empattement (*serif*), les polices sans empattement (*sans-serif*), les polices à chasse fixe (*monospace*), les polices cursives (*cursive*) et les polices décoratives (*fantasy*). On peut y ajouter les polices faites exclusivement (ou non) de casseaux, c’est-à-dire des parties de casse représentant des symboles ou des ornementations (des pictogrammes,  *dingbats*).

![Quelques familles de polices.](Captures/CH4-quelquesfamillespolice.png)

### Les styles de police

Dans LibreOffice, les styles de polices font indifféremment référence à la graisse, à l’italique ou au droit (romain).

Concernant la graisse (autrement nommée *weight*, poids, en anglais), il s’agit de faire référence à l’épaisseur des traits des caractères. La graisse permet de créer une hiérarchie. Ainsi un titre est souvent écrit avec plus de graisse que le corps de texte. 

Les polices de caractère électroniques ne présentant évidemment pas les  caractéristiques physiques des fontes en plomb des imprimeurs, le gras est souvent considéré par les logiciels de traitement de texte comme une version «&nbsp;graisse&nbsp;» de la police normale. Si bien que l’on trouve souvent l’occurrence ``gras`` sans autre nuance. Cependant certaines polices sont déclinées en  plusieurs version de graisse que l’on peut retrouver dans la colonne ``Style``, exprimée en «&nbsp;gras&nbsp;», «&nbsp;extra-gras&nbsp;», «&nbsp;maigre&nbsp;», etc.

![Certaines polices proposent différentes graisses.](Captures/CH4-policesgraisse.png)

L’italique s’oppose à la police romaine (droite) en ce qu’elle est inclinée vers la droite. La fonction de l’italique est nommée ``Accentuation`` (ou emphase) dans les styles de caractères de la fenêtre des ``Styles``. En français, l’italique s’applique dans certaines circonstances, par exemple pour écrire un mot en langue étrangère, citer un mot, écrire un titre d’ouvrage, mettre en emphase.

La police droite (romaine) est utilisée la plupart du temps en corps de texte. Elle est qualifiée de ``normale`` dans les styles de polices, par opposition à l’italique.

Une dernière variante&nbsp;: les ``Petites majuscules`` ou ``Petites capitales`` que l’on peut activer via l’onglet ``Effet de caractères > Effets`` ou via le menu ``Format > Texte > Petites capitales``. En réalité, il importe de ne pas confondre majuscules et capitales. Les capitales sont des glyphes différentes des minuscules. Lorsqu’on conçoit une police de caractères, on conçoit la plupart du temps les capitales et les minuscules (ainsi que les chiffres). Une majuscule concerne l’ortho-typographie&nbsp;: il s’agit de l’emplacement d’une capitale (comme au début d’un nom propre par exemple).

En typographie, les *petites capitales* sont des caractères *capitales* dont la hauteur correspond à la hauteur d’x, c’est-à-dire la hauteur de la lettre x en minuscule. Elles sont utilisées par exemple pour indiquer un numéro de siècle, écrire une didascalie, ou écrire le nom d’un auteur dans une bibliographie. Il ne faut pas confondre les petites capitales avec des capitales normales dont la taille serait réduite «&nbsp;artificiellement&nbsp;».

![Différence entre capitales et petites capitales.](Captures/CH4-differencecapitalesmajuscules.png)

### Sélectionner une police

En théorie, l’affichage des styles de police devrait vous aider à choisir les polices que vous souhaitez. Or, en pratique, il ne vous donne qu’une une indication approximative.

Pour commencer, une famille de polices comprend quatre styles principaux&nbsp;: régulier (droit), italique, gras et italique gras. Cependant deux polices de même nom peuvent présenter des caractéristiques différentes. Par exemple, la police Nobile Italic n’est pas du tout un italique, c’est en fait une oblique (une version régulière inclinée vers la droite). Si vous préférez un italique, vous devez utiliser une police compatible avec l’italique.

De même si le caractère esperluette (&) figure dans votre composition, il se peut que la police que vous voulez utiliser propose pour ce caractère un glyphe très banal qu’on retrouve dans d’autres polices courantes. Ce n’est pas parce que le concepteur de la police pense que les styles ou les caractères individuels se combinent les uns avec les autres que vous devez accepter ce jugement.

![Plusieurs glyphes pour l’esperluette.](Captures/CH4-esperluette.png)


Un autre point à considérer est que vous êtes rarement susceptible d’avoir besoin du style ``italique gras``. En effet, le style de caractère ``Accentuation`` (emphase) utilise l’italique et le style ``Accentuation forte`` (forte emphase) utilise le gras. Dès lors l’italique gras n’a pas vraiment de légitimité. Il peut être utile comme police d’affichage ou pour des titres, mais le mieux est encore de trouver une police plus adaptée sans utiliser cette combinaison.

Un autre problème avec les styles de police est qu’au fil des ans, diverses tentatives ont été faites pour les normaliser. Dans cette histoire, italique et oblique n’ont pas de place naturelle, c’est pourquoi on croise souvent des polices contenant dans leurs noms les termes «&nbsp;italique&nbsp;» ou «&nbsp;obliques&nbsp;». Il en va de même pour les polices soulignées, qui n’affichent que les limites des caractères mais pas de remplissage.

Plus important encore, ce qui ressemble à première vue à un système bien réglementé est en fait extrêmement arbitraire. Si vous connaissez CSS, on peut attribuer un poids (``font-weight``) à la police pour en déterminer la densité de graisse. En fait, cette échelle se compose de nombres à trois chiffres et on s’accorde généralement pour nommer (en anglais) les caractéristiques en fonction de ce poids. 

| Poids | Appellation          |
|-------|----------------------|
| 100   | Extra Light          |
| 200   | Light                |
| 300   | Book                 |
| 400   | Regular, Roman       |
| 500   | Medium               |
| 600   | Semi-Bold/Demi-bold  |
| 700   | Bold                 |
| 800   | Heavy, Extra, Black  |
| 900   | Ultra, Extra         |

L’utilisation des noms n’est pas normalisée en 300-500, ou en 700-900, et que dires des polices qui, par exemple, pourraient avoir un poids de 350&nbsp;?

Ces mises en garde ne signifient pas que les styles de police ne sont pas utiles pour décider quelles polices utiliser. Mais vous devez vous rappeler que les styles sont relatifs à la famille de police. Faites toujours des expériences approfondies avant de choisir une police.

Traditionnellement, les polices sont mesurées en points. À l’ère du numérique, cette mesure a été normalisée à 72 points pour 1 pouce (0,3527 millimètres). Mais peu importe la correspondance car les points restent non métriques et sont plutôt une affaire d’expertise typographique.

<div style="astuce">

Astuce

Lorsque vous configurez des styles de paragraphes et de caractères, allez dans ``Outils > Options > LibreOffice Writer > Général > Paramètres`` et changez l’unité de mesure en points. L’utilisation de points rendra la conception beaucoup plus facile parce que vous aurez une mesure cohérente. Vous pouvez toujours changer l’unité de mesure en centimètres ou en pouces lorsque vous commencez à ajouter du contenu à un modèle.
</div>

La taille de la police fait référence à l’espace donné à chaque caractère et à l’espace vide autour de celui-ci. Cependant, la façon dont une police utilise l’espace vide peut varier énormément. Chaque police utilise un espace vide différent, ce qui explique parfois pourquoi la hauteur réelle des polices de même taille semble incohérente.

![Taille similaire (54 points) pour trois polices différentes (Liberation Serif, Bitstream Charter, DejaVu Serif).](Captures/CH4-taillepolicepoint.png)

<div style="astuce">

Astuce

Les styles de titres de niveau (1, 2, 3, etc.) prédéfinis de LibreOffice expriment la taille en pourcentage du style de ``Titre`` dont ils sont les enfants.

Étant donné que les tailles des titres sont généralement déterminées au même moment et en fonction les unes des autres, l’utilisation de pourcentages est logique. Cependant, comme nous le verrons plus loin dans ce chapitre, de nombreux éléments de conception sont basés sur la taille des polices standard, qui sont mesurées en points. C’est la raison pour laquelle il peut être tout aussi logique de définir également des titres en points.

Vous pouvez forcer LibreOffice à afficher la taille en points en plaçant le curseur dans le champ ``Taille`` de l’onglet ``Police``, et en entrant la taille suivie de «&nbsp;pt&nbsp;».

</div>

![Par défaut, les tailles des titres sont déterminées en pourcentage.](Captures/tailletitrepourcentage.png)

### Effets de caractères

L’onglet ``Effets de caractères`` contient un panel d’options, avec des degrés d’utilité très différents.

Dans la plupart des cas, les champs ``Couleur de police`` et ``Effets`` sont les plus utiles. En revanche, le surlignage, le barré et le soulignement ont un usage limité dans la plupart des cas. Ces trois derniers sont plutôt destinés à l’utilisation automatique de LibreOffice lors de l’affichage du suivi des modifications dans un document collaboratif.

Dans tous les autres cas, utilisez l’onglet ``Effets de caractère`` avec précaution. Les effets tels l’ombre, le clignotement ou le relief sont des reliques de l’époque où les traitements de texte étaient nouveaux et où les utilisateurs se sont laissés emporter par des élans créatifs discutables. Tous ces effets brisent l’objectif fondamental de la typographie en attirant l’attention sur eux-mêmes sans améliorer la lecture.

![L’onglet des effets de caractères.](Captures/CH4-effetsdecaracteres.png)


### Comment choisir une police

Une bonne pratique typographique consiste à octroyer à chaque document un maximum de deux familles de polices&nbsp;: une pour le corps de texte et une pour les titres, en-têtes et pieds de page. Utiliser davantage de polices aurait tendance à attirer l’attention sur la conception au détriment du contenu.

Cependant, limiter à deux familles ne signifie pas limiter à deux styles de caractères. La majorité des polices de caractères comprennent un minimum de quatre styles de police&nbsp;: romain (droit), italique, gras et italique gras. Certaines en ont jusqu’à neuf, et d’autres encore plus. Utiliser autant de styles de polices dans un même document peut sembler aussi encombrant qu’un trop grand nombre de polices différentes, mais personne ne s’apercevra si vous en utilisez plusieurs ensemble.

Il existe aussi des polices de caractères disponibles en paires, par exemple une version avec un empattement (*serif*) et une autre sans empattement (*sans serif*), et même parfois une variante à chasse fixe (*monospace*). Ces styles sont plus que suffisants pour la plupart des besoins. En fait, il suffit souvent d’une seule famille de polices pour que la mise en forme soit efficace.

### Évaluer une police

Pour apprécier les différences entre les polices de caractères, certains concepts typographiques sont utiles&nbsp;:

- Glyphe&nbsp;: c’est le dessin d’un caractère.
- Hauteur d’x&nbsp;: la hauteur du glyphe du caractère x dans cette fonte, à peu près la hauteur des autres lettres sans ascendantes ni descendantes (a, c, e, m, n, etc.).
- Ascendantes (fûts ou hampe) et descendantes (jambage), respectivement les traits verticaux des caractères (comme dans la lettre t), et le prolongement de ces traits sous la ligne de pied (comme la lettre p).
- Pleins et déliés&nbsp;: les tracés les plus épais et les plus fin d’un caractère.
- Empattements (*serif*)&nbsp;: les extrémités des caractères.
- Diagonales et traverses&nbsp;: les traits diagonaux ou horizontaux des caractères.
- Chasse&nbsp;: la largeur du glyphe d’un caractère.
- Œil, ou hauteur d’œil&nbsp;: la hauteur du glyphe d’un caractère (ne pas confondre avec la hauteur d’x&nbsp;: pour une lettre comme le e, l’œil correspond à la hauteur d’x, mais pas pour une lettre comme h ou l).

Si vous étudiez différentes polices, vous verrez comment ces éléments diffèrent. C’est la première étape pour apprendre à les apprécier de manière professionnelle. Avec ces concepts de base, vous pourrez les observer plus facilement. Bien sûr la typographie étant à la fois une science et un art, vous pouvez largement approfondir vos connaissances. Une bonne manière pour cela est de vous pencher sur les articles de Wikipédia.

![Quelques concepts de typographie.](Captures/CH4-xylophone.png)

### Une police pour le corps

La première police que vous devez choisir est celle du corps de texte. Le principal critère fonctionnel d’une police de caractères est qu’elle doit être facile à lire pour votre public.

Par exemple, si vous concevez un modèle de mémo pour un télécopieur à faible résolution, vous pouvez préférer un style de texte plus large et gras. De même, une brochure destinée aux personnes âgées pourrait utiliser des caractères plus gros que d’habitude, par respect pour leur vue défaillante. Dans d’autres cas, il se peut que vous soyez limité par une résolution d’imprimante plus faible ou même une pénurie temporaire de toner qui vous empêche d’utiliser une police avec des lignes épaisses.

De manière générale choisissez votre police en fonction du type de document (roman, mémo, rapport, lettre, etc.), de votre lectorat (pensez à l’accessibilité de vos documents) et du support de lecture (écran, écran et papier, papier uniquement, etc.).

<div style="astuce">
Astuce

Souvent, un élément-clé du corps du texte est la hauteur d’x. En règle générale, plus la hauteur d’x est élevée, plus une police est lisible. Cependant, si les ascendantes et les descendantes sont courtes, une hauteur x élevée peut jouer en votre défaveur.
</div>

### Une police pour les titres

La deuxième police dont vous avez besoin concerne les titres, en-têtes, pieds de page et sous-titres, en somme, tout ce qui guide le lecteur dans le document mais qui n’est pas réellement du contenu. Elle peut appartenir à la même famille de polices que celle du corps de texte, mais si c’est le cas, elle doit être de style, de taille ou de couleur différente.

LibreOffice prévoit jusqu’à dix niveaux de titres, mais ce nombre est exagéré. Plus de trois ou quatre niveaux (y compris les titres de chapitres) rendent la mise en page difficile et dénuée de sens. Il est possible de mettre en forme dix niveaux d’en-tête pour que chacun soit distinct, mais il est probable que ni le concepteur ni le lecteur ne se souviennent à chaque fois de ce qu’ils signifient.

### Autres considérations

Le choix des polices de caractères commence par l’impression que vous voulez donner. Par exemple, une police de caractères d’usage courant contribue à mettre le lecteur dans un état d’acceptation, tandis qu’une police de caractères originale peut renforcer une impression d’innovation. 

Parfois, une police de caractères doit s’adapter aux contraintes de la page. Une page haute et étroite, par exemple, peut être assortie d’une police de caractères aussi grande et étroite. Par exemple, au moins une édition du *Chien des Baskerville* d’Arthur Conan Doyle a été imprimée en caractères Baskerville.

Voici d’autres considérations plus générales&nbsp;:

- Où le document sera-t-il utilisé&nbsp;? La convention nord-américaine recommande une police avec empattement (*serif*) pour le corps du texte, et une police sans empattement (*sans serif*) pour les titres. En revanche, en Europe, les typographes sont beaucoup moins enclins à respecter cette convention. Vous pouvez utiliser un texte sans empattement pour le corps du texte en Amérique du Nord, mais il peut être perçu comme avant-gardiste.
- Le document est-il destiné à un usage papier ou sur écran&nbsp;? Même aujourd’hui, le texte sur écran a une résolution inférieure à celle d’une impression professionnelle et il peut être traité différemment par le cerveau que les mots sur une page en papier. Pour une utilisation sur écran, il est préférable que les polices aient des formes régulières, avec un minimum de rétrécissement.
- Les destinataires auront-ils les polices installées sur leur ordinateur pour afficher le document correctement&nbsp;? Si vous souhaitez que les destinataires ne voient que le document, vous pouvez envoyer un fichier PDF. Sinon, vous feriez mieux de vous en tenir aux polices les plus courantes installées par défaut sur les systèmes d’exploitation.
- Embarquer les polices dans le fichier est-il une solution&nbsp;? LibreOffice permet d’embarquer des polices de caractères depuis la version 4.1.3.2. Cela simplifie le partage de fichiers, et cela dispense les destinataires d’installer les polices du document. Cependant, même avec seulement deux polices de caractères embarquées, le poids de votre fichier risque de s’en trouver fortement augmenté. Si vous avez en plus des images, il peut devenir trop volumineux pour être envoyé en tant que pièce jointe à un courriel.
- Préférez-vous n’utiliser que des polices libres&nbsp;? Si c’est le cas, vous ne pourrez pas utiliser certaines des polices de caractères les plus connues, bien que vous puissiez parfois trouver des substituts (voir Annexe D). Cependant, les polices libres sont souvent gratuites. Cela signifie également que les destinataires n’ont besoin que d’une connexion Internet pour installer les polices de caractères dont ils ont besoin (s’ils ne savent pas le faire, montrez-leur).

<div style="astuce">
Astuce

Si vous utilisez GNU/Linux et que les polices propriétaires (comme Times New Roman) vous font défaut, utilisez les polices de la série Liberation si vous devez échanger des fichiers avec des utilisateurs n’ayant pas le même système d’exploitation. Ces polices sont conçues pour utiliser les mêmes espacements que celles livrées avec les produits Microsoft. L’adaptation est plus aisée.
</div>


## Harmoniser les polices

La typographie moderne utilise généralement des polices différentes pour le corps et les titres. L’appariement des polices de caractères est plus un art qu’une science, mais vous pouvez augmenter vos chances de trouver des polices harmonieuses&nbsp;:

- Celles qui font partie de la même famille. Les typographes modernes conçoivent parfois des polices disposant de versions avec et sans empattement, de manière à les  utiliser ensemble, ce qui peut être d’une grande commodité.
- Celles qui sont conçues par le même typographe. Les préférences et les habitudes d’un créateur peuvent rester suffisamment similaires entre les polices pour donner une apparence commune.
- Celles qui ont un grand nombre de styles de police, surtout si vous prévoyez d’utiliser la même police pour le corps du texte et les titres.
- Celles qui s’inspirent de la même époque historique ou sont décrites dans les mêmes termes. Même si vous n’êtes pas sûr de savoir en quoi une police Humaniste (de style Renaissance) diffère d’une police Géométrique (avec des formes simples), vous pouvez deviner que les deux ne vont probablement pas ensemble. Bien sûr, plus vous en savez sur l’histoire de la typographie, plus vous associez les polices par leurs origines.
- Celles qui occupent la même largeur horizontale ou verticale. Ce critère permet d’obtenir une conception plus symétrique.

Cependant, quels que soient les critères que vous utilisez, la seule façon de s’assurer que les polices correspondent est de les expérimenter à la fois à l’écran et en imprimant fréquemment des épreuves papier.

### Par catégories historiques

Les polices de caractères défient souvent les classifications faciles. Cependant, il existe certaines tendances historiques générales, même si les experts ne s’entendent pas toujours sur ces critères.

Les catégories historiques ne sont mentionnées nulle part dans LibreOffice, mais vous pouvez parfois faire correspondre les polices de cette manière.

### Polices humanes (ou humanistes)

Les empattements humanistes étaient à l’origine des polices de caractères conçues pendant la Renaissance, principalement par des concepteurs italiens. C’est pourquoi par exemple une police standard est appelée police romaine (droite) et une police cursive italique (d’Italie). Les polices humanistes sont caractérisées par de petites hauteurs d’x, des traits réguliers, des pleins arrondis, de petits empattements et une couleur foncée. Certains traits peuvent être coudés, comme la barre transversale du e minuscule. 

Ces polices sont populaires pour le corps de texte d’aujourd’hui, bien que certains les pensent démodées.

Les caractères humanistes d’origine étaient aussi extrêmement sombres, peut-être pour les rendre plus lisibles à la chandelle. Les imitations modernes réduisent parfois cette noirceur pour les rendre plus acceptables aux goûts modernes.

![Police libre Coelacanth, par Ben Whitmore, inspirée de Centaur par Bruce Roger, elle-même inspirée du travail de l’imprimeur français Nicolas Jenson.](Captures/CH4-humanistserif2.png)

### Old style

Parfois un nom alternatif pour les polices humanistes est utilisé en anglais&nbsp;: Old Style. Cette appellation est techniquement réservée aux polices du XVII^e^ siècle ou à celles qui s’en inspirent. 

Les polices Old Style sont caractérisées par des empattements en pointes, et leurs traits présentent davantage de variations pleins et déliés que les polices humanistes. Comme les empattements humanistes, les polices Old Style sont très populaires pour les usages généraux.

En français la classification Vox-Atypi (Inventée par Maximilien Vox en 1952, et adoptée ensuite par l’Association typographique internationale, ATypI) fait référence aux polices Garaldes, avec empattement triangulaire, milieu XVI^e^ siècle et XVII^e^ siècle.

![Police libre Linden Hill, inspirée du travail de l’imprimeur Frederick Goudy et sa police Deepdene.](Captures/CH4-oldstyle2.png)

### Transitionnelles

Appelées aussi en anglais Enlightenment, NeoClassical, and Modern, les polices transitionnelles sont apparues durant le siècle des Lumières. Leurs empattements sont petits, anticipant la mode à venir des sans empattement (*sans serif*). Leurs traits sont très variés, leurs pleins sont ovales et leurs déliés minces. Les versions extrêmes, comme la Didot (police propriétaire), ont des traits si fins qu’elles disparaissent presque lorsque les caractères sont de petite taille ou avec une imprimante basse résolution.

Les familles des Réales dont la célèbre Baskerville (1757) peuvent être qualifiées de transitionnelles bien que la non moins célèbre Times New Roman (1931) soit en fait inspirée de l’époque. Idem concernant les famille Didones qui apparaissent à l’époque napoléonienne, dans lesquelles on compte les Didot de l’Imprimerie Nationale Française (Firmin Didot, 1811) et, beaucoup plus tard, la Computer Modern dessinée par Donald Knuth et livrée avec le logiciel LaTeX (un processeur de texte).

![Police libre Baskervald ADF, inspirée de la police éponyme d’Hugo Baskerville.](Captures/CH4-transitional2.png)

### Égyptiennes (mécanes)

Les polices égyptiennes sont nommées ainsi en référence à la popularité de la Campagne d’Égypte de Napoléon Bonaparte. Elles apparaissent à cette époque où s’éveillait un certain goût pour les graphies antiques. En anglais elles sont nommées *Slab Serif*. Ce sont des polices avec empattement, mais plutôt larges.

Elles sont souvent utilisées pour les affiches, et certaines peuvent être utiliseés pour le corps de texte. Parfois même, elles font exception à la sagesse populaire selon laquelle les polices de caractères avec empattement ne devraient pas être utilisées pour une lecture écran (site web ou diaporama).

![Police libre Josefin Slab par Santiago Orozco.](Captures/CH4-slabserif2.png)

### Sans serif

Les polices *sans serif* sont exactement ce que leur nom français signifie&nbsp;: des polices sans empattements. Lorsqu’elles sont apparues au début du XIX^e^ siècle, on les appelait *grotesques* et *gothiques*.

Les sans empattements modernes entrent généralement dans l’une des deux catégories suivantes&nbsp;:

- Les polices géométriques sont marquées par des traits réguliers et des formes simples, y compris des cercles. Elles étaient populaires dans les écoles modernistes.
- Les polices humanistes sans empattement ou humanistes modernes sont inspirées des lignes épurées des inscriptions romaines. Les écoles des Arts et Métiers ou Art Nouveau les ont plébiscité, et elles restent populaires aujourd’hui. Certaines Humanistes sans empattements sont suffisamment polyvalentes pour être utilisées à la fois pour les titres et le corps de texte.


![Police Oxygen, utilisée pour le bureau KDE sous GNU/Linux.](Captures/CH4-geometric2.png)

![Police Cantarell, utilisée pour le bureau GNOME sous GNU/Linux.](Captures/CH4-humanistsans2.png)


## Expérimenter

Un faux-texte est un texte sans signification. Il est utilisé pour que vous puissiez vous concentrer sur le résultat des changements de mise en forme plutôt que sur le contenu.

Le faux-texte traditionnel en typographie est le Lorem Ipsum. Il s’agit d’un texte en latin factice, basé sur un passage de Cicéron, *De finibus bonorum et malorum*. Il peut être téléchargé à partir de plusieurs sites ou être généré à la volée. 

> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed mi ut turpis ultricies congue id in neque. Vestibulum eget mollis eros. Integer interdum augue vitae eleifend elementum. Curabitur eget ipsum risus. Vivamus tincidunt erat justo, at lobortis magna tempor vel. Etiam mi velit, imperdiet vitae lectus id, venenatis porttitor felis.

L’hypothèse est que, à moins que vous puissiez lire le latin, vous ne serez pas distrait par la signification des mots lorsque vous vous êtes concentré sur la mise en forme. D’ailleurs, même si vous lisez le latin, il est peu probable que le passage puisse vous distraire très longtemps.

<div style="attention">
Attention

Ne vous fiez pas à l’aperçu de la police dans la barre d’outils ou dans la fenêtre des ``styles``. Un seul mot ne suffit pas pour juger une police. Au minimum, vous avez besoin de plusieurs lignes, et la mise en forme d’une page nécessite habituellement une page entière.
</div>

Étudiez comment les polices que vous choisissez fonctionnent ensemble. Elles ne doivent pas obligatoirement être radicalement différentes, mais suffisamment pour que les lecteurs puissent les distinguer d’un seul coup d’œil. 

Regardez, aussi, comment le corps de texte remplit une page ou une colonne en limitant les césures. Un échantillon criblé de traits d’union indique souvent que la police, le style ou la taille que vous avez choisis ne sont pas adaptés.

Au fur et à mesure que vous travaillez avec un modèle, vous ferez probablement des changements les premières fois que vous l’utiliserez, y compris pour les polices de caractères. Cependant, vous préférerez une sélection de polices de caractères avec un minimum de réglages. Si vous avez besoin de jouer trop avec la largeur ou l’espacement dans un style de paragraphe, alors trouver une autre police est probablement une meilleure solution pour un résultat plus esthétique.

## Ajuster la couleur

Un aspect important du choix des polices est la couleur du corps. Dans ce contexte, la couleur ne fait pas référence au fait que le texte soit noir, rouge ou vert. La couleur est le terme typographique désignant l’impression sur l’œil de la vision générale d’un texte. On parle aussi de gris typographique, c’est-à-dire le résultat optique du texte. C’est surtout la première impression que votre lecteur aura de votre travail.

Une fois que vous avez trouvé la hauteur d’interligne qui donne la couleur la plus agréable à vos yeux, prenez-en note. Cet espacement est le nombre magique qui peut être la clé du reste de votre mise en forme.

Comme vous pouvez le constater en ouvrant des livres publiés par des professionnels, on cherche généralement à faire en sorte que le corps du texte ne soit ni trop noir ni trop clair. Une couleur trop noire peut être dérangeante, alors qu’une couleur trop claire peut être difficile à lire et ressemble à une erreur d’impression. On préfère un gris foncé uniforme.

La couleur des titres est généralement plus foncée que le corps du texte, de sorte qu’elle s’impose lorsque vous naviguez. Sinon, vous pouvez ignorer la couleur des titres et vous concentrer sur le corps du texte.

<div style="astuce">
Astuce

Lorsque vous cherchez la bonne couleur, réglez le zoom d’affichage sur 100%. Imprimez une page pour obtenir une autre perspective. Vous trouverez peut-être que le fait de loucher vous aide à vous concentrer sur la couleur et à ignorer le contenu.
</div>

### Couleur et interligne

Ce qui influence le plus la couleur est l’interlignage (l’action qui consiste à modifier l’interligne). Dans LibreOffice, l’espacement des lignes est défini dans le champ ``Retraits et Espacement > Interlignes``. L’interligne est la mesure de la distance entre une ligne de base (ou ligne de pied) jusqu’à la ligne suivante.

![L’interligne est la distance entre deux lignes de base.](Captures/CH4-interligne.png)

L’interligne est aussi influencé par la taille de la police. La mesure exacte est affichée lorsque vous réglez le champ ``Interligne`` sur ``Fixe``. Pour plus de commodité, les typographes écrivent l’interligne après la taille de la police. Par exemple un paragraphe défini en 12/14 a une taille de police de 12 points et un interligne de 14 points. 

Lorsque la taille de police et l’interligne d’un paragraphe sont identiques (par exemple 12/12) on dit alors que le paragraphe est *solide*. Cependant, il est rare de voir un jeu de paragraphes plein sauf pour des lignes de texte courtes dans des brochures ou des publicités. Cela donne aux lignes un effet de surcharge sauf si on utilise des polices de caractères ayant de petites tailles de lettres et beaucoup d’espace autour d’elles.

![Paragraphe en 12/12.](Captures/CH4-1212.png)

![L’onglet Retraits et espacement.](Captures/CH4-retraitsetespacement.png)

LibreOffice détermine un espacement des lignes par défaut en fonction de la taille de la police. Pour une police de caractères de 12 points, cette moyenne est légèrement supérieure à 14 points. Cependant, comme chaque police utilise différemment l’espace pour ses caractères, cet interligne par défaut n’est pas toujours idéale.

<div style="attention">
Attention

La typographie distingue l’interligne au masculin, qui est l’intervalle entre deux lignes, et l’interligne au féminin qui est la lame de métal placée entre deux lignes pour une composition d’impression avec des fontes en plomb. Dans LibreOffice Writer, on trouve ``Interligne`` (au féminin) qui règle la hauteur de l’espace entre deux lignes, et la valeur ``Fixe``, qui règle l’espacement des lignes pour qu’il corresponde exactement à la valeur que vous saisissez dans la boîte (il peut en résulter un rognage des caractères).
</div>


### Quand changer la couleur d’un texte&nbsp;?

Exercer votre œil pour l’interlignage demande de la pratique. Une couleur acceptable peut également dépendre de la culture, de l’époque et des principes des différentes écoles de design. Cependant, certaines situations  nécessitent des ajustements dans l’interlignage. Faites des expériences jusqu’à ce que vous obteniez des résultats acceptables.

| Augmentez l’interligne si      | Diminuez l’interligne si        |
|--------------------------------|---------------------------------|
| La police a des caractères étroits ou des petits espaces entre eux (le réglage de l’échelle et de l’espacement dans l’onglet ``Position`` peut également être utile). | La police a des caractères larges et comporte des espaces relativement grands entre eux. |
| Les lignes de textes comportent plus de 80 caractères | Les lignes de textes comportent moins de 45 caractères |
| La taille de la police est de moins de 10 points ou plus de 14 points | La taille est comprise entre 10 et 14 points |
| Les polices romaines sont trop noires | Les polices romaines sont gris clair |
| Vous utilisez majoritairement des polices sans empattement | Vous utilisez quelques polices avec empattement | 
| Plusieurs polices sont italiques ou obliques |  |

### Utiliser un nombre magique

Les typographes débutants se demandent souvent comment proportionner les documents. Au fil des âges, d’innombrables théories ont vu le jour, avec plus ou moins de réussites. Mais la plus facile est —&nbsp;faute d’un meilleur nom&nbsp;— le nombre magique.

*Le nombre magique est l’interlignage qui donne la meilleure couleur avec la police de corps de texte choisie*. Son application est simple&nbsp;: chaque fois que possible, toute mesure dans un document sera un multiple de l’interligne des lignes concernées.

Tous les éléments de la mise en forme ne peuvent pas utiliser ce nombre magique, mais il s’applique à la plupart des éléments importants&nbsp;:

- les indentations (ou retraits),
- les indentations (ou retraits) de première ligne,
- l’espacement sous les paragraphes,
- l’espacement au-dessus des paragraphes,
- l’espacement entre les puces (ou les numéros) d’une liste,
- l’espacement avant et après un objet inséré (comme une illustration),
- les tabulations,
- les largeurs des marges,
- la hauteur des en-têtes et pieds de page,
- l’espacement entre le texte et l’en-tête et/ou le pied de page,
- le rapport entre la taille de police d’un titre et ses espacements avant et après.

Parfois, vous pouvez calculer en prenant comme nombre magique la moitié de l’interligne au lieu d’un multiple (ce que propose Libre Office par exemple avec une mesure d’interligne à 1,5). En théorie, vous pourriez utiliser comme unité de base les quarts d’interligne, ou une fraction encore plus petite, mais c’est plus difficile à suivre.

Au fur et à mesure, vous aurez peut-être besoin d’une calculatrice, ou bien de noter dans un carnet une liste des multiples ou demi-multiples que vous utiliserez.


![Une palette d’interlignage pour un corps de texte. Toutes les mesures sont en points et peuvent être utilisées pour déterminer les tailles et les espacements.](Captures/CH4-nombremagique1.png)

L’utilisation du nombre magique est laborieuse, mais elle apporte de la précision et de la cohérence. Vous ne devriez pas avoir de difficulté à détecter si un document a été conçu avec le numéro magique, en raison de son apparence unifiée.


### Le nombre magique par l’exemple

La couleur de la page peut être affectée par de nombreuses variables, y compris l’œil du concepteur, le papier et la quantité d’encre dans  l’imprimante. Parfois, il peut être subjectif, surtout si votre vue est imparfaite. Cependant, à tout le moins, vous devriez être capable de rendre la couleur de la page régulière, même si elle peut toujours être améliorée.

En fait, les variables sont tellement nombreuses qu’à chaque fois que vous utilisez une police, vous devez vous demander si les circonstances sont suffisamment différentes pour qu’il vous faille refaire un test de couleur.

Ne pensez pas que parce qu’une couleur a fonctionné une fois, elle fonctionnera aussi bien dans d’autres circonstances. Le choix de la couleur de la page est toujours relatif à un contexte, et de petits changements dans la mise en page peuvent avoir des effets importants.
 
Toujours expérimenter, systématiquement. Augmentez la taille de la police et l’espacement des lignes petit à petit, et travaillez toujours avec différentes combinaisons. Faire de grands changements vous fera perdre du temps en vous forçant à revenir en arrière.

Si vous avez de la chance, les réglages par défaut de LibreOffice en 12/14 (taille de police à 12 points, et interligne à 14 points) peuvent ne pas nécessiter de réglage. D’après mon expérience, 20% à 35% des polices n’ont pas besoin d’être ajustées.

Par exemple, voici Josefin Slab sans modifications&nbsp;:

![Police Josefin Slab en 12/14.](Captures/CH4-josephinslab1214.png)

La taille de la police peut être augmentée, mais dans l’ensemble la couleur par défaut de Josefin Slab est acceptable telle quelle. C’est tout aussi bien, parce qu’un ou deux points supplémentaires entre chaque ligne peuvent ajouter des dizaines de pages à un livre et augmenter son coût de production.

Cependant, d’autres polices de caractères doivent être testées, en changeant la taille de la police et l’interligne une par une, et en essayant différentes combinaisons.

Par exemple, à la valeur par défaut 12/14, la couleur de la police E. B. Garamond est acceptable, mais la hauteur des lettres majuscules est anormalement élevée, et rend les lignes un peu pincées. La modification de l’espacement des lignes à 12/16 améliore la mise en page&nbsp;:

![En haut, E.B Garamond 12/14, et en bas, E.B. Garamond 12/16.](Captures/CH4-garamondpince.png)


Les polices humanistes conçues à la Renaissance ont souvent été conçues pour être très noires selon les standards modernes. Puisque c’est ainsi qu’elles sont censées être vues, essayer de les alléger ne leur rendrait pas justice.

Toutes les polices avec empattement humanistes modernes ne sont pas aussi sombres que leurs inspirations, mais beaucoup le sont. Coelacanth en est un exemple&nbsp;:

![Police humaniste Coelacanth 12/14.](Captures/CH4-coelacanth.png)


Si vous rencontrez une police moderne qui reste très sombre malgré tous les réglages, c’est probablement le signe qu’elle est conçue pour les titres ou comme police d’affichage. Elle ne doit pas être utilisée pour le corps de texte.

Si vous voulez vraiment utiliser une police avec une couleur foncée, essayez de modifier la largeur des caractères à partir de ``Position > échelle ``, ou modifier l’espacement entre les caractères à partir de ``Position > Espacement``.

Par exemple, après avoir bataillé avec la police Heuristica à un réglage de 12/16, l’auteur de l’ouvrage la trouvait encore trop sombre, il a augmenté l’espacement de 0.8 point entre les caractères. Même à ce moment-là, la police était encore trop foncée pour lui, mais un espacement encore plus important entre les lettres aurait complètement détruit l’apparence de la police.
 
Dans ces circonstances, cette dernière modification était la meilleure qu’il ait pu faire, et elle est encore plus sombre qu’idéale&nbsp;:

![En haut Heuristica 12/14 et en bas Heuristica 12/16 avec +0.8 point d’espacement.](Captures/CH4-pasideal.png)


À d’autres moments, si tout échoue, essayez un style de police différent. Par exemple, la police Raleway Thin est trop pâle pour le corps de texte. Changer la taille de la police ou l’espace entre les caractères améliorerait le rendu, mais pas assez. 

En fin de compte, l’auteur a conclu qu’il utilisait Raleway Thin dans un but pour lequel elle n’était tout simplement pas destinée, et il est passé au poids régulier de Raleway à la place.

![En haut Raleway Thin 12/14, en bas Raleway Regular 12/14.](Captures/CH4-finalement.png)

La leçon de ces exemples&nbsp;? Les valeurs par défaut de LibreOffice peuvent ne pas être ce dont vous avez besoin, alors faites l’expérience aussi largement que possible en recherchant la meilleure couleur de page possible. Trop de choses en dépendent pour que vous vous contentiez de moins.





## Un voyage inattendu

Ce chapitre a commencé par la question de la sélection des polices de caractères, mais il se termine sur  les styles de caractères et de paragraphes. Le chapitre suivant continue là où celui-ci s’arrête, en fournissant davantage de détails.
