# C’est stylé&nbsp;!

Vous avez deux façons de composer un document dans LibreOffice&nbsp;: en le formatant manuellement ou en appliquant des styles&nbsp;; soit, avec un peu d’humour, la mauvaise et la bonne manière.

La mise en forme manuelle (également appelée formatage direct) est la façon dont la plupart des gens conçoivent un document. Lorsque vous faites une mise en forme manuellement, chaque fois que vous souhaitez modifier le formatage par défaut, vous sélectionnez une partie du document  (par exemple, un paragraphe ou une page) et vous appliquez ensuite la mise en forme à l’aide des barres d’outils ou de l’un des menus.

Ensuite, vous répétez l’opération à l’endroit suivant où vous voulez le même formatage. Et le suivant, et le suivant… Si vous décidez de modifier la mise en forme, vous devez parcourir l’ensemble du document, en modifiant le formatage un endroit à la fois.

La mise en forme manuelle est populaire parce qu’elle exige peu de connaissances du logiciel. En fait, vous utilisez l’application bureautique comme si c’était une machine à écrire. Bien que cette approche soit efficace, elle est lente. De plus, de nombreuses fonctionnalités sont difficiles à utiliser lorsque vous formatez manuellement – en supposant que vous puissiez les utiliser.

Comment vous en sortir&nbsp;? Avec les styles. Un style est un ensemble de formes. Par exemple, un style de caractère peut mettre des caractères en italique et en taille 12 pt en un seul clic. Un style de page peut lister toute la composition d’une page, de la largeur de ses marges à son orientation en passant par la couleur de fond.

L’avantage des styles est que vous formatez tout en une seule fois. Au lieu d’ajouter toutes les caractéristiques à chaque endroit où vous formatez, vous appliquez le style. Si vous décidez de changer de fromatage, vous éditez le style une fois, et en quelques secondes, chaque endroit où vous l’avez appliqué sera modifié.

Vous n’avez pas non plus à vous souvenir des détails de la mise en forme – juste du nom du style.

<div style="astuce">

Pour apprécier pleinement la différence entre la mise en forme manuelle et celle basée sur les styles, imaginez que vous prépariez une dissertation de vingt pages demandée par votre professeur. Vous avez décidé d’utiliser la police de caractères DejaVu Sans avec une taille de 10 points. Vingt minutes avant de partir en classe, vous relisez les instructions du professeur et réalisez qu’il n’accepte que des textes en 12 points avec une police avec empattement de type Times New Roman.

Si vous avez formaté manuellement, vous pouvez espérer terminer l’édition avant de partir. Mais si vous avez utilisé des styles, vous pouvez changer la police et sa taille en moins d’une minute, imprimer une nouvelle version, et gagner du temps.

Vous pouvez ensuite enregistrer le document comme modèle. Lors de la prochaine dissertation que vous écrirez pour ce professeur, vous pourrez vous concentrer sur le contenu sans avoir à vous soucier de la mise en forme.
</div>


Autres avantages des styles&nbsp;? La plupart du temps vous pouvez vous passer d’indenter le début d’un nouveau paragraphe et créer un style qui le fait automatiquement.

De même, au lieu de créer un cadre séparé pour une section formatée différemment du reste du document, vous pouvez créer un ensemble de styles, appliquer la mise en forme et continuer à écrire.

Un autre avantage est que si vous utilisez des styles de titres, vous pouvez les utiliser comme signets dans le Navigateur (touche ``F5``) pour vous aider à vous déplacer dans un document. Mais contrairement aux signets normaux, vous n’avez pas besoin de les définir séparément. Les titres sont disponibles dès que vous appliquez les styles.

De même, les titres vous permettent de générer une table des matières avec un minimum de paramètres. Les en-têtes et pieds de page séparés pour les différentes pages sont plus faciles à maintenir. Vous pouvez donner un aspect uniforme aux cadres autour des photos que vous ajoutez, mettre en place des lettrines pour marquer le début d’un nouveau chapitre, et changer automatiquement la mise en page, vous permettant ainsi de vous concentrer sur le contenu.

Cependant, la vraie économie de temps apparaît quand vous enregistrez votre composition comme *modèle*. Une fois que vous avez créé vos modèles de base, chaque fois que vous ouvrez un nouveau document, vous n’avez plus besoin de penser à la mise en forme – au contraire, vous pouvez ouvrir le modèle et commencer à écrire. En général, plus vous utiliserez de styles, plus vous gagnerez du temps.

Étonnamment, certains utilisateurs voient les styles comme une intrusion dans leur droit de travailler comme bon leur semble. Bien sûr, ils peuvent faire ce qu’ils veulent, mais ne pas utiliser des styles signifie qu’ils travaillent plus que nécessaire.

## Démystifier les styles

Il arrive que ceux qui n’ont jamais utilisé de styles aient des idées étranges à leur sujet. Parfois, il peut s’agir d’excuses pour se justifier de ne pas les utiliser, mais souvent ce sont des malentendus.

Voici quelques mythes parmi les plus courants rencontrés à chaque fois que l’on discute du formatage manuel par rapport aux styles&nbsp;:

| Mythes                                | Réalité                               |
|---------------------------------------|---------------------------------------|
|Les styles empêchent les utilisateurs d’exercer leur droit de travailler comme ils le souhaitent. | Vous pouvez travailler sans les styles. Mais pourquoi exiger le droit de vous faire du tort à vous-même&nbsp;? |
|Les styles sont difficiles à apprendre. |Les styles représentent une autre façon de penser que la mise en forme manuelle. Cependant, vous pouvez apprendre leurs concepts de base en dix minutes. |
| Les styles sont faits pour les programmeurs, pas pour les utilisateurs ordinaires. | Les styles sont faits pour tous ceux qui veulent apprendre à travailler efficacement. En fait, il y a autant de programmeurs qui formatent manuellement que d’autres utilisateurs. |
| Les styles vous obligent à mémoriser leurs noms. | Vous n’avez pas besoin de mémoriser quoi que ce soit. Vous devez simplement lire la liste des styles dans LibreOffice. |
| Ce que vous paramétrez dans les styles s’appliquent à tout le document, donc les styles sont plus limités que la mise en forme manuelle. | Un style affecte uniquement la partie du document où vous avez choisi de l’appliquer. |
Les styles sont trop compliqués. | Vous n’avez pas besoin de comprendre toutes les fonctionnalités de tous les styles. Souvent, vous avez seulement à accepter les paramètres par défaut. |
| Les styles sont limités. On ne peut pas changer ce qu’ils contiennent. | La plupart des aspects des styles peuvent être affinés, activés, désactivés, ou ignorés jusqu’à ce que vous en ayez besoin. Vous ne pouvez rien faire manuellement que vous ne puissiez faire avec des styles beaucoup plus facilement. |
| Les styles peuvent entrer en conflit les uns avec les autres. | Un seul style d’un type particulier peut s’appliquer à une sélection. Vous pouvez avoir un style de caractère et de paragraphe s’appliquant à la même sélection, mais pas deux styles de caractères différents ou deux styles de paragraphes différents. |
| La mise en forme manuelle est plus intéressante car vous pouvez faire des changements à la volée. | Vous pouvez aussi changer de style à la volée. En fait, changer de style est plus rapide&nbsp;: vous n’avez qu’à effectuer un changement une seule fois pour l’appliquer partout. |

## Les styles font gagner du temps

Combien de temps gagnez-vous en utilisant des styles&nbsp;? Imaginons que vous ayez un titre que vous souhaitez formater de façon approfondie.

Plus précisément, vous voulez définir la police, la taille, la graisse, la couleur et l’espace au-dessus du paragraphe. En outre, vous souhaitez modifier le titre de manière à ce qu’il commence par un numéro.

La manière la plus rapide d’effectuer ces changements est la suivante&nbsp;:


| Étapes | Actions                                              |
|-----------|------------------------------------------|
| 1             | Sélectionnez le texte. |
| 2–3        | Ouvrez la liste des polices dans la barre d’outils et sélectionnez la police voulue. |
| 4–5        | Ouvrez la liste déroulante de taille des polices dans la barre de menu et sélectionnez la taille voulue. |
| 6            | Cliquez sur l’icône ``Gras`` dans la barre d’outil. |
| 7–8        | Ouvrez ``Couleur de police`` dans la barre d’outils  et sélectionnez la couleur. |
| 9–13     | Allez dans le menu ``Format > Paragraphe > Retraits  et espacement``, éditez le champ ``Espacement au-dessus du paragraphe``, et cliquez sur OK. |
| 14          | Ajoutez une liste numérotée depuis la barre d’outils. |


Vous pouvez mémoriser quelques actions (si vous n’avez pas besoin de faire défiler d’autres paramètres dans des listes), mais cette série d’étapes est une bonne moyenne. Répéter exactement ces étapes pour chaque titre du document sera évidemment source d’erreurs.

En revanche, la mise en place d’un style prendrait 21 étapes. Une fois que le style est prêt, voici comment vous feriez les mêmes changements en utilisant ce style&nbsp;:

| Étapes | Actions                                  |
|--------|------------------------------------------|
| 1      | Placez le curseur n’importe où dans le texte à formater. |
| 2      | Appuyez sur ``F11`` pour ouvrir la fenêtre des ``Styles``. |
| 3      | Au bas de la fenêtre des ``Styles``, changez l’affichage en sélectionnant tous les styles (ou toute autre vue qui puisse afficher le style que vous voulez). |
| 4      | Sélectionnez le style voulu. |
| 5      | Double-cliquez pour appliquer le style. |



Cet exemple est exagéré. Si vous utilisez des styles, la fenêtre des ``Styles`` est probablement déjà ouverte. Souvent aussi, vous n’avez pas besoin de changer la vue.

Cela dit, l’application d’un style nécessite un tiers du nombre d’actions par rapport aux modifications manuelles. Lorsque vous modifiez un style de paragraphe, vous changez également toutes les autres utilisations du style dans le même document. 

## Quand devez-vous utiliser des styles&nbsp;?


Une réponse courte serait&nbsp;: «&nbsp;lorsque cela vous fait gagner du temps&nbsp;». Dans la pratique, même les experts utilisent parfois la mise en forme manuelle dans certaines circonstances.


| Formatez manuellement si&nbsp;:      | Utilisez les styles si&nbsp;:                 |
|---------------------------------|------------------------------------------|
| Le document est court (1-2 pages), et vous n’avez aucun modèle à disposition. | Le document est long (plus de 3 pages). |
| Le document ne sera utilisé qu’une seule fois. | Le document sera utilisé maintes fois. |
| Le document ne sera édité que par une seule personne. | Le document sera édité par plusieurs personnes. |
| Les révisions n’auront lieu que quelques jours après avoir finalisé le document. | Le document sera révisé des semaines, des mois ou même des années après la première version. |
| Certaines personnes qui seront amenées à éditer le document n’ont aucune idée de la manière dont on utilise les styles ou refusent d’apprendre. | Le document appartient à une classe standard comme une lettre, un fax, un mémo, un rapport, une thèse, etc. |
| Un formatage cohérent n’a pas d’importance pour diverses raisons. Par exemple, il ne s’agit pas d’un document qui puisse affecter la marque de votre entreprise. | La conception du document doit correspondre à celle d’autres documents de votre entreprise ou organisation. |
| Vous expérimentez des styles pour la construction d’un modèle. Jusqu’à ce que vous finalisiez ces styles, vous ferez tellement de changements que la création de styles pour le document en cours est souvent un gaspillage d’efforts. | Vous souhaitez utiliser le document de plusieurs façons différentes, chacune d’entre elles nécessitant des modifications mineures&nbsp;: par exemple, l’imprimer sur un fond blanc et un fond rouge. |
| La composition du document est extrêmement simple et régulière, comme une petite rédaction. | Le document a besoin d’une mise en page complexe, comme une brochure. |



Plus votre situation correspond à ces circonstances, plus votre décision sera claire. Toutefois, même si tout indique que vous deviez utiliser la mise en forme manuelle, vérifiez les modèles que vous avez enregistrés. Il se peut que vous en trouviez un assez proche de vos besoins pour être réutilisé.

## Les types de styles


La plupart des suites bureautiques se limitent aux styles de paragraphes et de caractères dans leur traitement de texte. Cependant, LibreOffice ajoute trois styles supplémentaires dans Writer (plus un pseudo-style pour les tableaux), et d’autres styles pour les feuilles de calcul, les présentations et les diagrammes.

Ces styles supplémentaires étendent considérablement les capacités de LibreOffice. Ils font de Writer moins un traitement de texte qu’un outil intermédiaire de publication assistée par ordinateur (PAO). Writer peut ne pas avoir toute la précision d’un outil comme InDesign, mais il soutient la comparaison face à un outil comme FrameMaker pour les documents longs. En fait,  beaucoup d’éditeurs utilisent Writer pour concevoir leurs livres.

Certes, les styles sont moins utiles dans d’autres logiciels. La mise en page de leurs fichiers tend à être moins uniforme que dans Writer. Cependant, pour d’autres applications encore, les styles aident à centraliser les paramètres fréquemment utilisés, ce qui est un avantage en soi.

Les styles disponibles sont les suivants&nbsp;:


### Writer

| Styles         | Commentaires                               |
|----------------|--------------------------------------------|
| Paragraphe     | Les styles ``Paragraphe`` sont les plus couramment utilisés. Un paragraphe commence et se termine lorsque vous pressez la touche ``Entrée``. Les styles de paragraphes courants comprennent ceux du corps de texte et des titres. Équivalent au formatage manuel avec ``Format > Paragraphe``, plus quelques options. |
| Caractère      | Les styles de ``Caractères`` modifient les lettres sélectionnées dans un paragraphe. Les styles de caractères courants sont les caractères gras pour mettre l’emphase, les italiques pour un titre de livre et le soulignement (avec une couleur différente pour un lien Web). Un équivalent avec le formatage manuel est obtenu avec ``Format > Caractère``. |
| Cadre          | Tout élément inséré dans un document Writer est contenu dans un ``Cadre``. En personnalisant les styles de ``Cadres``, vous pouvez ajuster automatiquement les éléments comme les bordures autour des objets, ou la manière dont le texte flotte autour d’eux. Clic droit sur un cadre pour obtenir l’équivalent en formatage manuel. |
| Page           | Les styles de ``Pages`` sont le moyen le plus fiable de formater les pages différemment, y compris les en-têtes, les pieds de page et les notes de bas de page.  L’équivalent en formatage manuel est ``Format > Page``. |
| Liste          | Il s’agit des styles pour la configuration des listes de puces et des listes numérotées. Les styles de liste peuvent être appliqués directement à une liste ou, plus élégamment, être associés à un ou plusieurs styles de paragraphe. L’équivalent en formatage manuel est ``Format > Puces et numérotation``. |
| Tableau        | Jusqu’à la version 5.3, techniquement, Writer n’avait pas de style de tableau.  La fonction de style de tableau est désormais disponible  dans les ``Styles``, il s’agit d’une version améliorée de  ``Tableau > Styles d’AutoFormat``. |

### Calc

| Styles         | Commentaires                               |
|----------------|--------------------------------------------|
| Cellule        | Les styles de ``Cellules`` définissent à la fois l’apparence des cellules et leurs contenus, tels les pourcentages ou les devises. Il est également possible de régler automatiquement le nombre de décimales utilisées par la cellule, d’envelopper le contenu et d’autoriser les césures. |
| Page           | Les styles de ``Page`` définissent la disposition des feuilles ou des cellules sélectionnées à imprimer sur le papier. |

### Draw & Impress

| Styles         | Commentaires                               |
|----------------|--------------------------------------------|
| Graphismes     | Styles pour dessiner des objets, y compris des textes. |
| Présentation   | Styles pour les contenus des diapositives (seulement pour Impress). |




## Travailler avec les styles


Pour ouvrir la fenêtre des ``Styles``, vous avez plusieurs options&nbsp;:

-   Sélectionnez ``Styles > Gérer les styles`` depuis la barre
    de menus.
-   Pressez la touche ``F11``.
-   Cliquez sur ``Plus de styles...`` en bas du menu déroulant de la
    liste de styles dans la barre d’outils.
-   Cliquez sur l’icône ``Styles`` présente dans la
    barre latérale.

<div style="astuce">

Astuce

Vous pouvez aussi surligner une entrée dans la liste des styles de la barre d’outils, cliquer sur la flèche et sélectionner ``Éditer le style`` pour ouvrir la fenêtre d’édition de style.

</div>


![Fenêtre des styles dans Writer](Captures/CH2-fenetrestyles.png)

### Détacher la fenêtre des Styles

Jusqu’à la version 4.4 de LibreOffice, la fenêtre des ``Styles`` s’ouvrait comme une fenêtre flottante. Cette fenêtre pouvait être placée n’importe où sur l’écran ou glissée pour être insérée sur le côté de la fenêtre d’édition. Cependant, à partir de la version 4.4, elle s’ouvre dans la barre latérale.

Pour détacher la fenêtre (ou tout autre affichage dans la barre latérale), cliquez sur la liste déroulante des commandes en haut à droite de la barre d’outils et sélectionnez ``Détacher``. Lorsque la fenêtre est détachée, sélectionnez ``Ancrer`` dans la liste déroulante pour la remettre en place. Au prochain redémarrage, LibreOffice se souviendra si la fenêtre était ancrée ou non.

### Modification du type de style

En haut à gauche de la fenêtre se trouvent des icônes pour chaque type de style. Si vous passez la souris sur elles, vous verrez de quel type il s’agit. Cliquez sur une icône pour afficher les styles correspondants. L’ordre est normalement le suivant&nbsp;:

-   Styles de paragraphe, styles de caractère, styles de cadre, styles de page, styles de liste, styles de tableau.

![Icônes des types de styles](Captures/CH2-iconesstyles.png)


### Visualiser les styles

Comme la liste des styles est longue, la liste déroulante en bas de la fenêtre contient des vues filtrées qui permettent d’éviter un long défilement.

![Filtrage des styles](Captures/CH2-fenetrestyles-vues.png)

La meilleure vue dépend de ce que vous faites. Par exemple, lorsque vous êtes en train de composer un document, la vue ``Hiérarchie`` vous aide à travailler avec des styles apparentés. En revanche, après avoir écrit quelques pages, la vue ``Styles appliqués`` minimise les styles affichés.

Les filtres de base varient selon l’application. Les plus communs sont&nbsp;:

-   ``Hiérarchie``&nbsp;: montre comment les styles sont liés les uns aux autres. Aide à décider où apporter des modifications dans plusieurs styles à la fois en éditant un seul d’entre eux.
-   ``Styles appliqués``&nbsp;: affiche les styles utilisés dans le document courant. Cette vue est inutile avec un nouveau document,
    mais en continuant à travailler, elle réduit le nombre de styles affichés.
-   ``Styles personnalisés``&nbsp;: affiche les styles que vous avez créés, par opposition aux styles prédéfinis.
-   ``Automatique``&nbsp;: une liste minimaliste de styles prédéfinis. C’est la vue par défaut lorsque vous ouvrez un nouveau document.
-   ``Tous les styles``&nbsp;: lorsque vous utilisez ce mode d’affichage, il peut vous donner tellement de noms à analyser que cette utilisation est contre-productive. N’utilisez ``Tous les styles`` que lorsque vous ne parvenez pas à trouver le style que vous recherchez.
-   ``Styles masqués``&nbsp;: ce sont les styles que vous avez supprimés des autres vues pour réduire l’encombrement.

Les styles de paragraphes ont un certain nombre de filtres supplémentaires. La plupart sont des subdivisions, comme les ``Styles HTML`` et les ``Styles d’index``.

Les styles qui ne rentrent dans aucune autre catégorie, tels que ``Légende`` ou ``Pied de page`` sont répertoriés comme ``Styles pour zones spéciales``.

Le filtre ``Styles HTML`` affiche les styles de paragraphes auxquels LibreOffice a affectés directement des balises HTML spécifiques.

## Trouver le style courant


La fenêtre des ``Styles`` s’ouvre toujours avec en surbrillance le style  correspondant à l’endroit où est positionné le curseur. Vous pouvez aussi voir le style courant dans la barre d’outils ``Définir le style de paragraphe``.

De même, le ``Style de page`` courant est affiché, en troisième position à partir de la gauche, dans les indicateurs en bas de la fenêtre d’édition. Lorsque vous parcourez un document, il est mis à jour automatiquement.

En outre, vous pouvez sélectionner ``Édition > Rechercher & Remplacer > Autres options > Styles de paragraphe`` pour localiser un style utilisé dans le document. Pour trouver des styles de caractères, il faut utiliser une autre méthode&nbsp;: ``Édition > Rechercher & Remplacer > Autres options`` inclut la recherche par paramètres de styles de caractères en utilisant les filtres ``Attributs`` et ``Format``.

![Rechercher les formats et les styles, un outil efficace d’aide à la structuration d’un document](Captures/CH2-rechercheretremplacer.png)

## La nature des styles

LibreOffice a des styles prédéfinis que vous pouvez modifier mais pas supprimer. Dans chaque type de style, en particulier les styles de paragraphes et de caractères, les styles prédéfinis sont bien pensés et peuvent être tout ce dont vous avez besoin. En fait, certains soutiennent même que vous aurez moins de problèmes si vous n’utilisez que des styles prédéfinis, même si cela peut limiter vos ambitions. On peut en douter, mais les styles prédéfinis sont des références utiles pour découvrir le potentiel des styles.

Par ailleurs, vous pouvez créer des styles personnalisés basés sur des styles prédéfinis ou en créer des nouveaux en cliquant sur n’importe quel style prédéfini. Par définition, les styles personnalisés répondent aux besoins spécifiques auxquels les styles prédéfinis ne répondent pas.

Les styles personnalisés et prédéfinis se comportent de la même façon. Comprendre leurs comportements est essentiel pour travailler avec tous les styles.

### La hiérarchie des styles

De nombreux styles dans LibreOffice sont hiérarchiques (bien que pour une raison ou une autre, certains, comme les styles de ``Liste``, ne le soient pas). En d’autres termes, ils sont ordonnés dans un arbre, chaque style prenant certaines de ses caractéristiques du style supérieur.

Un style de niveau supérieur est appelé le *parent* du style directement inférieur. Inversement, on appelle ces derniers les *enfants*. Changer le style d’un parent change aussi ses enfants.

Cette relation peut être déroutante, surtout la première fois qu’un style semble changer spontanément ses caractéristiques. Toutefois, en apportant des modifications au parent, vous économisez le temps que vous consacrez à changer les enfants un par un.

![Hiérarchie de styles.](Captures/CH2-parentsenfants.png)

### Modifier la hiérarchie

Lors de l’édition d’un style, vous pouvez manipuler la hiérarchie à l’aide de la zone ``Hériter de`` dans l’onglet ``Gestionnaire``. Le style hérité est le parent du style courant – c’est-à-dire le style dont il hérite des caractéristiques.

<div style="astuce">
Astuce

Dans Apache OpenOffice et les versions antérieures de LibreOffice, la
zone ``Hériter de`` est appelée ``Lié à``.
</div>

Parfois, vous pouvez utiliser ce champ pour définir un parent arbitraire. Par exemple, si vous avez créé deux styles de paragraphes pour les puces, ne différant que par le style de liste utilisé, vous pourriez gagner du temps en définissant l’un des styles comme parent de l’autre. Le but est d’apporter des changements une seule fois, et non deux fois.

### Les styles par défaut

Les seuls styles qui n’ont pas de parent hiérarchique sont ceux situés en haut de l’arborescence. Dans les styles de paragraphes et de caractères, ce style s’appelle ``Style par défaut``. Tous les autres styles du même type sont basés sur le style par défaut.

Vous pouvez choisir d’éditer le style par défaut pour qu’il intègre la mise en forme de base que vous avez choisie pour le document. Vous pouvez également le laisser inchangé, afin de pouvoir échanger facilement des documents avec des personnes sur d’autres machines.

Ce second choix n’est pas toujours possible car différentes versions de LibreOffice peuvent définir différents styles par défaut, mais cela vaut la peine d’essayer. En effet, il y a toujours des styles comme ``Corps de texte`` par défaut, que l’on peut alors activer, ce qui revient à utiliser la fonction ``Format > Effacer le formatage direct``.

Quoi qu’il en soit, les styles par défaut sont utiles lorsque le collage d’un texte formaté ailleurs crée des problèmes. La manière la plus simple de les résoudre est d’éliminer l’essentiel de la mise en forme en appliquant le style de caractère et/ou le style de paragraphe par défaut, puis d’appliquer seulement ensuite la mise en forme souhaitée.

<div style="astuce">
Astuce

Les listes laissent parfois des puces ou des chiffres après avoir appliqué le style de paragraphe par défaut. Dans ce cas, appuyez sur la touche ``Retour arrière`` (*Backspace*) jusqu’à ce que la puce ou le numéro disparaisse.
</div>

![L’onglet Gestionnaire.](Captures/CH2-stylegestionnaire.png)

L’onglet ``Gestionnaire`` comporte quatre champs&nbsp;:

-   ``Nom``&nbsp;: l’entrée qui apparaît dans la fenêtre des ``Styles``. Il doit     être descriptif ou suggérer la fonction du style.
-   ``Style de suite``&nbsp;: le style automatiquement utilisé lorsque vous appuyez sur la touche ``Entrée``. Par exemple, un ``Titre`` de niveau 1 est souvent suivi de ``Corps de texte``. Étant donné qu’un titre n’est presque jamais suivi immédiatement d’un autre titre, il s’agit d’un choix raisonnable. En revanche, le style suivant de ``Corps de texte`` est généralement aussi ``Corps de texte``, parce que plusieurs paragraphes à la suite sont susceptibles de n’être que du texte.
-   ``Hérité de``&nbsp;: le style parent dans la hiérarchie. Les modifications apportées au style parent changeront le style courant, vous n’aurez donc pas à modifier tous les styles associés.
-   ``Catégorie``&nbsp;: celle dans laquelle le style est listé. Par défaut, les styles que vous créez sont affichés dans ``Styles personnalisés``, mais vous pouvez choisir une autre catégorie à la place.

En outre, l’onglet ``Gestionnaire`` affiche un résumé de toutes les options de mise en forme du style concerné.

Les champs de l’onglet ``Gestionnaire`` sont grisés voire absents dans les types de styles où ils n’auraient aucun sens. Par exemple, deux cadres se produisent rarement l’un après l’autre, donc une fenêtre de style ``Cadre`` omet le champs ``Style de suite``. De même, le champ ``Catégorie`` est grisé, car une seule catégorie est nécessaire pour les cadres.

## Appliquer les styles


Pour appliquer un style, vous devez sélectionner une partie du document. Un paragraphe ou une page est sélectionné lorsque le curseur de la souris se trouve n’importe où dedans, mais les cadres ou les objets (comme les illustrations) doivent être sélectionnés en cliquant dessus pour que le cadre et ses huit poignées s’affichent. Vous pouvez également faire glisser la souris pour sélectionner plusieurs paragraphes ou les cellules d’un tableau.

Vous disposez de plusieurs possibilités pour appliquer un style&nbsp;: la fenêtre des ``Styles``, la barre d’outils, le mode ``Tout remplir``, le mode ``Collage`` depuis le presse-papiers et l’utilisation des raccourcis clavier.

### Via la fenêtre des styles

Avec la fenêtre des ``Styles`` ouverte, vous pouvez appliquer un style d’un simple clic de souris – ou deux clics lorsque vous sélectionnez une autre catégorie de style à lister.

Pour de nombreux utilisateurs, la fenêtre des ``Styles`` est le moyen le plus efficace d’appliquer des styles. Certains utilisateurs la gardent ancrée dans la barre latérale, ce qui peut signifier qu’une fenêtre maximisée est nécessaire, selon la taille de votre écran.

Si vous préférez ne pas travailler dans des fenêtres maximisées, détachez la fenêtre et placez-la à un endroit suffisamment proche de la fenêtre d’édition pour minimiser les mouvements de la souris, mais suffisamment éloigné pour ne pas masquer ce que vous faites.

À partir de la version 5.0, la fenêtre des ``Styles`` affiche un aperçu de la mise en forme des polices. Cette prévisualisation est utile pour traiter les styles de caractères, mais omet la plupart des informations pour un paragraphe ou un style de liste. Avec les styles de cadre et de page, cela n’a pas beaucoup d’intérêt.

### Via la barre d’outils

Dans Writer, vous pouvez appliquer des styles de paragraphe à une sélection en ouvrant la liste déroulante des styles qui se trouve dans la barre d’outils (généralement à gauche dans une configuration par défaut).

Lorsque vous ouvrez un document pour la première fois, la liste ne comprend qu’une demi-douzaine de styles couramment utilisés. Progressivement, d’autres styles de paragraphes sont listés au fur et à mesure qu’ils sont utilisés dans le document. En cliquant sur le lien ``Plus de Styles...`` au bas de la liste, vous ouvrez la fenêtre des ``Styles``.

À droite de chaque style, on trouve un menu déroulant qui permet d’ouvrir la fenêtre de configuration du style en cliquant sur ``Éditer le style``. Quant au lien ``Mettre à jour pour correspondre à la sélection``, il permet d’éditer le style en fonction de la mise en forme du texte sélectionné avant d’ouvrir la liste déroulante.

![Les styles dans la barre d’outil.](Captures/CH2-barreoutilstyles.png)

### Via le mode Tout remplir

Avec la fenêtre des ``Styles``, vous avez également la possibilité, pour certaines catégories de styles, d’inverser la technique d’application en choisissant le style d’abord, et l’endroit l’appliquer en second lieu. Lorsque vous appliquez des styles de caractères, c’est presque comme une peinture avec le curseur, ainsi que le suggère son icône en haut dans la fenêtre des ``Styles``.

Pour utiliser ce mode de remplissage&nbsp;:

1.  Sélectionnez un style.
2.  Cliquez sur le bouton ``Mode tout remplir`` en forme de pot de peinture.
3.  Si la mise en forme à appliquer est disponible, le curseur se transforme en pot de peinture. Si le bouton est grisé, c’est que
    vous ne pouvez pas utiliser ce mode avec le style sélectionné.
4.  Faites glisser le curseur sur la partie du document que vous souhaitez formater et cliquez.
5.  Cliquez une seconde fois sur le bouton ``Mode  tout remplir`` pour  désactiver le mode.

<div style="astuce">
Astuce

Une fois un style appliqué avec le ``Mode tout remplir``, vous pouvez changer d’avis en utilisant le clic-droit.

</div>

### Via le collage

Les règles pour copier et coller des styles sont simples, que vous utilisiez ``Coller`` ou ``Collage spécial`` ou que vous copiiez d’un document à un autre&nbsp;:

-   Lorsque la source mise en forme dans un style portant le même nom qu’un style dans la cible est copiée dans un paragraphe, c’est le
    style de la cible qui est utilisé.
-   Lorsque la source mise en forme dans un style autre que celui de la cible est copié dans un nouveau paragraphe, la mise en forme est conservée et le nom du style est ajouté au document cible.
-   Toute mise en forme manuelle ou avec un style de caractère est copiée partout où elle est collée.

Ces règles s’appliquent à tous les styles. Elles s’appliquent également à tous les fichiers Open Document Format, y compris ceux créés dans Apache OpenOffice ou Calligra Suite (la suite du bureau KDE).

Vous pouvez supprimer les mise en formes ainsi collées en sélectionnant ``Effacer le formatage`` en haut de la liste déroulante des styles dans la barre d’outils. Malheureusement, une partie de la mise en forme d’origine, y compris la couleur du texte et le soulignement, persiste parfois lorsque la mémoire système est faible. Dans ce cas, l’application du style de caractères par défaut peut le supprimer.

Lorsque vous collez du contenu, cliquez sur la flèche à côté de l’icône ``Coller`` dans la barre d’outils et sélectionnez ``Texte non formaté``. Cette étape supplémentaire peut prévenir les difficultés de mise en forme.

### Via les raccourcis clavier

Appuyez sur ``Ctrl+1... Ctrl+9`` pour appliquer les styles 1 à 9. Vous pouvez définir des raccourcis clavier pour d’autres styles à partir de ``Outils > Personnaliser > Clavier``.

Les raccourcis clavier peuvent épargner du stress à vos poignets et vos mains lorsque vous rédigez longtemps. Vous pouvez aussi enregistrer des macro-commandes (macros) supplémentaires et les affecter à des raccourcis clavier. Les styles comme ``Style par défaut`` et de ``Corps de texte`` sont de bons candidats pour être intégrés en macros, tout comme les styles de caractères ``Accentuation`` et ``Accentuation forte``.

<div style="astuce">
Astuce

Pour enregistrer des macros dans LibreOffice, vous devez d’abord sélectionner ``Outils > Options > LibreOffice > Avancé > Activer l’enregistreur de macro (limité)``. L’élément de menu ``Enregistrer une macro`` est alors listé sous ``Outils > Macros`` sans avoir besoin de redémarrer LibreOffice.

## Créer et modifier les styles

LibreOffice a des styles pour la plupart des usages ordinaires, donc une façon de gagner du temps est d’utiliser seulement des styles prédéfinis, en ne changeant pas plus d’attributs que nécessaire.

Cependant, si vous décidez d’éditer des styles, vous avez plusieurs possibilités dans Writer&nbsp;:

- Cliquez avec le bouton droit de la souris sur un style dans la fenêtre des ``Styles`` et sélectionnez ``Nouveau`` pour créer un style qui clone le style sélectionné (en d’autres termes, un style qui est un enfant du style sélectionné). Veillez à le renommer immédiatement dans l’onglet ``Gestionnaire``. Si vous oubliez de renommer, vous pouvez trouver le nouveau style dans la liste , désigné par quelque chose comme ``Sans nom1``.
- Cliquez avec le bouton droit de la souris sur un style dans la fenêtre des ``Styles`` et sélectionnez ``Modifier`` pour modifier le style  sélectionné. 
- Faites une mise en forme manuelle et sélectionnez cette partie du document. Ouvrez la liste déroulante en haut à droite de la fenêtre des ``Styles`` et sélectionnez ``Nouveau style`` (ou en cliquant sur l’icône ``Nouveau Style``  dans la barre d’outils, ou encore en composant le raccourci ``Maj + F11``). Cette option est pratique pour créer des styles à la volée ou si vous avez des difficultés à visualiser les paramètres pendant que vous élaborez un style.

Sélectionnez un passage et faites-le glisser dans la fenêtre des ``Styles``. Une fenêtre de dialogue s’ouvre alors pour que vous puissiez donner un titre au nouveau style.

Cochez la case ``Actualisation automatique`` dans l’onglet ``Gestionnaire`` d’un style. Lorsque cette case est cochée, toute mise en forme manuelle que vous effectuez met automatiquement à jour le style.

Modifier les styles depuis la barre d’outils avec les options ``Mettre à jour pour correspondre à la sélection`` ou ``Éditer le style``.

<div style="attention">

Attention

Sélectionnez ``Actualisation automatique`` uniquement si tous ceux qui éditent le document utilisent des styles. Sinon, le résultat pourrait être chaotique. En somme, lorsque vous utilisez des styles, découragez vos collaborateurs de faire du formatage manuel.
</div>

<div style="astuce">
Actuce

LibreOffice met habituellement à jour ses styles en quelques secondes. Cependant, si vous effectuez plusieurs opérations assez rapidement, il se peut qu’il ne parvienne pas à suivre sur les machines avec une mémoire minimale. Si les problèmes persistent, essayez de sélectionner ``Actualiser le style`` dans la liste déroulante à droite de la fenêtre des ``Styles`` ou dans la barre d’outils à droite du menu déroulant des styles. Si vous avez toujours des problèmes, la fermeture et la réouverture du document devraient les résoudre.
</div>

### Masquer et supprimer les styles

Masquer les styles réduit l’encombrement dans la fenêtre des ``Styles``. Pour masquer un style, sélectionnez ``Masquer`` dans le menu contextuel obtenu avec le clic-droit, un style à la fois. Si vous avez besoin d’utiliser à nouveau le style, vous pouvez le restaurer à partir du filtre ``Styles masqués``.

Un style personnalisé qui n’est plus nécessaire peut être supprimé à l’aide du menu contextuel. En revanche, vous ne pouvez pas supprimer un style prédéfini.

<div style="attention">
Attention

LibreOffice vous avertit si un style est utilisé, mais il ne vous empêche pas de le supprimer. Le style par défaut remplace un style supprimé.
</div>

### Nommer les styles

Dans Writer, les styles sont nommés d’après leurs fonctions, suivis de leur position dans la hiérarchie des fichiers. Par exemple, ``Index personnalisé 1`` est le style de paragraphe pour le premier niveau de texte d’un index. De même, les styles de caractères comprennent ``Accentuation`` puis ``Accentuation forte`` et ``Lien  Internet`` puis ``Lien Internet visité``.

<div style="astuce">
Astuce

Puisque vous allez probablement utiliser au moins quelques styles prédéfinis, vous pouvez décider d’utiliser les mêmes conventions pour les styles personnalisés. Cependant, pour vous épargner un défilement infini dans la fenêtre des ``Styles``, pensez à ajouter un préfixe à vos styles personnalisés. Par exemple «&nbsp;M-&nbsp;» pour comprendre «&nbsp;Mon style&nbsp;». De cette façon, vous pouvez facilement retrouver vos styles et les appliquer sans changer le filtre de liste.

</div>

Il n’est pas rare d’utiliser différentes catégories de styles dans une même mise en forme. Par exemple, un style de liste peut être associé à un style de caractère afin que vous puissiez avoir des puces colorées. En outre, le même style de liste peut être affecté à un style de paragraphe afin qu’il soit utilisé à chaque fois que vous choisissez le style de paragraphe. Pour vous aider à trouver chacun de ces styles plus tard, donnez-leur le même nom. Puisque chacun est dans une catégorie différente, ni vous ni LibreOffice ne pouvez les confondre.

### Automatiser l’application de styles

Comme nous l’avons vu, certains styles se succèdent selon un schéma préétabli. Un style de ``Titre 1`` est généralement suivi d’un ``Corps de texte``, et un style ``Corps de texte`` est suivi d’un autre ``Corps de texte``. Un style de première page est généralement suivi d’une page gauche, suivie d’une page droite, etc.

Vous pouvez profiter de ces patrons en remplissant le champ ``Style de suite`` dans l’onglet ``Gestionnaire``. Avec certains types de styles, comme les listes, avoir un style suivant n’a aucun sens et le champ est grisé. Mais avec un champ ``Style de suite`` rempli, le fait de commencer un nouveau paragraphe ou une nouvelle page appliquera automatiquement le style suivant sans que vos mains ne quittent le clavier.

## Une autre manière d’écrire

Ne soyez pas surpris si vous avez besoin de temps pour vous habituer aux styles. Leur utilisation implique une planification préalable plus poussée que la mise en forme manuelle. Pourtant, les concepts de base sont simples, et déjà vous pouvez commencer à voir comment les styles peuvent automatiser la mise en forme.

Dans le chapitre suivant, vous verrez comment les modèles peuvent vous aider à recycler les compositions de documents pour vous faire gagner encore plus de temps.

