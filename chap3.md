# Les modèles

Personne n’a le temps de mettre en forme un document chaque fois qu’il s’assoit pour écrire. C’est inefficace. Vous ne voulez pas non plus que tous vos collègues conçoivent leurs propres mise en forme. La solution à ces deux problèmes réside dans les modèles (on peut dire aussi gabarits ou, en anglais, les *templates*), c’est-à-dire des fichiers pour stocker la forme et la structure à réutiliser ou à partager.

Les modèles sont traités différemment des documents ordinaires. Avant de les utiliser, ils doivent être enregistrés pour que LibreOffice les reconnaisse. Ils ont également leurs propres menus et éléments de menu que les fichiers normaux n’utilisent pas.

La plupart du temps, les modèles enregistrent le formatage des paragraphes, des caractères, etc. Toutefois, ils peuvent également stocker la structure des documents, soit sous forme de contours, soit sous forme de zones qui sont censées contenir des informations standard ou indiquent avec des caractères génériques quelles informations doivent être ajoutées. 

Cette fonction structurelle des modèles tend à être sous-estimée. Cependant, vous pouvez en trouver des exemple dans les modèles d’Impress qui proposent des contenus calibrés vous permettant de réaliser rapidement une présentation en remplissant les champs.

![Modèle de présentation avec Impress.](Captures/CH3-modelepresentation.png)

<div style="astuce">

Astuce

Si vous avez déjà une expérience des suites bureautiques, vous avez l’habitude de vous méfier des modèles en raison de leur tendance à être corrompus dans Microsoft Word lorsque vous apportez des modifications ou essayez de mélanger des modèles.
Ne vous inquiétez pas. Les modèles de LibreOffice sont conçus pour éliminer ce genre de problèmes, mais parfois au prix d’un amoindrissement des possibilités.

</div>

## Quand utiliser les modèles&nbsp;?

Comme pour les styles, la réponse courte est&nbsp;: «&nbsp;chaque fois que cela est possible&nbsp;». Non seulement les modèles sont plus efficaces que la conception à partir de zéro, mais leur utilisation vous aide à vous habituer aux concepts qui les caractérisent.

On entend des utilisateurs dire que les modèles ne sont pas pratiques parce que chaque document qu’ils font est différent.  Cependant, à y regarder de plus près, on constate que le problème n’est pas que tous les documents sont différents, mais que la personne qui l’affirme n’a pas pensé en termes de structure. Elle ne se rend pas compte non plus qu’un gabarit cohérent destiné à tous les documents peut faire partie de l’image de marque d’une entreprise ou d’une personne.

Une raison plus plausible de ne pas utiliser les modèles est que leur configuration prend du temps. Pourtant, même cette excuse ne survit pas à un examen minutieux. 

<div style="exemple">
Exemple

*Témoignage de l’auteur*&nbsp;: le gabarit que j’ai conçu en 2002 m’a pris plusieurs heures, et peut-être deux autres à l’affiner.  Depuis, j’ai utilisé ce gabarit pour des centaines de documents. Chaque fois que je l’utilisais, je pouvais commencer à écrire immédiatement et sans me soucier de la mise en forme.  Avec un minimum de trois heures par document, j’ai pu facilement économiser plus d’un mois de temps de travail grâce à ce seul gabarit. D’autres modèles que j’ai faits au fil des années ont été moins utilisés. Mais toujours, les premières heures perdues dans la conception des gabarits ont été récupérées d’innombrables fois.

Utiliser des modèles, c’est planifier à l’avance. Ce nouveau flux de travail implique de faire l’effort de changer vos habitudes.

</div>

## Comment fonctionnent les modèles&nbsp;?

Contrairement à Microsoft Word, LibreOffice n’affiche pas de modèle «&nbsp;normal&nbsp;» dans ses menus. Cette omission permet de ne pas causer une trop grande production de documents utilisant ce modèle par défaut et donc prêter le flanc à des vagues de documents corrompus. 

La seule façon dont vous pouvez modifier les formats par défaut de LibreOffice est de modifier les paramètres de police dans ``Outils > Options > LibreOffice Writer > Polices standard (occidentales)``. Le formatage par défaut tel que les styles de page et de liste ne peut pas du tout être édité.

Toutefois, vous pouvez également concevoir un modèle avec beaucoup plus d’informations de mise en forme par défaut. Ses paramètres seront utilisés pour chaque document lorsque vous sélectionnez ``Fichier > Nouveau``, à moins que vous ne choisissiez spécifiquement un autre modèle.

### Lier des modèles avec des documents

Quel que soit le modèle que vous utilisez, il doit être correctement enregistré avant de pouvoir l’utiliser. Un seul modèle peut être appliqué à la fois à un document. 

<div style="attention">

Attention

Deux styles de même nom dans des documents séparés peuvent avoir différentes mises en forme.
</div>

Si le modèle change, la prochaine fois que vous redémarrez LibreOffice et ouvrez le document, vous serez invité à le mettre à jour. 

![Avertissement en cas de modification d’un modèle.](Captures/CH3-conserveranciensstyles.png)

Les styles partagés par le modèle et le document sont mis à jour, mais pas les styles qui ne sont que dans le document. Vous pouvez sélectionnez ``Mettre à jour les styles``, mais n’oubliez pas de sauvegarder le document après la mise à jour.

![On peut voir sur quel modèle est basé le document en cours.](Captures/CH3-proprietesdocument.png)

En revanche, si vous sélectionnez ``Conserver les anciens styles``, ils sont  dissocié du modèle et vous n’êtes pas invité à effectuer d’autres mises à jour lorsque le modèle change à nouveau. Toutefois, la fenêtre ``Propriétés`` mentionne toujours le modèle, même s’il n’est pas pertinent.

<div style="astuce">
Les documents dont les modèles ont été édités ne sont pas toujours mis à jour lorsqu’ils sont fermés et rouverts. Vous devrez peut-être redémarrer LibreOffice pour que la modification prenne effet.
</div>

En règle générale, on évite de détacher les documents de leurs modèles. Comme l’intérêt d’utiliser ces derniers est de faciliter la conception uniforme, les meilleures pratiques sont les suivantes&nbsp;:

- Modifiez la mise en forme uniquement sur le modèle.
- Gardez toujours les documents connectés à leurs modèles.
- Ne modifiez jamais la mise en forme d’un document &ndash; même la suppression ou l’ajout d’un style peut détacher un document de son modèle.
- Dès que vous apportez des modifications au modèle, fermez et rouvrez les documents qui utilisent ce modèle. Cette pratique peut perturber votre travail, mais cela signifie que vous n’aurez pas à vous souvenir de ce qui a changé, ou vous demander si vous avez fait un changement accidentel lorsque vous verrez affichée une notification de changement la prochaine fois que vous ouvrirez un de ces fichier.

### Identifier un modèle

Pour identifier un fichier modèle de LibreOffice, regardez la deuxième lettre de son extension&nbsp;: c’est toujours un *t*. C’est le cas à la fois pour ce qui concerne les formats Open Document et dans le format obsolète OpenOffice. org 1.0. 

| Application | ODF Format | OOo Format |
|-----------------|-------------------|-------------------|
| Writer           | .ott                     | .stw                   |
| Calc               | .ots                     |  .stc                   |
| Impress       | .otp                    |  .sti                     |
| Draw             | .otg                    | .stf                     |

### Utiliser le gestionnaire de modèles

LibreOffice n’affiche pas directement les répertoires de modèles. À la place, dans la fenêtre du ``Gestionnaire de modèles``, il crée une vue virtuelle du contenu de tous les répertoires de modèles listés dans ``Outils > Options > Chemins > Modèles``.

![Le gestionnaire de modèle est organisé en dossiers et sous-dossiers.](Captures/CH3-gestionnairemodeles.png)

On peut accéder au gestionnaire de modèles de plusieurs manières selon le système d’exploitation et la version. Par exemple&nbsp;:

- ``Fichier > Nouveau > Modèles``.
- ``Fichier > Modèles > Gérer``.
- Directement depuis l’écran d’accueil de LibreOffice 

## Préparer une bibliothèque de modèles

Que vous créiez ou téléchargiez vos modèles, commencez par évaluer les types de documents que vous composez régulièrement. L’installation ou la conception de plus de modèles que vous n’en avez besoin ne fera que rendre plus difficile la recherche de ceux que vous utilisez vraiment.

Commencez par penser votre organisation générale. Si vous travaillez pour une entreprise, a-t-elle a une charte graphique que vous utiliserez systématiquement&nbsp;? Si c’est le cas, commencez par créer les couleurs dans ``Outils > Options > Couleurs`` afin qu’elles soient disponibles pour des utilisations telles les bordures et les titres des documents couleur.

Des polices de caractères particulières&nbsp;? des tailles de polices particulières&nbsp;? Tous ces éléments peuvent entrer dans un modèle universel qui peut être le point de départ pour d’autres. Donnez-lui une appellation générique comme «&nbsp;Doc-Général&nbsp;» ou «&nbsp;Doc-Standard&nbsp;».

Gagnez en précision en rappelant les documents que vous avez rédigés dans le passé&nbsp;:

- ``Fichier > Ouvrir > Documents récents``.
- Vérifiez les pièces jointes des courriels que vous avez envoyés. 
- Tenez un journal des documents que vous produisez pendant une semaine ou un mois et de la fréquence à laquelle vous écrivez le même genre de document.

Un gabarit soigneusement élaboré peut durer des années, donc prendre le temps de classer votre travail en vaut la peine.

<div style="exemple">
Exemple

Imaginez que vous êtes chargé d’affaires dans une entreprise. Vous voulez que votre gabarit polyvalent utilise la marque de votre entreprise.

Après mûre réflexion, vous constatez que vous rédigez régulièrement des notes de service d’une page, de courtes lettres officielles et des rapports internes mensuels plus longs. Vous rédigez également des rapports trimestriels pour le bulletin d’information de l’entreprise dont le rédacteur en chef veut une copie en HTML, et une lettre personnelle occasionnelle, dont le design devrait indiquer clairement que vous ne parlez pas officiellement pour l’entreprise.

En planifiant votre bibliothèque de modèles, vous répondez à la question&nbsp;: «&nbsp;quels documents dois-je produire régulièrement dans LibreOffice&nbsp;?&nbsp;». Si vous êtes comme la plupart des gens, vous trouverez probablement au moins trois ou quatre modèles différents dont vous aurez besoin régulièrement. 
</div>

## Modèles prêts à l’emploi

Une fois que vous avez déterminé vos besoins, vous pouvez obtenir vos modèles de plusieurs façons&nbsp;:

- Télécharger et installer des modèles réalisés par d’autres, éventuellement avec des modifications mineures.
- Créer vos propres modèles en utilisant l’assistant de LibreOffice.
- Concevoir vos propres modèles à partir de zéro.

Indépendamment de la façon dont vous obtenez vos modèles, ils doivent être enregistrés avant que LibreOffice puisse en profiter pleinement.

### Téléchargement de modèles

Si vous préférez ne pas concevoir vos modèles à partir de zéro, vous pouvez utiliser des modèles préexistants à la place, en les modifiant si nécessaire (reportez-vous à l’annexe A).

Beaucoup de modèles que vous trouvez en ligne sont disponibles sous licence libre, donc vous pouvez généralement les modifier et les partager.

L’inconvénient d’utiliser les modèles des autres est que vous devez les trier pour trouver ce que vous voulez. Il peut même arriver que vous téléchargiez deux fois le même modèle depuis des sites différents&nbsp;: ils ont simplement été renommés.

Bien souvent, vous devez également modifier les modèles téléchargés pour obtenir exactement ce que vous souhaitez. En fin de compte, vous ne gagnerez peut-être pas autant de temps qu’espéré.

Un autre problème avec ces sites officiels est que chaque modèle doit être téléchargé et installé séparément. Cependant, vous pouvez aussi trouver des extensions qui proposent des pack de modèles. On peut les installer à partir de ``Outils > Gestionnaire des extensions`` après les avoir téléchargés. 

Vous pouvez également ouvrir des modèles issus de la suite Microsoft Office et les convertir au format Open Document Format. Il s’agit d’une solution envisageable pour les fonds de présentation avec Impress, mais beaucoup moins pour le traitement de texte, en particulier pour les modèles dont le formatage est complexe, et qui peuvent ne pas bien s’importer ou s’exporter.

<div style="attention">

Attention

L’utilisation de modèles issus de la suite Microsoft Office est illégale si vous n’avez pas de copie valide de Microsoft Office. Pour éviter toute difficulté juridique, évitez d’utiliser ces modèles sauf pour un usage personnel et non commercial.
</div>

### Création de modèles avec les assistants

Si la conception de vos propres modèles est une étape trop importante, commencez par utiliser les assistants de LibreOffice.

Ces assistants peuvent vous donner une idée des types de décisions que vous devez prendre lorsque vous concevez vos propres modèles. Ainsi, même si le résultat ne vous plaît pas, ils sont de bons exemples de conception structurelle de modèles et vous pouvez vous en inspirer.

Les assistants sont disponibles dans le sous-menu ``Fichier > Assistants``. Certains  vous donnent la possibilité d’utiliser une base de données LibreOffice Base ou le carnet d’adresses d’un client de messagerie pour remplir des champs, ce qui est pratique pour les envois massifs.

Lorsque vous avez terminé de paramétrer les options pour un modèle, l’assistant propose par défaut de l’enregistrer dans un sous-répertoire de votre répertoire de modèles personnels. L’utilisation peut être immédiate.

![Assistant de conception de modèle de lettre.](Captures/CH3-assistantlettre.png)

### Enregistrement des modèles

Vous pouvez simplement *sauvegarder* un modèle. En effet, si vous enregistrez un fichier à l’aide de ``Fichier > Enregistrer`` ou ``Fichier > Enregistrer sous``, vous pouvez sélectionner le modèle LibreOffice (``.ott``) dans la liste déroulante des formats de la boîte de dialogue ``Enregistrer``. Cette approche est utile si vous voulez installer ce modèle sur un autre ordinateur ou en donner une copie à un ami. 

En revanche, pour que le modèle soit utilisable avec votre système (et le retrouver dans le gestionnaire de modèles) il vous faut l’enregistrer dans LibreOffice. Vous pouvez suivre ces étapes&nbsp;:

1. Enregistrez-le avec ``Fichier > Modèles > Enregistrer comme modèle``. Le gestionnaire de modèles s’ouvre. Si vous avez récemment créé plusieurs modèles, vous devrez peut-être cliquer sur ``Actualiser`` pour afficher tous les modèles existants.
2. Cliquez sur un sous-dossier dans l’affichage si vous voulez préciser où le modèle s’affichera à l’avenir dans le Gestionnaire de modèles.
3. Cliquez sur ``Enregistrer`` et donnez un nom au nouveau modèle. Si nécessaire, vous pouvez écraser un modèle existant. Le modèle nouvellement enregistré est désormais disponible. 

<div style="attention">
Attention

Si vous ne sélectionnez pas de sous-dossier, le modèle est enregistré dans l’onglet approprié d’après son format de fichier, plutôt que dans un sous-dossier.
</div>

### Définir un nouveau modèle par défaut

Le modèle par défaut de chaque module dans LibreOffice est basé sur plusieurs hypothèses. Elles supposent que la plupart des utilisateurs veulent une police générique, comme Times New Roman ou Liberation Serif. Elles supposent aussi qu’il existe un lien entre la langue locale et le format du papier, de sorte qu’une installation qui utilise par défaut l’anglais américain utilisera du papier de format lettre, tandis qu’une installation qui utilise par défaut le français utilisera du papier A4.

Ces hypothèses sont raisonnables. Toutefois, si elles ne répondent pas à vos besoins, modifier le modèle par défaut vous permettra de ne pas avoir à retoucher tous les documents génériques que vous commencez.

Pour modifier le modèle par défaut&nbsp;:

1. Créez et enregistrez le modèle de remplacement. 
2. Ouvrez le Gestionnaire de modèles. Sélectionnez le modèle par défaut de remplacement.
3. Cliquez sur ``Définir par défaut``.

Maintenant, chaque nouveau document utilisera ce modèle par défaut à moins que vous ne choisissiez spécifiquement un autre modèle. LibreOffice revient à son modèle par défaut d’origine si vous supprimez le modèle de remplacement.

## Modèles et structures

Les modèles peuvent tout stocker. Par exemple, dans un modèle de lettre, vous pouvez placer une adresses de retour. Dans un modèle de présentation, vous pouvez placer l’arrière plan propre à votre entreprise, etc. En somme, dans un modèle, vous pouvez stocker toute information que vous réutilisez mais que vous ne voulez pas réinventer ou recopier. 

Ces informations sont particulièrement faciles à stocker dans Impress, où chaque information peut être placée sur une seule diapositive et facilement supprimée ou réorganisée.

### Utilisation des champs réservés

Les champs réservés sont des champs qui marquent le type d’informations nécessaires à un endroit donné, et que vous pouvez remplir en cliquant dessus.

Typiquement, Impress a des champs réservés intégrés pour les titres de diapositives, le corps du texte et les objets insérés. 

![Une diapositive dans Impress, avec des champs réservés.](Captures/CH3-champsdansimpress.png)

Dans Writer, vous pouvez utiliser les champs réservés pour le texte et les objets à partir de ``Insérer > Champs > Autres ``. Sélectionnez le type de champ, puis configurez-le dans la fenêtre d’édition. Une fois le champ créé, on peut l’utiliser dans le document en cliquant dessus.

De multiples champs sont disponibles, plus ou moins configurables. Vous pouvez d’abord sélectionner le type de champ parmi des catégories, puis le format voulu. Par exemple, dans les champs faisant référence  à la structure du document, on trouve le type ``chapitre`` puis le format ``numéro de chapitre`` (il fait appel à un compteur). Vous pouvez aussi configurer un champ conditionnel dans la rubrique ``fonctions > texte conditionnel``.

![Configurer un champ réservé dans Writer](Captures/CH3-champreserve.png)

<div style="astuce">
Astuce

Les champs ont leurs propres styles de caractères, ce qui permet de les rechercher rapidement avec la fonction ``Rechercher & remplacer``.
</div>

### Utiliser les champs automatiques dans les modèles

Quatre types de champs automatiques sont susceptibles d’être utiles dans un usage courant&nbsp;:

- Les informations système, telles la date et l’heure,
- Les informations générales de l’utilisateur, stockées dans ``Outils > Options > LibreOffice > Données d’identité``.
- Les informations sur le document, que l’on peut trouver dans ``Fichier > Propriétés``. 
- Les statistiques du document, générées au fur et à mesure que vous créez le document. Elles comprennent les numéros de page, le nombre de pages et d’autres informations généralement placées dans l’en-tête ou le pied de page.

Au fur et à mesure que ces informations changent, les zones du modèle sont également modifiées et mises à jour chaque fois que vous ouvrez le document sans avoir à le modifier manuellement.

<div style="astuce">
Astuce

Les champs ``Date`` et ``Heure`` ont deux déclinaisons chacune. Si vous allez dans ``Insérer > Champs > Autres > Document``, vous verrez que vous avez le choix entre ``Date`` et ``Date (fixe)`` et entre ``Heure`` et ``Heure (fixe)``.

Les champs fixes ajoutent des informations changent jamais. En revanche, les champs variables sont toujours mis à jour à la date et à l’heure actuelles lorsque quelqu’un ouvre un document ou met à jour les champs.

Les deux ont des usages. Par exemple, vous pouvez placer un champ ``date fixe`` à côté de la signature d’un témoin et un champ ``date variable`` dans l’en-tête d’un modèle de lettre.

Les champs de date et d’heure prennent également en charge un certain nombre de formats, par défaut celui configuré pour la langue courante. LibreOffice n’offre pas la possibilité de modifier à la volée les formats par défaut.
</div>

## Manipuler les modèles

À moins d’être extrêmement bien organisé ou d’avoir de la chance, vous ne réaliserez pas un modèle parfait du premier coup. Il est probable que, la première fois que vous utiliserez votre modèle, vous trouviez d’innombrables raisons de l’améliorer pour qu’il réponde à vos besoins.

Pour ouvrir un modèle à éditer, sélectionnez ``Fichier > Modèles > Gérer`` pour ouvrir le gestionnaire de modèles, sélectionnez un modèle et cliquez sur Modifier dans la barre au-dessus des modèles.

Une fois qu’un modèle est ouvert, vous pouvez l’éditer exactement comme n’importe quel autre document. Cependant, n’oubliez pas d’enregistrer vos modifications via ``Fichier > Modèles > Enregistrer comme modèle``.

### Supprimer des modèles

Dans le gestionnaire de modèles, sélectionnez le modèle à supprimer, puis cliquez sur le bouton ``Supprimer``.

Vous pouvez supprimer des modèles personnalisés ou importés. Vous ne pouvez pas supprimer les modèles installés avec LibreOffice. Quant à ceux installés via une extension vous pouvez les supprimer en désinstallant l’extension depuis ``Outils > Gestionnaire des extensions``.


### Changer de modèle

Vous ne pouvez pas directement modifier le modèle utilisé par un document. Il n’y a pas non plus de moyen d’appliquer plusieurs modèles à un même document. Toutefois, vous pouvez utiliser trois solutions de contournement.

La première consiste à ouvrir un document basé sur un autre modèle, puis à le copier et le coller. Cette méthode fonctionne mieux lorsque tous les styles des deux documents ont le même nom, car les styles du document original prendront en charge la mise en forme du nouveau document.

De plus, tous les styles personnalisés utilisés dans le document d’origine sont copiés dans le nouveau document. Par contre, les styles personnalisés qui sont définis mais non utilisés n’apparaîtront pas dans le nouveau document.

Une deuxième méthode consiste à créer un nouveau document de base avec un modèle différent. Si vous importez le document dans le document maître (via ``insertion > Document``), il est reformaté lorsqu’il est utilisé à partir du document maître. Lorsqu’il n’est pas ouvert à partir du document maître, il conserve sa mise en forme originale.

La troisième méthode, la plus pratique, consiste à transférer des styles entre les documents. Le transfert de styles est pratique lorsque deux personnes ont travaillé sur un document, mais ont apporté leurs propres modifications au modèle (bien que vous ne devriez pas encourager cette pratique).  Vous pouvez également utiliser cette fonctionnalité pour transférer le formatage manuel d’un document à son modèle, bien que les modifications apportées au modèle soient généralement plus fiables.

![Transférer des styles entre documents](Captures/CH3-chargerdesstyles.png)

Pour transférer des styles entre les documents&nbsp;:

1. Appuyez sur ``F11`` pour ouvrir la fenêtre des ``Styles``.
2. Dans le menu déroulant à droite de la barre d’icônes, cliquez sur ``Charger les styles``. 
3. La fenêtre ``Charger les styles`` s’ouvre. Le volet ``Catégories`` affiche les dossiers dans le Gestionnaire de modèles, tandis que le volet ``Modèles`` affiche les modèles du dossier sélectionné.
3. Sélectionnez l’un des modèles dans le volet ``Modèles`` ou cliquez sur le bouton ``À partir d’un fichier`` et sélectionnez un modèle de document stocké par ailleurs à l’aide du gestionnaire de fichiers.
4. Sélectionnez les types de styles à importer en choisissant les options à cocher au bas de la fenêtre ``Charger les styles``. Notez que l’option ``texte`` concerne à la fois les styles de paragraphes et de caractères.
5. Si vous voulez remplacer les styles du document courant qui portent le même nom que ceux du modèle ou du document à partir duquel vous importez des styles, cochez l’option ``Écraser``.
6. Cliquez sur le bouton OK. L’importation est terminée.


<div style="attention">
Attention

Vérifiez soigneusement les styles qui seraient censés être écrasés. Il se peut qu’ils ne soient pas écrasés systématiquement si vous avez de gros documents et un ordinateur avec une mémoire limitée. Si vous ne voulez pas remplacer les styles, ne sélectionnez pas cette option. Vous n’importerez que les styles dont les noms ne se trouvent pas dans le document courant.
</div>

### Gérer les modèles via un gestionnaire de fichiers

LibreOffice inclut toutes les fonctionnalités dont vous avez besoin pour interagir avec les modèles. Cependant, il peut arriver que vous souhaitiez gérer les modèles depuis l’extérieur de LibreOffice, soit parce que LibreOffice n’est pas ouvert, soit parce que vous traitez plus d’un modèle à la fois. Ou peut-être voulez-vous les organiser différemment en ajoutant des sous-dossiers dans les répertoires de modèles principaux.

Dans ce cas, vous pouvez utiliser un gestionnaire de fichiers. Le répertoire de stockage des modèles varie selon le système d’exploitation et la version du logiciel, mais vous pouvez trouver les répertoires que votre installation utilise en allant dans ``Outils > Options > Chemins > Modèles``.

Vous pouvez ajouter des répertoires au chemin en les séparant les uns des autres par un point-virgule. Si vous voulez rendre les répertoires accessibles à tous les utilisateurs du système, connectez-vous en tant que *root* ou administrateur et allez dans le répertoire dans lequel LibreOffice a été installé. Par exemple, si vous avez téléchargé une version GNU/Linux directement depuis le site de *The Document Foundation*, ce répertoire sera dans quelque chose comme ``/opt/libreoffice4.4/share/template``. Dans d’autres cas, les deux répertoires supérieurs peuvent être différents, mais les deux répertoires inférieurs devraient être les mêmes. 

Si vous installez un grand nombre de modèles, le moyen le plus rapide de les enregistrer consiste à tous les placer dans un seul répertoire, puis ajouter le chemin d’accès et redémarrer LibreOffice.

## Les bases complètes

Vous devriez maintenant avoir une idée générale du fonctionnement des styles et des modèles. Rien n’est difficile en théorie, bien que la manière dont les modèles sont gérés peut sembler inutilement compliquée, jusqu’à ce que vous réalisiez qu’elle vise à empêcher la corruption de fichiers.

Les chapitres suivants expliquent les points dont vous devez tenir compte pour concevoir des styles et des modèles utiles et esthétiques.







