# Les styles de pages

Les styles de page sont l’une des caractéristiques distinctives de Writer par rapport aux autres logiciels de traitement de texte. Cela lui donne une immense flexibilité. En effet, la conception des pages est au cœur de la typographie. Des livres entiers ont été écrits sur le sujet, dont beaucoup sont bourrés de théories absurdes renforcées par des diagrammes compliqués qui semblent souvent bien loin de la pratique. En fait, jusqu’à l’essor de la typographie numérique, la page était généralement l’unité principale de composition.

Heureusement, pour concevoir une page, vous n’avez pas besoin de croire en des théories obscures, et encore moins les suivre.
Le seul inconvénient est que la plupart des utilisateurs ne sont pas habitués à penser en termes de pages. Pourquoi devrait-on associer aux pages des numéros de page, des pieds de page et des en-têtes&nbsp;? Les autres traitements de texte ne font pas ce type d’association. En fait, ils n’offrent souvent aucun moyen de comprendre la cohérence d’une page. 

Et pourtant, quand on y pense, la page est vraiment l’endroit le plus logique pour de telles fonctionnalités. Au fil des années, LibreOffice a ajouté des outils pour faciliter la mise en forme des pages.

## Comprendre les conventions de mise en page

La mise en page est basée sur les deux pages, gauche et droite, parce que c’est ce que vous voyez quand vous lisez un livre, à moins que vous ne soyez sur la première ou, parfois, la dernière page. 

Même si votre document est destiné à être lu à l’écran, le visualiser en mode double page (ou livre) vous permet d’identifier rapidement les éventuels problèmes de conception.  C’est d’autant plus pertinent que les écrans larges sont de plus en plus courants.

Vous pouvez obtenir cette disposition en sélectionnant ``Affichage livre`` dans les commandes de zoom en bas à droite de la fenêtre d’édition. 

Traditionnellement, la première page d’un document est une page de droite, modifiée pour indiquer clairement qu’il s’agit du début. Ouvrez n’importe quel livre et vous verrez immédiatement pourquoi&nbsp;: à gauche se trouve la couverture ou la reliure.

Cela signifie également que les pages de droite sont généralement numérotées impaires, et les pages de gauche sont paires. Les documents maîtres ajoutent automatiquement des pages vierges pour que les chapitres commencent par une page de droite.

![Vue en mode livre.](Captures/CH8-vuelivregauchedroite.png)

## Appliquer des styles de page

On trouve la liste des styles de page en cliquant sur l’icône ``Styles de pages`` dans la fenêtre des ``Styles``.

En théorie, vous pouvez changer le style de la page à tout moment en plaçant le curseur sur une page et en faisant une sélection dans la fenêtre des ``Styles``. Cependant, cette pratique peut amener d’autres pages du document à changer de style (pas systématiquement, mais cela peut arriver). Mieux vaut dans ce cas utiliser un affichage qui démarque clairement le début et la fin d’une page (et non un affichage continu dont les sauts de page ne sont indiqués que par une ligne pointillée).

## Automatiser le style suivant

Lorsque vous travaillez, vous ne voulez pas sélectionner continuellement un style de page à chaque saut de page. Par conséquent, allez visiter le champ ``Style de suite`` de l’onglet ``Gestionnaire``.

Le modèle le plus simple est défini pour vous&nbsp;: une page de gauche est suivie d’une page de droite, et une page de droite d’une page de gauche. Si vous utilisez un style ``Première page``, son champ ``Style de suite`` par défaut est une page gauche. 

![Succession des pages.](Captures/CH8-1gdg.png)

Conservez cette disposition par défaut, et vous ne devriez penser à définir les styles de page que lorsque vous faites quelque chose d’inhabituel, comme l’ajout d’une page paysage à un document orienté en portrait. Dans ces circonstances inhabituelles, sélectionnez ``Insertion > Saut manuel > Saut de page`` et sélectionnez le style de page approprié. Dans d’autres cas, vous pouvez créer un style de paragraphe via l’onglet ``Enchaînement`` pour commencer une nouvelle page avec un style particulier. 

## Organiser les styles de page

Les modèles de page par défaut sont faits soit pour des dimensions spécifiques, comme ``Enveloppe`` et ``Paysage``, soit dans un but spécifique, comme ``Note de fin`` ou ``Index``.

Vous pouvez  également envisager d’ajouter des styles de page pour&nbsp;:

- la table des matières,
- la page de garde (ou de grand titre) et le colophon,
- un index alphabétique (si les entrées sont courtes, il peut économiser de l’espace en étant multi-colonnes),
- les notes de fin (ou en fin de chapitre), même si ce n’est pas conseillé,
- une page pour le faux-titre (généralement centrée),
- une épigraphe,
- un poème,
- un scénario (script),
- de la documentation juridique,
- une page spéciale, éventuellement orientée en paysage, pour des graphiques, des illustrations ou des diagrammes.

Pour certains documents, surtout les documents courts, le style par défaut est tout ce dont vous avez besoin. Les styles de page les plus basiques pour n’importe quel usage sont la page de gauche, la page de droite et la première page. Ce sont eux qui seront des modèles pour la plupart des autres styles de page.

## Conception des pages de gauche et de droite

La mise en page commence presque toujours par la page de gauche et la page de droite. Il s’agit le plus souvent d’images miroir l’une de l’autre. Vous pourriez choisir de n’utiliser qu’un seul de ces styles pour un document destiné à un affichage à l’écran, bien que de nombreux lecteurs préfèrent consulter des documents longs sur tablettes ou liseuses. Vous devriez également toujours envisager que le document puisse être imprimé&nbsp;: des en-têtes ou des pieds de page déséquilibrés peuvent distraire les lecteurs par une conception maladroite.

## Conception de la première page

La première page ne se réfère pas à une page de titre, mais à la page sur laquelle un document ou son premier chapitre commence. Une première page est habituellement une page de droite modifiée. Elle doit être suffisamment différente pour qu’il soit immédiatement évident qu’il s’agit du début d’une nouvelle partie du document.

Parfois, l’indicateur principal d’une première page peut être un en-tête ou un pied de page différent du reste des pages. C’est souvent trop peu&nbsp;: il est préférable de marquer clairement la première page que de risquer d’embrouiller le lecteur. Voici un panel d’indicateurs les plus évidents&nbsp;:

![Premières pages. Une marge supérieure, de 76 à 230 pts plus grande qu’une marge de page de droite normale. Un titre de chapitre et/ou un numéro, avec un ou deux styles différents. Une image ou un symbole au dessus du texte ou dans un cadre avant le premier paragraphe. Utilisation des lettrines.](Captures/CH8-indicprempage1234.png)




## Format du papier

Le format de page est défini dans l’onglet ``Page``. Par défaut il est généralement déterminé par votre région, et ne doit être modifié qu’en fonction de vos besoins ou de votre imprimante. Dans beaucoup de pays, le format standard est A4, mais en Amérique du Nord, le format standard est ``Letter``.

Quelle que soit la taille de la page, l’orientation standard est portrait (plus haut que large). L’alternative est le paysage (plus large que haut).

![L’onglet Page du style de page.](Captures/CH8-ongletstyledepage.png)

Tous les formats de page n’impliquent qu’une seule feuille de papier. Cependant, l’impression de plusieurs pages sur une feuille est plus économique. Vous pouvez configurer l’impression de plusieurs pages par feuille via ``Fichier > Imprimer``, ou bien en utilisant des blocs de texte au lieu de pages.

Si vous destinez votre document à un affichage à l’écran, ni le format papier ni l’orientation n’ont d’importance, à moins que le document ne soit produit au format PDF ou converti au format ebook.

<div style="astuce">

Astuce

A4 est un format de page standard dans la plupart des pays du monde, et Letter aux États-Unis et au Canada. A4 a une largeur de 595 points et une hauteur de 842 points, tandis que Letter a une largeur de 612 points et une hauteur de 792 points.
 
Si vous avez un public international, vous pouvez créer un modèle combiné A4/Letter&nbsp;:

1. Créez un format de page avec une largeur de 595 points et une hauteur de 792 points. Ce sont les plus petites mesures des deux formats. Pour être affichés, tous les contenus doivent se trouver dans cette zone.
2. Définissez les marges gauche et droite (ou intérieures et extérieures) comme si vous les conceviez pour le format A4, le plus étroit des deux formats. 
3. Définissez des marges supérieures et inférieures comme s’il s’agissait de Letter, la plus petite en hauteur des deux formats. 

Le résultat ne sera jamais tout à fait satisfaisant pour l’une ou l’autre taille, mais avec ce modèle, vous pouvez imprimer à partir du même fichier en deux tailles, plutôt que de créer à chaque fois deux fichiers avec le même contenu.
</div>

## Paramètres de mise en page

Les ``paramètres de mise en page`` dans l’onglet ``Page`` offrent quatre options de mise en page&nbsp;:

- Pages en vis-à-vis&nbsp;: le style de page est utilisé à la fois pour les pages paires et impaires avec des marges intérieures et extérieures définies dans la section ``Marges``.
- Droite et gauche&nbsp;: le style de page est utilisé à la fois pour les pages paires et impaires avec des marges de droite et de gauche définies dans la section ``Marges``.
- Droite uniquement&nbsp;: le style de page concerne seulement les pages impaires.
- Gauche uniquement&nbsp;: le style de page concerne seulement les pages paires.

<div style="attention">

Attention

Le champ ``Mise en page`` peut être écrasé par la sélection ``Style suivant`` dans l’onglet ``Gestionnaire``. Normalement, ce remplacement signifierait que vous pourriez ignorer le champ Mise en page. 

Cependant, certaines combinaisons peuvent causer des pages blanches ou d’autres problèmes de formatage. Pour cette raison, vous devez vous assurer qu’il n’y a pas de conflit, comme une sélection ``Style suivant`` qui force un style de page défini à gauche seulement à apparaître sur une page de droite. De même, un style de page créé dans une version de LibreOffice peut parfois ne pas s’afficher comme vous l’aviez prévu dans une autre version.
</div>

## Les marges

Les marges sont définies dans l’onglet ``Page`` d’un style de page. Les marges ont trois objectifs&nbsp;:

- permettre aux lecteurs de tenir une copie papier sans cacher une partie du texte avec leurs doigts,
- offrir aux lecteurs un espace pour ajouter des commentaires,
- cadrer le document de manière discrète afin que les lecteurs puissent se concentrer sur son contenu.

<div style="attention">
Le réglage par défaut des marges dans Writer est de 57 points (environ 3/4 de pouce ou 2 centimètres). Dans presque tous les cas, il s’agit d’un minimum pour une marge digne de ce nom.
</div>

### Proportions des marges

Les théories sur les marges idéales portent à peu près sur tout, depuis la façon dont les rames de papier ont été pliées pour faire des livres au Moyen Âge jusqu’aux analogies musicales, avec des diagrammes dont on suppose qu’ils sont bien capables d’invoquer des démons.

Une théorie populaire soutient que les marges idéales sont basées sur le nombre d’or, un ensemble de proportions décrites par Euclide et réputées les plus naturelles pour l’œil humain.

L’application du nombre d’or aux marges de page signifie que, en commençant par la marge extérieure, puis les marges intérieures et inférieures, les proportions des marges devraient être de 2:3:4:6. Vous pourriez aussi commencer par la marge intérieure, mais le résultat est souvent une marge étroite qui peut réduire exagérément la reliure.

Ce ratio signifie que, en utilisant une unité de base de 20 points, la marge intérieure serait de 40 points, la marge supérieure à 60 points, la marge extérieure à 80 points et la marge inférieure à 120 points.

Ces proportions produisent une page agréable, mais les éditeurs ont tendance à considérer ces mesures comme du gaspillage, surtout avec une grande marge inférieure. 

Pour être honnête, le seul moment où vous verrez probablement le nombre d’or utilisé pour formater les pages est dans la poésie et les petits tirages de prose, pour lesquels les imprimeurs sont prêts à publier des livres assez chers en échange d’une page esthétique. 

En fin de compte, le nombre d’or est davantage dédié au prestige qu’à la pratique. Si toutefois vous décidez de l’utiliser, choisissez un papier épais et de qualité et une police simple mais élégante pour le corps du texte du document.


Un ensemble de règles plus souples peut être distillé à partir des différentes théories&nbsp;:

- la marge supérieure devrait être plus grande que la marge intérieure,
- la marge extérieure devrait être égale ou supérieure à la marge intérieure,
- la marge inférieure devrait être plus haute que la marge supérieure,
- toutes les marges doivent être des multiples de l’interligne. 
- vous pouvez trouver d’autres jeux de mesure, mais ces principes reflètent les pratiques les plus courantes. Tout aussi important, ils sont assez flexibles pour que, contrairement au nombre d’or, vous puissiez les utiliser sans augmenter le coût d’impression.

### Reliure et massicotage

Si votre document doit être imprimé et relié, la marge intérieure nécessite un espace supplémentaire pour la reliure. Cet espace donnera l’impression que les deux pages sont asymétriques à l’écran, mais empêche le texte de disparaître dans la gouttière (la zone d’une page où les bords sont reliés) du produit fini.

L’espace exact nécessaire pour la reliure varie selon le type de reliure et les accessoires choisis. Par exemple, un livre relié en spirale peut utiliser des bobines de différentes tailles.

Vous devriez consulter votre imprimeur, mais dans la plupart des cas, vous aurez probablement besoin d’au moins 20 à 45 points supplémentaires sur la marge intérieure pour la reliure… peut-être plus. 

<div style="astuce">

Astuce

Si vous n’êtes pas sûr du type de reliure que vous utiliserez, une marge intérieure trop large est plus facile à ajuster qu’une marge trop étroite.

Par ailleurs, selon le procédé d’impression, il se peut que vous ayez besoin d’espace supplémentaire sur les autres marges pour le découpage (fond perdu). Consultez votre éditeur et votre imprimeur au besoin.
</div>

## En-têtes et pieds de page

Les en-têtes et les pieds de page sont des espaces en haut et en bas de la page qui contiennent des informations sur le document. Dans Writer, leur espacement s’ajoute à la marge dont ils ne font pas partie. 

Contrairement à Microsoft Word, dans Writer, les en-têtes et les pieds de page font partie d’un style de page. Même lorsque vous formatez manuellement à partir de ``Insertion > En-tête et Pied de page``, votre sélection est basée sur les styles de page prédéfinis. 

Les en-têtes et les pieds de page apparaissent souvent ensemble, mais n’en utiliser qu’un seul est tout aussi courant. En règle générale, chaque en-tête ou pied de page contient jusqu’à trois éléments d’information.

![Onglet pour l’en-tête.](Captures/CH8-ongletentete.png)

<div style="attention">

Attention

Vous pouvez utiliser un seul pied de page, puis décocher les cases ``Même contenu sur les pages de droite et de gauche`` et ``Même contenu sur la première page`` plutôt que de concevoir des styles de page séparément. Cependant, n’utilisez pas les deux méthodes ensemble. Il peut en résulter des conflits de mise en page insolubles.
</div>

### Activer les en-têtes et les pieds de page

Pour ajouter un en-tête ou un pied de page, allez à l’onglet ``En-tête`` ou ``Pied de page`` dans la boite de dialogue du style de page et sélectionnez la case ``Activer l’en-tête`` ou ``Activer le pied de page`` en haut. 

Le résultat immédiat est l’apparition d’une seule ligne de texte dans la marge supérieure ou inférieure. Avec LibreOffice, vous devez cliquer n’importe où dans la marge pour que les guides soient visibles. Bien que vous puissiez ajouter des lignes supplémentaires, la pratique courante consiste à n’utiliser qu’une seule ligne.

### Choisir les tabulations ou un tableau

De nombreux utilisateurs utilisent simplement les tabulations pour ajouter des informations dans l’en-tête ou le pied de page, en faisant en sorte d’espacer suffisamment les informations sur la largeur de la ligne.

Cependant, cet arrangement peut nécessiter un réajustement constant au fur et à mesure que vous révisez le document, surtout si vous utilisez des champs ou si vous modifiez la taille des caractères, ce qui rend la tâche plus difficile qu’autre chose.

Une solution plus robuste consiste à ajouter un tableau à une seule rangée avec des bordures de cellules invisibles et autant de colonnes que nécessaire. Cette technique signifie aussi moins de travail. Vous apprécierez particulièrement la commodité des tableaux si votre document est susceptible de durer plusieurs années et à travers plusieurs versions de LibreOffice.

<div style="astuce">
Astuce

Si vous créez un tableau d’une ligne avec la fonction ``Tableau > Insérer un tableau``, vous aurez l’inconvénient de voir persister la ligne sous le tableau, ce qui implique l’occupation de cet espace vertical supplémentaire et malvenu pour votre en-tête ou votre pied de page.

La solution consiste à utiliser la fonction ``Tableau > Convertir > Texte en tableau``. Vous écrivez d’abord les éléments de votre tableau en les séparant, par exemple par un point-virgule ou une tabulation, puis lors de la conversion vous précisez quel est l’élément séparateur. Vous décochez l’en-tête et la bordure pour le tableau, n’utilisant ainsi que le format de tableau au strict minimum.

Le tableau qui en résultera utilisera alors tout la ligne sans la «&nbsp;pousser&nbsp;» sous lui. 
</div>


### Mise en page des en-têtes et des pieds de page

Sous la case d’activation de l’en-tête ou du pied de page se trouvent les options de mise en page&nbsp;: 

- Marge gauche et marge droite&nbsp;: elles s’ajoutent aux marges de la page. Elles peuvent être définies à zéro, puisque les en-têtes et les pieds de page sont rarement plus indentés que le corps du texte.
- Espacement&nbsp;: la distance entre l’en-tête ou le pied de page et le bloc de texte principal. Ce champ doit être un multiple de la hauteur de l’interligne. Si la distance est suffisamment grande, vous n’ayez pas besoin d’une ligne ou d’un autre type de séparateur pour séparer l’en-tête ou le pied de page du corps du texte.
- Hauteur&nbsp;: la hauteur de la ligne du pied de page ou de l’en-tête. Souvent, il s’agit de la hauteur de ligne du corps de texte. Si vous utilisez le pied de page ou l’en-tête pour inclure une image récurrente, la hauteur devra être au moins égale à la hauteur de l’image.

<div style="astuce">

Astuce

Vous ne pouvez pas entrer un nombre négatif pour faire en sorte qu’un en-tête ou un pied de page s’étende dans la marge gauche ou droite. Si toutefois vous voulez cet arrangement, vous devez donner à vos styles de paragraphe (corps de texte et autres) des retraits à gauche et à droite, de sorte qu’ils utilisent une ligne plus courte que l’en-tête ou le pied de page.
</div>

#### Bordure et arrière-plan

Les onglets ``Bordure`` et ``Arrière-plan`` pour les en-têtes et les pieds de page sont remplis de paramètres qui doivent être utilisés avec prudence.

Pour commencer, à moins que vous ne cherchiez un look des années 1990 ou que vous pratiquiez délibérément une mauvaise typographie, ignorez les paramètres pour ajouter une ombre au pied de page ou à l’en-tête. 

Il en va souvent de même pour l’arrière-plan. L’utilisation est déconseillée à l’exception de la délicate question du contraste entre la page et l’en-tête ou le pied de page, ou si l’en-tête ou le pied de page doivent comporter un élément graphique. Éventuellement, utilisez une transparence pour rendre le fond plus subtil qu’une couleur unie.

La plupart du temps, vous pouvez évacuer tout autre élément de décoration, à l’exception d’une éventuelle ligne fine entre l’en-tête ou le pied de page et le bloc de texte. Vous pouvez aussi colorer cette ligne. 

Pour cela, rendez-vous dans l’onglet ``Bordures`` et configurez une bordure supérieure ou inférieure. Si votre en-tête ou votre pied de page  semblent à l’étroit avec cette bordure, configurez aussi la zone ``Remplissage`` qui vous permettra de définir l’espacement entre la ligne de bordure et le texte.

Cela dit, si l’espacement est assez grand entre le bloc de texte et l’en-tête ou le pied de page, même cette ligne est inutile.

#### Styles de paragraphe dans les en-têtes et pieds de page

À ce stade, vous voudrez peut-être revoir les styles de paragraphe pour les en-têtes et les pieds de page. Dans la fenêtre des ``Styles``, des styles par défaut sont disponibles&nbsp;: ``En-tête droit``, ``En-tête gauche``, ``Pied de page droit``, ``Pied de page gauche``. Cependant, vous pouvez également créer ``En-tête centre`` et ``Pied de page centre``. 

Quelle que soit la disposition des en-têtes et des pieds de page, leurs informations doivent avoir une taille de police similaire à celle du corps du texte. Le numéro de page, éventuellement, peut être plus grand. Rendre le texte plus petit que le corps du texte ne fait que réduire l’utilité des en-têtes et des pieds de page. 

#### Contenus des en-têtes et pieds de page

Le contenu des en-têtes et des pieds de page comprend généralement des informations statiques telles le titre du document, le titre du chapitre et le nom de l’auteur. Mais d’autres contenus pertinents peuvent être définis en utilisant ``Insertion > Autres champs``.

Pendant que vous écrivez, vous pouvez aussi utiliser des contenus temporaires, en particulier ceux que l’on trouve dans les champs ``Insertion > Autres champs > Document``, comme le nombre de mots ou le nom du modèle. À la fin de votre travail, vous pourrez supprimer ou remplacer ces éléments temporaires.


Le tableau ci-dessous suggère des champs intéressants.


| Document             | Info document       |
|----------------------|---------------------|
| Utilisateur          | Titre               |
| Nom de fichier       | Numéro de révision  |
| Modèles              | Modifié             |
| Expéditeur           | Dernière impression |
| Statistique (mots)   | Créé                |
| Page                 |                     |
| Chapitre             |                     |



![Les champs sont des sources de contenus pour les en-têtes et pieds de page.](Captures/CH8-champspourentetepied.png)

#### En-têtes et pieds de page courants

Les en-têtes (ou pieds de page) peuvent être configurés en fonction de la dernière instance d’un style de paragraphe particulier. De cette façon, ils permettent de fournir un guide pour les lecteurs qui cherchent à localiser un passage.

Par exemple, vous pourriez avoir un en-tête pour les pages de gauche qui reprend le texte du style de titre de niveau 1 et un en-tête de page de droite qui reprend le texte du titre de niveau 2, de manière à aider les lecteurs à voir où ils se trouvent dans le chapitre.

Pour configurer un en-tête ou un pied de page courant&nbsp;:

1. Configurez un style de paragraphe dans l’onglet ``Enchaînement`` pour qu’il démarre une nouvelle page. Donnez-lui un nom facile à retenir.
2. Dans ``Outils > Numérotation des chapitres > Numérotation``, affectez le style de paragraphe que vous utilisez à un niveau de titre si vous ne l’avez pas déjà fait.
3. Placez le curseur de la souris dans l’en-tête ou le pied de page.
4. Sélectionnez ``Insertion > Champs > Autre champs > Document > Chapitre > Nom de chapitre``. Cliquez ensuite sur le bouton Insérer. 
5. Répétez les étapes 3 et 4 pour chaque style de page du document qui inclut ces indications de chapitrage.

#### En-têtes et pieds de page verticaux

Les en-têtes et les pieds de page sont généralement horizontaux. Cependant, vous pouvez trouver sur les pages au format paysage que la longueur des en-têtes et des pieds de page rend la lecture difficile.

Vous pouvez également choisir des en-têtes et pieds de page verticaux sur les pages en mode portrait pour donner un effet original.

L’inconvénient des en-têtes et pieds de page verticaux est que les styles de page ne se répètent pas automatiquement sur chaque instance d’un style de page, comme ils le font pour les styles horizontaux. Au lieu de cela, vous devez les recréer sur chaque instance, ou les copier-coller. Ces choix tendent à limiter ce choix à des documents plus courts.

Pour créer des en-têtes et pieds de page verticaux&nbsp;:

1. Créez le texte de l’en-tête ou du pied de page en utilisant le style de paragraphe approprié et en ajoutant des champs. Positionnez-le à peu près au milieu de la page.
2. Sélectionnez la ligne qui deviendra un en-tête ou un pied de page et sélectionnez ``Caractère > Position > Rotation/Echelle > 90 degrés`` ou ``270 degrés``. 90 degrés positionne le texte de sorte qu’il commence en bas et se poursuive vers le haut, tandis que 270 degrés positionne le texte de sorte qu’il commence en haut et se poursuive vers le bas.
3. Sélectionnez la ligne maintenant verticale et cliquez sur ``Insertion > Cadre`` pour la placer dans un cadre.
4. Placez le cadre à l’extérieur de la marge intérieure ou extérieure, où il servira d’en-tête ou de pied de page. Activez la grille via ``Affichage > Grille et lignes guide > Afficher la grille`` pour vous aider à positionner le cadre.
5. Dimensionnez le cadre de façon à ce qu’il occupe tout l’espace entre les marges supérieure et inférieure. Pour être exact, vous pouvez soustraire les marges supérieure et inférieure de la hauteur de la page pour obtenir la taille exacte du cadre. Cependant, avec un zoom suffisamment élevé, les limites du texte peuvent suffire à vous guider.
6. Cliquez avec le bouton droit de la souris sur le cadre et sélectionnez ``Propriétés`` dans le menu contextuel pour ajouter des bordures ou des ombres, ou pour les désactiver.
7. Répéter les opérations selon vos besoins.


## Numéros de page

``Insertion > Champs`` inclut le numéro de page dans son sous-menu. Vous pouvez insérer un numéro de page en fonction du réglage du style de page en cours dans le champ ``Page > Paramètres de mise en page > Numéros de page``. Vous pouvez ensuite aligner le numéro de page en modifiant son style de paragraphe.

Le sous-menu comprend également un champ ``Nombre de pages`` que vous pouvez 
associer au numéro de page de manière à obtenir des occurrences du genre «&nbsp;Page 2 de 3&nbsp;» ou «&nbsp;Page 2/3&nbsp;», etc.

Cependant, pour obtenir le contrôle total de la numérotation des pages, vous devez aller dans ``Insertion > Champs > Autres champs > Document > Page``. Là, vous pouvez compléter le réglage du style de page par un autre format de numérotation choisi dans le volet de droite. Par exemple, vous pouvez commencer par une introduction numérotée en chiffres romains, puis continuer avec le corps du texte numéroté en chiffres arabes.

### Réinitialiser les numéros de page

Le scénario le plus courant dans lequel la numérotation des pages redémarre est lorsqu’un document commence par une introduction (généralement numérotée en chiffres romains minuscules), puis continue avec le reste du texte (généralement numéroté en chiffres arabes).

Pour réinitialiser le compteur des pages&nbsp;:

1. Créer des styles de page Introduction-Début, Introduction-Gauche et Introduction-Droite, modélisés sur le style de page par défaut. Sur chacun de ces styles personnalisés, définissez ``Page > Paramètres de mise en page > Numéros de page`` avec des chiffres romains en minuscules.
2. Immédiatement avant la page sur laquelle le compteur de pages redémarre et le corps principal du texte commence, sélectionnez ``Insérer > Saut manuel``.
3. Sélectionnez ``Saut de page`` pour le type.
4. Sélectionnez le style de page à utiliser après le saut. Vous voudrez probablement utiliser ``Première page`` ou le style par défaut.
5. Sélectionnez ``Modifier le numéro de page`` et réglez le numéro, généralement à 1.

Attention&nbsp;: on peut être tenté de soustraire le nombre de page *ad hoc* dans ``Insertion > Champs > Autres champs > Document > Page`` pour obtenir 1 sur la première page du texte principal. Le numéro de page dans le pied de page sera alors correct, mais il sera faux dans al table des matières et dans les index, car il ne sera pas corrigé.


### Ajout de numéros de chapitre aux numéros de page

Les documents techniques recommencent parfois la numérotation à chaque chapitre. Dans ce système, par exemple, la troisième page du chapitre 5 porte le numéro de page 5-3. 

Ce style peut être utilisé de sorte que, lorsque des révisions sont publiées, les utilisateurs peuvent remplacer qu’un seul chapitre au lieu de l’ensemble du document —&nbsp;un arrangement particulièrement utile avec les classeurs à anneaux.

Pour inclure le chapitre dans la numérotation des pages&nbsp;: 

1. Configurez un style de paragraphe dans l’onglet ``Enchaînement`` pour qu’il démarre une nouvelle page.
2. Si nécessaire, dans ``Outils > Numérotation des chapitres > Numérotation``, affectez le style que vous utilisez au Titre de niveau 1. Dans la zone ``Après``, ajoutez un séparateur, tel qu’un trait d’union.
3. Placez le curseur à la position du numéro de page.
4. Sélectionnez ``Insertion > Champs > Autres Champs > Document > Chapitre > Numéro de chapitre``. Cliquez ensuite sur le bouton Insérer. 
5. Sélectionnez ``Insertion > Champs > Autres champs > Document > Numéros de page`` (ou simplement ``Insertion > Champs > Numéro de page``) pour ajouter le numéro de page. 
6. Suivez les étapes 3 et 5 pour chaque style de page du document qui inclut la numérotation des pages de chapitre.




## Utiliser des colonnes

Les colonnes multiples sont souvent utilisées pour les bulletins d’information et pour imprimer des index ou d’autres tableaux dont les entrées nécessitent rarement une ligne entière.

![Onglet colonne pour un style de page.](Captures/CH8-pagestylecolonnes.png)

Pour les formats de page les plus courants (papier A4 ou Letter) avec une orientation portrait, vous avez de la place pour un maximum de 4 à 5 colonnes, à moins que certaines d’entre elles soient extrêmement étroites.

<div style="attention"> 

Attention

L’utilisation de colonnes peut rendre beaucoup plus difficile la gestion des césures. Soyez attentif à cela.
</div>

Dans l’onglet ``Colonnes``, vous pouvez définir le nombre de colonnes en saisissant un nombre exact plus quelques options de mise en page. Dans la plupart des cas, toutes les colonnes ont la même largeur.

Si vous préférez, vous pouvez définir la largeur de chaque colonne séparément dans la section ``Largeur et espacement``. Évidemment, le total de toutes les colonnes doit être égal au total disponible.

La sous-section ``Espacement`` permet de définir l’espacement entre les colonnes. Plus l’espacement est grand, plus le texte sera facile à lire. 

N’utilisez un ``Trait de séparation`` que si l’espacement est extrêmement serré et lorsque s’en remettre aux seuls espaces blancs rendrait la lecture difficile. En effet, l’ajout d’une ligne entre les colonnes implique que votre mise en page ne fonctionne pas. 

<div style="astuce"> 

Astuce


Il n'est pas facile de combiner colonne simple et multi-colonne. Writer est conçu pour des pages uniformes. Par conséquent, vous ne pouvez pas créer un style de page avec plus d’un format de colonne.

Si vous avez besoin d’une telle mise en page, commencez par un modèle à une seule colonne, puis ajoutez une section à plusieurs colonnes. Malheureusement, vous devrez ajouter une telle section à chaque utilisation.

Une autre option est d’ajouter des cadres manuels et de définir les enchaînements  que vous voulez.
</div>


## Notes de bas de page

Les notes de bas de page sont positionnées automatiquement par LibreOffice. Cependant, vous pouvez contrôler une grande partie de la mise en page des notes de bas de page via l’onglet ``Note de bas de page`` d’un style de page.

Le réglage le plus important est la hauteur de la zone des notes de bas de page. Vous pouvez définir une hauteur maximale.

En règle générale, on peut compter au minimum deux lignes d’espacement pour le texte, et au moins une ligne vide entre la ligne de séparation et le contenu de la note de bas de page.

Le séparateur doit être le plus fin et le plus court possible. Avec un espacement approprié, il se peut qu’il ne soit pas nécessaire du tout. Toutefois, veillez à ce que le séparateur dédié au pied de page et celui de la note de bas de page ne finissent par produire deux lignes à la suite.

![Onglet pour les notes de bas de page dans un style de page.](Captures/CH8-ongletnotebasdepage.png)


## Arrière-plan de la page


Les fonds sont utiles soit pour marquer une partie du document (par exemple un encart), soit pour des mises en page courtes comme des brochures. Si vous n’avez pas de raison précise pour ajouter un fond, alors vous pouvez être sûr qu’il n’est pas nécessaire.

Le problème est que les fonctionnalités d’arrière-plan sont assez limitées. En particulier, ils ne couvrent que l’espace à l’intérieur des marges. Certes, vous pouvez mettre toutes les marges à zéro, mais vous aurez besoin d’une imprimante capable de gérer des fonds perdus. Sur la plupart des imprimantes domestiques, vous aurez encore environ un centimètre autour du bord de la page où vous ne pouvez pas imprimer. Cette zone de non-impression limite sérieusement l’intérêt de l’arrière-plan.



![Onglet pour l’arrière-plan.](Captures/CH8-ongletarriereplan.png)

D’autre part, pour un document destiné à l’impression, changer le fond du blanc par défaut peut causer une lenteur d’impression et un gaspillage d’encre. Une solution plus efficace est d’imprimer sur du papier de couleur.

Si vous persistez à utiliser des fonds malgré ces limitations, le principe de base reste le même que pour les paragraphes ou toute autre élément de mise en page qui peut utiliser un fond.

<div style="astuce">
Astuce

Une couleur ou un fond graphique n’a pas besoin de bordure, à moins qu’un de ses bord ne se fonde dans la couleur du papier. Lorsque cela se produit, utilisez une bordure minimale et n’ajoutez jamais d’ombre, sauf pour un look rétro.
</div>

## Vers une nouvelle dimension

Les utilisateurs d’autres traitements de texte qui viennent à utiliser LibreOffice  négligent parfois les styles de page. Or, ils permettent d’ajouter facilement les informations courantes comme les numéros de page, les en-têtes et les pieds de page.

Tout aussi important, ils vous aident à concevoir de grands blocs, comme les premières pages de chapitre, et à gérer les enchaînements de styles de page. Cette capacité vous donne un atout que la plupart des logiciels de traitement de texte n’ont pas. Vous voudrez peut-être utiliser des sections pour des blocs un peu plus petits, mais une fois que vous serez à l’aise avec les styles de page, vous trouverez probablement peu d’intérêt aux sections, à moins que vous ne souhaitiez protéger une partie de votre document par mot de passe.

Prenez le temps de comprendre les styles de page, et vous découvrirez que Writer se suffit à lui seul.
