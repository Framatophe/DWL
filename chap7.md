# Positionnement et automatisation du texte

Ce chapitre concerne les fonctionnalités avancées des styles de caractères et de paragraphes. Il explique la relation entre les styles de paragraphe et de liste, comment positionner les caractères avec précision et quels moyens sont à disposition pour automatiser votre travail à l’aide des styles.

On néglige souvent les fonctions d’automatisation, mais elles peuvent être aussi importantes que les procédés de mise en forme. Par exemple, un style conditionnel vous permet de mettre en forme différemment le même style en fonction d’un contexte, ou de définir un style de paragraphe pour qu’il commence toujours sur une nouvelle page.

Bien que les utilisateurs occasionnels connaissent rarement ces fonctions, ceux qui écrivent dans le cadre de leur travail seront bientôt tout à fait conscients du temps que ces fonctions leur permettent d’économiser.

Par exemple, l’effet de caractère ``Masqué`` peut sembler trivial. Mais il offre pourtant une solution élégante pour l’une des tâches les plus difficiles des documentalistes professionnels&nbsp;: maintenir plusieurs versions d’un même document dans un seul fichier.

## Réglage fin des caractères

Comme la plupart des logiciels de traitement de texte ou de mise en page, LibreOffice assure une grande partie des tâches ingrates de la conception de document.

![L’onglet Position permet d’ajuster les caractères.](Captures/CH7-stylecaractereposition.png)


Par exemple, sans consulter l’utilisateur, Writer examine les fichiers de polices pour afficher correctement les caractères. Il détecte également si une famille de polices comprend des italiques ou des graisses, et décide de la taille et de l’emplacement des numéros de notes de bas de page. La plupart des utilisateurs sont heureux de laisser LibreOffice prendre seul ces décisions. Cependant, elles ne sont pas toujours idéales, de sorte qu’à l’occasion, on peut modifier l’espacement entre les caractères ou repositionner les numéros de notes de bas de page.

LibreOffice inclut les outils dont vous avez besoin pour ce genre d’ajustements. On les trouve surtout dans l’onglet ``Position`` des styles de caractères. D’autres sont éparpillés dans les boites de dialogues de styles de caractères et de paragraphes.

### Exposant et indice

L’onglet ``Position`` vous offre plusieurs possibilités pour ajuster les caractères en exposant (notation supérieure à droite et de corps plus petit) et en indice (notation inférieure à droite et de corps plus petit).

Une alternative consisterait à ajuster l’alignement vertical d’un ou plusieurs caractères sur la même ligne. Cette pratique est en réalité assez courante, car, selon la police, les caractères en exposant et en indice tels que LibreOffice les défini par défaut peuvent parfois être trop petits pour la lecture.

Pour comprendre les caractères en exposant et en indice, il faut se rappeler que toutes les lettres se trouvent sur une ligne de base (ou ligne de pied) imaginaire. On mesure les caractères en fonction de la hauteur d’x et de l’œil (voir le chapitre 4). Les caractères ont des ascendantes (comme la barre verticale du t) et des descendantes (comme la barre verticale du p), c’est-à-dire des parties de glyphes qui s’étendent en dessous ou au-dessus de la hauteur d’x.

Ainsi, les caractères en exposant, comme certaines composantes mathématiques ou les abréviations des ordinaux, sont habituellement placés quelque part entre la hauteur d’x et la hauteur des ascendantes. De même, les caractères en indice, comme ceux des formules chimiques, se situent généralement entre la ligne de base et le point bas des descendantes.

La taille des caractères en exposant et en indice est un compromis entre la facilité de lecture et la position.

![Exposant et indice.](Captures/CH7-exposantindeice.png)

Lorsque vous ajustez à la fois la position et la taille, attendez-vous à faire quelques essais et erreurs avant d’obtenir les meilleurs résultats.

Pour orienter vos expériences, considérez ces points&nbsp;:

- La taille exacte varie en fonction de l’espace blanc de la police, mais 40% à 60 % du corps du texte devrait être un ratio convenable.
- Si les caractères d’une police utilisent beaucoup d’espace blanc, de sorte qu’ils paraissent petits par rapport aux polices de même taille, augmentez la taille.
- À moins que la hauteur d’x ne soit exceptionnellement grande, l’utiliser comme repère pour placer le bas des caractères en exposant permet souvent d’obtenir un design cohérent.
- Évitez les caractères de style ancien pour l’exposant et l’indice. La rupture de style signifie soit un encombrement à la lecture, soit un positionnement chronophage pour obtenir une mise en forme cohérente.
- Aligner les caractères en exposant avec le haut des ascendantes et les caractères d’indice avec le bas des descendantes présente l’avantage d’être symétrique tout en vous donnant un repère.

<div style="attention">
Attention

L’onglet ``Position`` pour les styles de caractères et de paragraphe s’applique spécifiquement aux blocs de texte. Pour créer des formules, ouvrez ``Fichier > Nouveau > Formule`` ou utilisez l’application ``Maths``.
</div>

### Aligner des textes de tailles différentes

L’option ``Texte à texte`` de l’onglet ``Alignement`` permet d’aligner verticalement des textes de tailles différentes sur la même ligne. On l’utilise plutôt lorsqu’on réalise une brochure ou une affiche, mais on peut aussi, de cette manière, créer des caractères en exposant ou en indice sans avoir à utiliser l’onglet ``Position``.
 
Le réglage de l’alignement propose le choix de repérage entre la ligne de base, et (par rapport à la hauteur d’x) le milieu, le haut ou le bas. Dans chaque cas, les caractères les plus grands restent sur la ligne de base, tandis que les autres caractères sont ajustés par rapport à eux. Par exemple, si vous sélectionnez ``Bas``, les petits caractères sont positionnés au bas des descendantes des plus gros caractères. De même, avec ``Haut``, les plus petits caractères sont positionnés en haut des ascendantes des plus grands caractères.

Cependant, la plupart du temps, vous pouvez laisser le réglage sur ``Automatique``, qui l’est par défaut, de sorte que toutes les tailles de police différentes sont alignées sur la ligne de base.

![Régler l’alignement texte-à-texte.](Captures/CH7-haubasmilieu.png)


### Rotation du texte

L’onglet ``Position`` pour le style de caractère inclut des paramètres de rotation du texte&nbsp;: 90 degrés (angle droit par rapport à la ligne de base et au-dessus) et 270 degrés (angle droit par rapport à la ligne de base et en dessous). Ces paramètres sont surtout utiles dans un en-tête de tableau, mais tous deux interfèrent avec la lisibilité et ne devraient pas être utilisés s’il existe une meilleure solution.

### Largeur de police

La largeur des caractères résulte de l’interprétation du fichier de  chaque police par LibreOffice. Vous pouvez l’ajuster en utilisant le champ ``Échelle de Largeur`` dans l’onglet ``Position``. Cette fonction est particulièrement utile lorsque vous n’avez pas de version condensée ou étendue dans une famille de polices.

![Largeurs réglées à 100%, 115%, et 85%. Les variations peuvent parfois être maladroites avec les polices de petites taille.](Captures/CH7-echellelargeurtexte.png)

Prenez en compte qu’un typographe à l’ancienne devait modifier toute sa collection de caractères en plomb (et encore bien d’autres choses) s’il voulait modifier la largeur de la police qu’il utilisait. Même avec une composition numérique, ce type d’ajustement bouleverse la mise en page et peut créer de grands désordres. Les polices bien faites ont été pensées par des professionnels, ne l’oubliez pas avant de les modifier artificiellement. 

On peut raisonnablement affirmer que la plupart des polices peuvent supporter 1% à 15 % d’ajustements en moins ou en plus de la valeur par défaut (100%) sans trop se détériorer. Ces ajustements peuvent aider à améliorer la couleur de la page du corps du texte.

### Espacement entre les lettres (crénage)

Le crénage est l’ajustement de l’espacement entre les lettres. La police doit s’y prêter. Aussi, une police à chasse fixe ne peut faire l’objet de crénage. Les imprimeurs professionnels ajustent parfois le crénage pour améliorer l’apparence des combinaisons de lettres et mieux répartir les blancs. Par exemple, les combinaisons telles que «&nbsp;Va&nbsp;», «&nbsp;ll&nbsp;» et «&nbsp;ff&nbsp;» peuvent être améliorées dans la plupart des polices.

![Exemples de crénage.](Captures/CH7-vallff.png)

On peut modifier l’espacement entre les caractères à l’aide du champ ``Espacement`` de l’onglet ``Position``. Dans l’illustration&nbsp;:

- on optimise l’espacement entre les caractères V et a,
- on crée notre propre ligature avec la paire de f,
- on augmente l’espacement pour améliorer la lisibilité entre les deux l.

Le crénage a toujours été une préoccupation dans la typographie, mais la typographie numérique la rend plus importante que jamais. Contrairement à la typographie manuelle, les polices numériques n’ont généralement pas d’espacement différent lorsque la taille de la police change. L’espacement est prévu pour une taille standard. 

Par conséquent, si vous diminuez ou augmentez considérablement la taille de la police, le crénage peut être désactivé. Ce qui est prévu pour 12 points peut ne pas fonctionner pour 8 ou 48 points.

De plus, le crénage dans LibreOffice a tendance à être très lâche, et vous pouvez souvent l’améliorer. Si vous choisissez de gérer votre propre crénage, créez des styles de caractères avec un espacement ajusté. Vous pouvez créer un style de caractère comprenant vos paramètres de crénage, mais si vous êtes vraiment attentif aux détails, vous pouvez aussi décider de créer des styles de caractères avec des crénages différents pour différentes combinaisons de lettres —&nbsp;tout dépend de votre patience, de la police que vous utilisez et de votre perfectionnisme.

Pour autant, vous pouvez modifier l’espacement de manière ponctuelle, soit pour améliorer la lisibilité, soit pour une courte chaîne de caractères dans un document plus graphique, comme une brochure.

Quelles que soient vos intentions, vous pourrez sélectionner la case ``Paire de crénage`` à côté du champ de manière à ajuster automatiquement le crénage pour la combinaison de certaines lettres (en particulier selon les ligatures).

### Fabrication de petites capitales

Les petites capitales sont conçues pour améliorer l’apparence de deux lettres majuscules ou plus à la suite. Bien qu’elles doivent être appliquées dans des cas précis, les petites capitales sont particulièrement utiles pour améliorer la lisibilité d’un texte comprenant beaucoup d’abréviations.



![Petites capitales authentiques, petites capitales fabriquées et capitales normales. Comparez la lettre A entre les échantillons&nbsp;: les petites capitales ne sont pas seulement des capitales en taille réduite. Police&nbsp;: Linux Libertine G.](Captures/CH7-capitales.png)


Lorsqu’une police n’inclut pas son propre jeu de petites capitales, LibreOffice crée une imitation de celles-ci, les rendant généralement plus petites que les capitales ordinaires. Cependant, ces imitations sont rarement adéquates, car les vraies petites capitales ne se distinguent pas seulement par leur taille, mais par une refonte complète des caractères.

Cela dit, toutes les polices ne sont pas accompagnées d’un jeu de petites capitales, ce qui vous oblige parfois à créer votre propre police.

### Comment faire

Si une police manque de petites capitales, LibreOffice en fabrique. Cependant, il est parfois possible d’améliorer ce rendu. Suivez ces étapes pour fabriquer vous-même des petites capitales&nbsp;:

1. Utilisez les capitales normales pour vos expériences.
2. Commencez par un style de caractère de plusieurs points plus petit que la police de paragraphe avec laquelle il sera utilisé. Expérimentez jusqu’à ce que vous trouviez une taille convenable.
3. Utilisez le champ ``Échelle de largeur`` de l’onglet ``Position`` pour rendre les caractères légèrement plus larges que ceux du style de paragraphe qu’il accompagnera. N’augmentez pas la largeur de plus de quelques pour cent, sinon elle peut devenir grotesque.
4. L’augmentation de la largeur peut avoir perturbé l’espacement entre les caractères, donc expérimentez avec le champ ``Espacement`` dans l’onglet ``Position``. Dans la mesure où le style de caractères à petites capitales a une taille de police plus petite, vous voudrez probablement augmenter l’espacement pour optimiser la lisibilité.

Lorsque vous avez terminé vos réglages, comparez votre travail avec les petites capitales fabriquées par LibreOffice, et choisissez le meilleur rendu.

### Rendre l’interligne cohérent

Même les utilisateurs avancés s’interrogent sur le réglage ``Contrôle de repérage`` dans l’onglet ``Retraits et espacement``. En fait, la fonction ``Contrôle de repérage`` rend les lignes cohérentes d’une page à l’autre —&nbsp;ou aussi cohérentes que possible si des polices de tailles différentes sont utilisées.

Lorsque ``Contrôle de repérage`` est sélectionné, les lignes de texte dans les colonnes, les pages en miroir ou sur les deux côtés d’une page sont espacées de manière identique.

Le réglage améliore l’apparence des documents à une ou plusieurs colonnes et empêche, sur le papier, les ombres du verso d’interférer avec la lecture du recto (et inversement).

Normalement, l’espacement est celui de la police de corps de texte. 

<div style="astuce">

Activer le ``Contrôle de repérage`` pour plus d’un style de paragraphe peut annuler le réglage. N’utilisez le paramètre que pour le style de paragraphe le plus souvent utilisé —&nbsp;habituellement, le corps de texte&nbsp;— et les styles connexes —&nbsp;par exemple, le retrait dans le corps de texte&nbsp;— qui utilisent le même interligne.

Au lieu d’utiliser ``Contrôle de repérage`` pour les titres, faites en sorte que la taille de la police, l’espace au-dessus et l’espace en dessous, soient un multiple de l’interligne (votre nombre magique). De cette façon, les titres seront rarement désynchronisés.
</div>

![Le contrôle de repérage rend les interlignes cohérents entre les pages.](Captures/CH7-pageslignes.png)

## Automatisation avec les styles

Les gens pensent à la typographie surtout en termes de format, c’est-à-dire le choix des polices de caractères et des espacements. Cependant, la typographie numérique vise également à faciliter la construction et la maintenance d’un document.

Ces préoccupations n’ont pas d’importance si vous écrivez un document qui sera envoyé, lu et jeté en quelques minutes. En fait, toute tentative de mise en forme poussée de ce type de document de courte durée est un gaspillage d’efforts et de temps.

Cependant, de nombreux documents ont une durée de vie plus longue. Par exemple, un manuel technique peut être révisé une douzaine de fois ou plus au cours de son cycle de vie. Dans de telles circonstances, tout formatage qui vous donne une chose de moins à penser est le bienvenu.

Cette section présente deux caractéristiques des styles de paragraphes qui facilitent la construction des documents&nbsp;: l’utilisation de styles conditionnels et la définition des sauts de page par style.

En échange d’une configuration supplémentaire, les deux fonctions permettent de garder vos mains sur le clavier pendant que vous travaillez, de manière à vous concentrer sur le contenu au lieu de vous distraire avec des problèmes de forme.

### Configurer les styles conditionnels


Habituellement, on définit un style, puis, dans le champ ``Style de suite`` dans l’onglet ``Gestionnaire``, on défini le style sensé être utilisé à la suite du paragraphe. Un style conditionnel est un autre moyen d’utiliser les styles de paragraphe. 

Avec un style conditionnel, vous définissez le format du paragraphe pour chaque contexte, comme dans un tableau ou un pied de page. Lorsque le curseur se déplace vers un nouveau contexte, le style change automatiquement.

![L’onglet des conditions.](Captures/CH7-conditions.png)



<div style="attention">
Attention

Ne confondez pas un style de paragraphe conditionnel avec un champ de texte conditionnel disponible dans ``Insérer > Champs > Autres champs > Fonctions``. Tout ce que les deux ont en commun, c’est que chacun change selon des variables.
</div>

Les styles conditionnels peuvent être déroutants. Cependant, ils sont plus faciles à utiliser que vous ne l’imaginez. Chaque style contextuel est défini par un autre style de paragraphe, puis il est connecté au style conditionnel dans l’onglet ``Condition``.

Les styles conditionnels ont des limites&nbsp;:

- Vous ne pouvez pas conditionner les styles de paragraphe prédéfinis, à l’exception du corps de texte. En fait, les styles de paragraphe prédéfinis n’affichent pas d’onglet ``Condition``, bien que les nouveaux styles de paragraphe (personnalisés) créés à partir de ces styles le fassent.
- Si vous voulez qu’un style personnalisé soit conditionnel, vous devez configurer au moins une condition avant de cliquer sur le bouton ``OK`` ou ``Accepter`` lorsque vous fermez la fenêtre de dialogue de style pour la première fois. Sinon, la prochaine fois que vous ouvrirez la fenêtre de dialogue du style, l’onglet ``Condition`` ne sera plus disponible (cela dit, pour vous en sortir, il suffit de dupliquer le style et éditer le nouveau).
- Un style conditionnel est limité à trente contextes prédéfinis. Vous ne pouvez pas créer des contextes personnalisés. 

Même avec ces limitations, les styles conditionnels peuvent être utiles, surtout si la structure du document n’est pas trop complexe.

### Définir un style conditionnel

Avec les styles conditionnels, vous n’avez besoin de mémoriser que le nom d’un seul style de paragraphe par document ou modèle, tout en ayant des variantes de mise en forme.

Pour créer un style conditionnel&nbsp;:

1. Examinez l’onglet ``Condition`` et notez les contextes que vous voulez utiliser. Vous ne pouvez pas créer de nouveaux contextes.
2. Créez un style de paragraphe pour chaque contexte que vous comptez utiliser. Le seul style prédéfini que vous pouvez utiliser comme style conditionnel est le corps de texte, mais vous pouvez créer un nouveau style à partir de n’importe quel style prédéfini.
3. Créez un nouveau style de paragraphe et allez dans l’onglet ``Condition``, cochez la case ``Style conditionnel``.
4. Sélectionnez un contexte.
5. Dans le volet ``Styles de paragraphe``, sélectionnez le style de paragraphe que vous voulez appliquer dans le contexte sélectionné. Cliquez deux fois, et le style sélectionné est répertorié sous ``Styles appliqués`` dans le volet ``Contexte``.
6. Répétez les étapes 4-5 autant que nécessaire.
7. Cliquez sur le bouton ``OK`` ou ``Appliquer`` lorsque tous les contextes que vous prévoyez d’utiliser sont associés à un style de paragraphe.

<div style="attention">

Attention

Si vous voulez utiliser des conditions avec un style personnalisé, vous devez en définir au moins une avant de fermer la fenêtre de dialogue. Sans quoi l’onglet sera indisponible la prochaine fois que vous ouvrirez la fenêtre de dialogue du style. Tant que vous avez défini au moins une condition, vous pouvez ajouter et supprimer des conditions plus tard. 
</div>

### Configurer des sauts de page par style

L’application d’un style de paragraphe coïncide souvent avec le début d’une nouvelle page.

Par exemple, les nouveaux chapitres peuvent toujours commencer par un style de paragraphe appelé ``Numéro de chapitre`` ou ``Titre``, tandis qu’un style appelé ``Titre de diagramme`` peut commencer une nouvelle page pour assurer un espace suffisant pour un diagramme.

Cette fonction est configurée dans la section ``Sauts`` de l’onglet ``Enchaînements``.

![Onglet Enchaînements.](Captures/CH7-Sautstyles.png)

### Automatiser les sauts de pages

Pour configurer des sauts de page automatiques&nbsp;:

1. Ouvrez la fenêtre de dialogue concernant le style de paragraphe qui coïncidera avec le début d’une nouvelle page.
2. Allez dans l’onglet ``Enchaînements`` puis cochez la case ``Sauts > Insérer``.
3. Réglez le type sur ``Page``.
4. Si la position est ``Avant``, vous pouvez sélectionner la case ``Avec  le style de page`` et choisir le style de la nouvelle page dans la liste déroulante.
5. Lorsque vous sélectionnez un style de page qui s’enchaîne avec le saut, vous pouvez également réinitialiser le numéro de page. Par exemple, vous pourriez avoir un style de page pour une introduction numérotée en chiffres romains minuscules et des pages ordinaires qui utilisent des chiffres arabes.
6. Si vous voulez que le numéro de page continue séquentiellement à partir de la page précédente, laissez le champ ``Numéro de page`` à 0.

<div style="attention">
Le type de saut comprend également l’option ``Colonne``. Cette sélection peut être utile dans une section à plusieurs colonnes ou dans un bulletin d’information. Cependant, cela peut être gênant et déroutant. Dans de nombreux cas, vous aurez probablement moins de problèmes de déplacement d’éléments si vous créez un tableau à la place.
</div>

## Textes cachés

Les versions multiples de documents qui ne diffèrent que par certains détails sont courantes dans les milieux d’affaires ou universitaires.

Par exemple, vous pourriez vouloir une version d’un polycopié pour les étudiants et une autre version pour les enseignants qui comprend des annotations. Vous pouvez aussi avoir une version d’un manuel de logiciel pour les utilisateurs et une autre pour les administrateurs système.

Le seul problème est qu’il est difficile de maintenir plusieurs versions d’un document. Des outils de versioning comme Git permettent cependant de trouver des solutions efficaces, mais si l’on s’en tient aux logiciels de traitement de texte, la synchronisation de plusieurs versions devient vite complexe et chronophage.

Le maintien de toutes les versions d’un document dans un seul fichier complique l’impression, vous obligeant à créer d’abord une copie en double, puis à supprimer toutes les parties qui ne sont pas nécessaires pour la version que vous imprimez, tout en espérant ne pas commettre d’erreur.

La solution de LibreOffice à ce dilemme est de créer un seul fichier dans lequel des mots, des paragraphes ou des sections sont cachés ou révélés selon les besoins. Les outils comprennent des styles et des champs. Tous les outils pour les éléments cachés fonctionnent avec deux versions du texte, mais certains ne fonctionnent pas avec trois versions ou plus.

![Onglet des effets de caractères.](Captures/CH7-effetmasque.png)

L’utilisation de texte ``masqué`` est plus rapide que les opérations manuelles et réduit les risques d’erreurs. Il élimine également le besoin de manipuler des copies, ce qui, avec des mains négligentes ou des cerveaux fatigués, peut conduire à l’écrasement accidentel du fichier original.

### Stratégies de masquage

Les fonctions de masquage et d’affichage du texte peuvent être utilisées de deux façons.

Si deux versions du document partagent un texte commun, entrez-le dans l’une en utilisant des styles de paragraphe ordinaires et créez des styles de paragraphe uniques pour l’autre qui peuvent être cachés et affichés au besoin. Cette méthode fonctionne à la fois avec les styles et avec les sections et les champs, mais elle peut être difficile à organiser.

![Une version ouverte à tous et des contextes différents pour les autres versions.](Captures/CH7-publicononff.png)

L’alternative consiste à créer un ensemble de styles de paragraphes pour chaque version du document, en les activant et les désactivant selon les besoins.

Dans cette structure, chaque version pourrait être distincte, et il pourrait aussi être possible de mélanger les versions. Par exemple, si vous préparez des guides pour l’utilisateur, le développeur et l’administrateur d’un logiciel, le guide publié pour les administrateurs pourrait montrer à la fois les contenus qui s’adressent à  l’utilisateur et à l’administrateur, tandis que le guide pour les développeurs pourrait inclure à la fois les contenus pour l’administrateur et pour le développeur. Vous pourriez également créer plusieurs contenus que l’on trouverait dans plus d’une version, bien que cela puisse devenir trop complexe pour que vous puissiez travailler avec.

Toutes ces possibilités peuvent être utilisées en configurant plusieurs variables pour les champs via ``Insertion > Champs > Autres champs``, une pour chaque version, mais il est plus facile de travailler avec les styles.


![Une version unique pour des contextes différents.](Captures/CH7-publicononff2.png)

### Masquer du texte avec les styles

Pour masquer ou révéler du texte, vous pouvez cocher la case ``Masqué`` dans l’onglet ``Effets de caractère``. Vous pouvez soit choisir de masquer une partie du document pour produire une version alternative, soit créer un ensemble différent de styles de paragraphes pour chaque version.

Quelle que soit la méthode que vous choisissez&nbsp;:

1. Créez un ensemble de styles de caractères et de paragraphes pour le texte qui apparaît dans toutes les versions du document.
2. Créez les styles nécessaires pour chaque version du texte. Par exemple, dans un quiz d’étudiants avec des champs de réponse, vous pouvez avoir un ensemble de styles avec des noms comme ``Utilisateur - Corps de texte`` et ``Enseignant - Corps de texte``. Ces styles sont formatés exactement de la même façon que les styles communs, et cachés selon les besoins.
3. Avant l’impression, dans l’onglet ``Effets de caractère`` de chaque style spécifique, activez l’option ``Masqué``. Remarquez que l’espacement au-dessus ou au-dessous d’un paragraphe est caché avec le texte. En revanche, vous devez sélectionner l’espace après une chaîne de caractères cachés.
4. Après avoir imprimé une version à partir d’un fichier source unique, désélectionnez ``Masqué`` pour que le fichier complet soit visible la prochaine fois que vous l’ouvrirez. 

### Masquer du texte à l’aide de champs

Une autre méthode, mais qui prend plus de temps, consiste à configurer chaque passage. Vous pouvez soit basculer une version du document, soit créer une variable séparée avec une valeur unique pour chaque version du document. Cette deuxième méthode n’est pratique que pour des documents relativement courts.

L’onglet ``Fonctions`` de la boîte de dialogue ``Champs`` contient plusieurs outils utiles&nbsp;: ``Texte masqué``, ``Paragraphe masqué`` et ``Texte conditionnel``.

Pour des utilisations limitées, comme changer le titre ou le contenu d’un en-tête ou d’un pied de page, vous pouvez utiliser des listes d’entrées, qui contiennent des éléments interchangeables. Cependant, les listes d’entrée ne sont pas pratiques dans un document plus long, parce que chaque liste doit être modifiée séparément.

L’option ``Masqué`` dans un style de caractère ou de paragraphe est l’équivalent stylistique de certains champs dans ``Insérer > Champs > Autres Champs > Fonction > Type``, tels ``Texte masqué`` et ``Paragraphe masqué``. 

Une condition (comme celle qui définit si un texte doit être masqué ou non) est simplement un état du document —&nbsp;ou, si vous préférez, une version avec un contenu différent.
 
Par exemple, lorsqu’une condition est fixée à 0, le contenu des champs est masqué, créant ainsi une version du fichier. Lorsqu’une condition est fixée à 1, le contenu est affiché, créant ainsi une deuxième version. Alternativement, la condition qui active une version d’un document pourrait être le nom de la version. Cette disposition n’est pas différente de cocher ou décocher la case ``Masqué`` dans un style.

Prenons un autre cas. Dans un champ ``Texte conditionnel``, une expression simple est configurée à l’aide des champs ``Condition``, ``Alors``, et ``Sinon`` sur le côté droit de la fenêtre. Par exemple, si la condition est 1, alors le texte qui apparaît dans le document est ce qui est entré dans le champ ``Alors``. Si vous changez la condition à autre chose, le texte devient ce qui est entré dans le champ ``Sinon``.

### Masquage avec les sections

Lorsque les documents ont de grandes zones à masquer ou à afficher, on préférera l’utilisation des sections via ``Insertion > Section``.

Les sections sont des zones qui ont des propriétés différentes du corps principal du texte. Ces propriétés peuvent être une mise en forme ou un contenu protégé par mot de passe. 

Vous pouvez également ajouter un lien pour insérer un fichier séparé dans le document en cours.

![Insérer des sections.](Captures/CH7-inserersection.png)

#### Automatisation des champs et des sections

Les fenêtres de champ restent ouvertes après l’insertion d’un champ, ce qui vous permet de passer à la position suivante dans le document. Les fenêtres de section ne le font pas, bien que vous puissiez utiliser le Navigateur pour passer d’une section à l’autre au fur et à mesure que vous éditez.

Cependant, changer les conditions pour chaque champ ou section individuellement prend du temps. Si le même champ apparaît plus d’une fois, vous pouvez aussi copier et coller.

Au lieu de cela, vous pouvez définir une variable générale qui bascule tous les champs en même temps&nbsp;:

1. Placez le curseur au début ou à la fin du document, ou à tout autre endroit facile à localiser.
2. Cliquez sur ``Insérer > Champs > Autres champs > Variables > Définir une variable``.
3. Définissez le format. Vous pouvez le laisser en tant que standard, ou spécifiquement en tant que texte ou en tant que nombre.
4. En bas de la fenêtre, donnez un nom à la variable. Le nom peut indiquer une des versions du document, ou bien quelque chose comme «&nbsp;basculement&nbsp;».
5. Entrez une valeur. Il peut s’agir de texte, ou simplement 0 ou 1.
6. Cochez la case ``Invisible`` pour que cette variable ne soit pas visible dans le document. Cliquez ensuite sur le bouton ``Insérer``.
7. Dans tous les champs ``Texte conditionnel``, ``Texte masqué`` et ``Paragraphe masqué``, ainsi que ``Sections``, définissez la condition sur le nom de la variable, suivi de la valeur entre guillemets. Par exemple&nbsp;: Basculement "0" ou "1".

Vous pouvez maintenant afficher ou masquer tous les champs du document en ne modifiant que la variable, comme vous le feriez avec un style. Si vous avez plus de deux versions du document, vous pouvez créer d’autres variables pour activer et désactiver chacune d’entre elles.

Cependant, assurez-vous que les valeurs soient différentes pour les champs, et qu’ils soient placés là où vous pouvez facilement les trouver.


#### Utiliser les sections

Utiliser les sections est une autre façon de cacher ou de masquer du texte. Elles peuvent être utilisées plus facilement que les champs pour les longs passages, mais elles sont moins polyvalentes qu’un style de paragraphe.

Les sections fonctionnent à peu près de la même façon que les champs&nbsp;:

1. Placez le curseur à l’endroit où vous voulez une section vide, ou bien mettez en surbrillance le texte existant. Sélectionnez ensuite ``Insérer > Section``. La fenêtre de dialogue s’ouvre. 
2. Donnez à la section un nom unique qui reflète son contenu et, si vous le souhaitez, protégez-la avec un mot de passe. 
3. Cliquez sur la case ``Masquer``, et / ou définissez le champ ``Sous condition`` à 1. Lorsque vous fermez la fenêtre de dialogue, la section et l’espacement au-dessus et en dessous ne seront plus visibles dans le document.
4. Lorsque vous souhaitez modifier la section, cliquez sur ``Format > Sections``. La fenêtre de dialogue liste les sections du document, avec un cadenas ouvert ou fermé à côté de chaque nom pour indiquer s’il est caché ou non. Les sections peuvent être mises en forme sans les déverrouiller, mais doivent être déverrouillées pour éditer le texte.


## Les limites de la mise en page avec LibreOffice

Si avez lu ces chapitres dans l’ordre, vous devriez commencer à comprendre pourquoi Writer peut être décrit comme un assistant de publication intermédiaire. En utilisant Writer, vous pouvez suivre les principes typographiques de base et concevoir facilement des documents complexes.

Certaines fonctionnalités avancées peuvent être ajoutées en modifiant seulement quelques paramètres dans les styles de caractères et de paragraphes. D’autres ne peuvent pas du tout être mises en place dans les styles.

Le plus grand manque est probablement l’insertion facile de caractères non standards, ou spéciaux. Une façon d’atténuer cette limitation est de choisir une locale de clavier international qui inclut des caractères accentués et des devises internationales. Ou bien, sous GNU/Linux, configurer la touche ``Compose`` qui permet d’utiliser de nombreux raccourcis clavier pour insérer des caractères.

Mais même dans ce cas, les ressources comme les petites capitales, les figures anciennes et les ligatures ne peuvent pas être utilisées automatiquement. Parfois, ces fonctions sont disponibles dans des familles de polices, mais, dans d’autres cas, vous devez utiliser des fonctions telles que macros, ``AutoTexte`` et ``AutoCorrection`` pour créer des bibliothèques. La difficulté, bien sûr, est de se rappeler comment accéder à ces bibliothèques.

### Typographie avec Grammalecte

Grammalecte est un correcteur grammatical spécialement dédié à la langue française et qui fonctionne comme une extension avec LibreOffice, et d’autres logiciels comme Thunderbird et Firefox. Voyez le site ``dicollecte.org``.

L’un de ses nombreux avantages est qu’il s’occupe aussi de typographie et permet de composer des documents en facilitant la saisie, par exemple&nbsp;: la gestion des ordinaux, la gestion différenciée des espaces insécables, etc. 

Bien sûr, ce n’est pas un  assistant à la gestion des styles mais il permet de compléter efficacement certains manques de LibreOffice.



