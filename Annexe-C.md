# Se procurer des polices sous licence libre

Tout comme les logiciels libres ou open source, les polices sous licence libre sont celles que vous pouvez utiliser, partager et modifier comme bon vous semble. La plupart sont également disponibles gratuitement.

Les polices sous licence libre existaient à peine avant 2000. Il y en a des centaines aujourd’hui, bien que leur nombre soit encore faible par rapport aux milliers de polices propriétaires disponibles dans les fonderies de polices comme Adobe.

Beaucoup sont des clones ou des quasi-variations de polices classiques, mais certaines sont des polices originales et même exceptionnelles&nbsp;! 

Les polices sous licence libre sont disponibles sous la licence SIL Open Font License (OFL) ou la GNU General Public License (GPL —&nbsp;avec l’exception pour les polices). Certaines sont également dans le domaine public. D’autres licences existent, mais toutes n’ont pas été évaluées par la Free Software Foundation ou l’Open Source Initiative, et devraient être utilisées avec prudence.

Si vous utilisez GNU/Linux, certaines de ces polices peuvent être installées sous forme de paquets à partir des dépôts de votre distribution. Beaucoup d’autres peuvent être téléchargées en ligne, quel que soit le système d’exploitation.

## Arkandis Digital Foundry

Visitez&nbsp;: ``http://arkandis.tuxfamily.org/adffonts.html``

Les polices Arkandis sont destinées à fournir des versions sous licence libre de polices pour GNU/Linux. La sélection comprend Baskervald (Baskerville), Gillius (Gill Sans) et Universalis (Univers), ainsi que des polices originales telles que Mint Spirit, qui a été conçue à l’origine comme police non officielle pour la distribution de GNU/Linux Mint.

## Barry Schwartz 

Visitez&nbsp;: ``http://crudfactory.com/font/index``

Barry Schwartz est l’un des grands concepteurs de polices de texte sous licence libre. Son travail comprend trois polices basées sur les dessins de Frederick Goudy, ainsi que Fanwood, une police discrète qui ressemble beaucoup à Joanna d’Eric Gill. Vous pouvez également obtenir certaines de ses polices à partir de *The League of Movable Type* (voir ci-dessous).

## Cantarell 

Visitez&nbsp;: ``https://git.gnome.org/browse/cantarell-fonts/``

La police officielle de GNOME 3. Critiquée à l’origine pour certaines de ses formes de lettres, Cantarell est devenue une police humaniste moderne qui peut être utilisée aussi bien pour le corps de texte que pour les titres.

## Dover Books

Visitez&nbsp;: ``http://www.doverbooks.co.uk/Fonts,_Lettering.html``

Dover Books publie une trentaine de livres avec des CD de polices de caractères et de dingbats de l’époque victorienne et des époques antérieures. Ils sont marqués comme étant *libre de droit*, ce qui signifie vraisemblablement le domaine public.

## Polices Google 

Visitez&nbsp;: ``https://www.google.com/fonts/``

Avec plus de 630 familles de polices, les Google Fonts sont principalement destinées à une utilisation en ligne. Cependant, vous pouvez également télécharger des polices pour l’impression. Les mises à jour sont régulières, donc vérifiez régulièrement les nouvelles versions.

## La League of Moveable Type

Visitez&nbsp;: ``https://www.theleagueofmoveabletype.com/``

Se décrivant comme «&nbsp;la toute première fonderie de polices open-source&nbsp;», la League of Moveable Type offre une petite mais sélective bibliothèque de polices originales. Si vous vous perdez dans le nombre de polices libres, tout ce qui vient de The League peut être considéré comme étant de haute qualité, et inclut généralement des petites capitales et des figures de style ancien.

## Polices Liberation 

Visitez&nbsp;:  ``https://fedorahosted.org/liberation-fonts/``

Les polices Liberation sont conçues pour être l’équivalent métrique des polices propriétaires standard. En d’autres termes, les glyphes occupent le même espace vertical et horizontal. Libération Sans est destiné à remplacer Arial et Helvetica, Liberation Serif  correspond à Times New Roman et Liberation Mono correspond à Courier.

## Open Font Library

Visitez&nbsp;: ``http://openfontlibrary.org/``

Avec plus de 400 familles de polices, la bibliothèque de polices Open Font Library se classe deuxième derrière Google Fonts dans cette sélection. Sa page d’accueil comprend une liste des polices les plus récemment téléchargées.

## Oxygen

Visitez&nbsp;: ``http://www.fontspace.com/new-typography/oxygen``

Créé pour l’environnement de bureau KDE sous Linux, Oxygen est une police géométrique moderne, faite de formes simples, mais très lisible et agréable à l’œil.

## Raph Levien

Visitez&nbsp;: ``http://levien.com/type/myfonts/``

Employé de Google, Levien développe aussi des polices pendant son temps libre. Bien que toutes les polices affichées sur cette page ne soient pas complètes, leur qualité les rend dignes d’intérêt.

## SIL International

Visitez ``http://software.sil.org/products/``

SIL International est une organisation religieuse œuvrant pour les communautés linguistiques du monde. Elle a également développé le système Graphite pour l’utilisation automatique de ligatures, de petites capitales, de figures anciennes et d’autres caractéristiques typographiques avancées. La licence SIL Font License est la licence la plus largement utilisée pour les polices libres, et responsable d’une grande partie de la diffusion des polices libres.

## Ubuntu

Visitez&nbsp;: http://font.ubuntu.com

Conçue pour la distribution Linux Ubuntu, il s’agit d’une police humaniste moderne. Elle est polyvalente, bien que son utilisation l’identifie formellement avec Ubuntu.
