# Télécharger LibreOffice

LibreOffice est disponible en différentes versions pour GNU/Linux, OS X et Windows. La plupart des distributions Linux comprennent une copie dans leurs dépôts de paquets, parfois avec des extensions et des jeux d’icônes distincts.

La page officielle pour télécharger la dernière version (*Stable*) est&nbsp;:

``https://fr.libreoffice.org/download/libreoffice-stable/``

La page officielle pour télécharger la dernière version (*Évolution*) est&nbsp;:

``https://fr.libreoffice.org/download/libreoffice-fresh/``

Utilisez les liens en haut à droite de la page pour télécharger des versions pour d’autres systèmes d’exploitation et d’autres langues.

La page officielle pour télécharger Apache OpenOffice est&nbsp;:

``https://www.openoffice.org/download/index.html``

## Extensions et modèles

Des centaines d’extensions et de modèles sont disponibles pour LibreOffice. Les extensions peuvent ajouter des fonctionnalités et des ressources indispensables, tandis que les modèles peuvent vous faire gagner du temps de conception. Cependant, sachez que certaines extensions peuvent ne pas être mises à jour et ne pas toujours fonctionner avec la dernière version. De même, ce n’est pas parce qu’un modèle est proposé qu’il est bien conçu.

Pour obtenir des extensions et des modèles pour LibreOffice, rendez-vous à ces adresses&nbsp;:

- Les extensions&nbsp;: ``https://extensions.libreoffice.org/``
- les modèles&nbsp;: ``https://templates.libreoffice.org/``

Pour la collection d’extensions et de modèles d’Apache OpenOffice, allez à&nbsp;:

- ``https://extensions.openoffice.org/``
- ``https://templates.services.openoffice.org/``

De nombreuses extensions et tous les modèles fonctionnent à la fois sur LibreOffice et Apache OpenOffice. Cependant, les deux sont suffisamment différents pour que la compatibilité diminue lentement au fur et à mesure que chaque version est publiée.

## Zotero et Grammalecte

Dans cette adaptation de l’ouvrage, deux logiciels ont été mentionnés&nbsp;:

- Zotero, un logiciel libre de gestion de base de données bibliographique qui dispose d’une extension pour LibreOffice (et Firefox). Rendez-vous sur&nbsp;: ``https://www.zotero.org``
- Grammalecte, un correcteur grammatical open source dédié à la langue française. Rendez-vous sur ``https://www.dicollecte.org/``
