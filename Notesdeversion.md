- Titre&nbsp;: Designing with Libre Office (version publiée en mars 2016)
- Titre (fr)&nbsp;: LibreOffice, c’est stylé&nbsp;!
- site web&nbsp;: http://designingwithlibreoffice.com/
- auteur et copyright&nbsp;: Bruce Byfield
- traduction et adaptation&nbsp;: Christophe Masutti
- Licence&nbsp;: CC-By-Sa

Relecteurs (remerciements)&nbsp;: dodosan, Barbara Bourdelles, Thierry Meyer, Yann Kervran


# Préface du tradaptateur

Il manquait à la collection Framabook un manuel dédié spécifiquement à
LibreOffice et son traitement de texte. Certes, Internet ne manque pas
de ressources pour qui cherche une procédure particulière dans
LibreOffice. On peut mentionner l’excellent Wiki multilingue de la
Document Foundation consacré à l’aide de LibreOffice
(*wiki.documentfoundation.org*).

En revanche, rares sont les ouvrages dédiés qui, en plus d’apporter une
collection de procédures, donnent des conseils et des exemples d’usages
pour utiliser un traitement de texte. Beaucoup d’affirmations dans cet
ouvrage pourront faire l’objet de discussions ou de comparaisons avec
d’autres logiciels de traitement de texte. Il n’en demeure pas moins que
l’objectif consiste davantage à faire comprendre à l’utilisateur
l’intérêt des styles dans LibreOffice qu’à lui proposer un simple
inventaire des fonctionnalités plus ou moins avancées.

Cette adaptation de l’ouvrage est à la fois une traduction et une libre
modification de la version originale de Bruce Byfield. Outre la
traduction de la majorité des passages, certains d’entre eux ont fait
l’objet de modifications, suivant ces principes :

-   adaptation à la dernière version de LibreOffice (6),
-   suppression de passages redondants ou superflus,
-   ajouts et précisions pour certaines procédures,
-   suppression de quelques chapitres.

La version originale traite de l’utilisation des principales composantes
de LibreOffice : Writer, Calc, Draw et Impress. Dans cette adaptation,
j’ai fait le choix d’axer l’ouvrage uniquement sur Writer et le bon
usage des styles, sans intégrer les chapitres 12 à 15 de la version
originale.

De même, certains passages ont été sévèrement abrégés. Par exemple,
celui sur la gestion des modèles de bibliographie : tout en conservant
l’idée générale et les éléments-clés, j’ai considéré qu’en principe il
est plus efficace d’utiliser un logiciel libre spécialisé (comme Zotero)
et son extension pour LibreOffice. Si LibreOffice permet de faire
énormément de choses, en particulier grâce aux styles, il est plus
prudent de ne pas prétendre qu’il peut tout faire et aussi bien que
d’autres logiciels.

Cette adaptation de *Designing with LibreOffice* s’adresse donc en
priorité aux utilisateurs désireux de tirer parti efficacement de
LibreOffice Writer en leur apprenant à composer des documents avec style
et avec les styles. Temps, efficacité et esthétique sont les principes
clés qui guideront cet apprentissage. En cela, cette adaptation libre ne
trahit pas l’œuvre originale.

##### Remerciements

La collection Framabook et moi-même adressons nos plus sincères
remerciements aux relecteurs : Dodosan, Barbara Bourdelles, Thierry
Meyer, Yann Kervran.

C. Masutti (alias Framatophe)




