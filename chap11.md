# Les documents longs

Les styles prouvent constamment leur intérêt, même lorsque vous pensez avoir créé le modèle de vos rêves. Dans Writer, les styles de paragraphe rendent de grands services, en particulier pour les documents académiques et formels.  Les tâches telles que le titrage, la navigation dans le document, l’ajout de références croisées ou la création de tables des matières sont impossibles sans l’utilisation des styles de paragraphes (du moins, les réaliser manuellement serait une grande perte de temps).

Ce chapitre commence par les fonctions avancées de Writer, en se concentrant sur la façon dont les styles de paragraphes les améliorent et sur la façon dont vous pouvez les utiliser pour personnaliser vos documents et leur donner une touche professionnelle. La suite concerne les outils de conception de documents longs et académiques. Certains de ces outils ne s’appuient pas lourdement sur les styles, mais vous devrez peut-être être conscient de leurs bizarreries au moment de la conception.

## Utilisation des niveaux hiérarchiques

Si vous avez remarqué l’option ``Outils > Numérotation des chapitres``, vous avez peut-être pensé qu’il s’agit d’une méthode manuelle de création de chapitres. 

![Numérotation des chapitres.](Captures/CH11-numchap.png)


En fait, la numérotation des chapitres n’est qu’une utilisation très  élémentaire de cet outil. Ce dernier définit également les styles de paragraphes utilisés pour chaque niveau. Ces styles sont repris par d’autres outils de Writer pour simplifier votre travail.

Par défaut, chaque niveau se voit attribuer un style de paragraphe. Par exemple, le titre 1 est affecté au niveau de titre 1 dans le plan, et ainsi de suite. Chaque style correspond au même niveau de titre. Cependant, ce que la plupart des utilisateurs ne remarquent jamais, c’est qu’on peut assigner un autre style de paragraphe à n’importe quel niveau de titre, en utilisant le champ ``Niveau de plan`` dans l’onglet ``Plan & numérotation`` pour un style de paragraphe. Vous pouvez également modifier le style par défaut d’un niveau hiérarchique en modifiant le champ ``Style de paragraphe`` dans ``Outils > Numérotation des chapitres``.
 
Une fois qu’un style de paragraphe est assigné à un niveau hiérarchique, il peut être utilisé pour&nbsp;:

- La rédaction d’un plan.
- Apparaître dans le Navigateur.
- Mettre en place efficacement des références croisées.
- Créer des tables des matières et mettre en forme l’index et la bibliographie.


### Rédaction d’un plan

Vous pouvez utiliser des styles de titre avec un style de liste qui y est attaché, ou un style de paragraphe unique avec un style de liste sommaire qui y est attaché. 

Cependant, la méthode la plus évidente est d’utiliser ``Outils > Numérotation des chapitres``. Les paramètres de cet outil ressemblent aux choix de l’onglet ``Personnalisation`` d’un style de liste. Le formatage peut être personnalisé séparément pour chaque niveau hiérarchique ou pour tous les niveaux à la fois.


### Apparaître dans le Navigateur

Le Navigateur est l’une des fonctionnalités les plus sous-utilisées de LibreOffice. Cependant, plus le document est long, plus il devient utile à mesure que vous l’éditez et le révisez. 

Pour ouvrir le Navigateur, sélectionnez ``Affichage > Navigateur``, ou appuyez sur la touche ``F5``, ou sélectionnez le Navigateur dans la barre latérale.

Au niveau le plus simple, le Navigateur liste tous les objets d’un document, y compris les niveaux de titre —&nbsp;les titres par défaut, mais aussi d’autres styles de paragraphe dont vous avez modifié les niveaux hiérarchiques. Cliquer sur un élément de la liste dans le Navigateur permet d’y accéder dans la fenêtre d’édition. 

Cependant, ce qui est peut-être moins évident, c’est que les rubriques énumérées dans le Navigateur peuvent vous aider à restructurer un document.

![Utilisation du Navigateur.](Captures/CH11-navigateur.png)

### Modification des niveaux hiérarchiques à l’aide du Navigateur

Les titres doivent être hiérarchiques, de sorte que le sujet d’un style de paragraphe de la rubrique 3 soit contenu dans le style de paragraphe de la rubrique 2 directement au-dessus. Par exemple, le titre «&nbsp;Le corps humain&nbsp;» peut avoir des sous-titres comme «&nbsp;Le cœur&nbsp;» et «&nbsp;Les poumons&nbsp;». Cette structuration renforce la logique interne d’un document et aide les lecteurs à trouver des sections lorsqu’ils lisent.

De telles relations sont essentielles à la structure d’un document. Dans le Navigateur, si vous voyez un titre qui devrait être d’un niveau supérieur ou d’un niveau inférieur dans la hiérarchie, mettez-le en surbrillance, puis cliquez soit sur ``Hausser d’un niveau``, soit sur ``Abaisser d’un niveau``, dans les boutons de la barre d’outils du Navigateur.

Chaque fois que vous cliquez, le titre actuellement sélectionné sera élevé ou abaissé d’un niveau dans la hiérarchie. Ainsi, tout niveau de hiérarchie lui sera subordonné en fonction de sa place, de sorte que la promotion d’un niveau de titre 2 à un niveau 1 promeut également tous les niveaux de titre de niveau 3  à un niveau 2, etc.

## Déplacement de contenus à l’aide du Navigateur

De même, au fur et à mesure que vous travaillez, vous constaterez peut-être qu’une partie du contenu appartient en fait à un autre endroit dans le document.

Au lieu de couper et coller, vous pouvez cliquer soit sur ``Hausser d’un niveau``, soit sur ``Abaisser d’un niveau``, pour déplacer un chapitre et son texte à un nouvel endroit dans le document. 

<div style="astuce">

Astuce

Le mot «&nbsp;chapitre&nbsp;» fait référence à la partie du document située entre une rubrique et la suivante. La «&nbsp;hausse&nbsp;» rapproche le chapitre du début du document, l’«&nbsp;abaissement&nbsp;» le rapproche de la fin.

</div>

Tous les titres subordonnés et tout autre style de paragraphe sous le titre sélectionné se déplaceront également, en conservant le même classement les uns par rapport aux autres.

Ainsi, le Navigateur remplace le copier-coller ordinaire. Cependant, il est plus efficace, car il fournit une représentation visuelle de vos actions.

Tout aussi important&nbsp;: si vous êtes interrompu, avec le Navigateur, il n’y a pas de danger de perdre du contenu parce que vous l’avez oublié.

## Les références croisées

Les références croisées sont des champs actualisables qui font référence à une autre partie du document ou peuvent renvoyer à un autre document et même à un endroit précis de celui-ci. Dans les documents Web, il s’agit de liens pour faciliter la navigation vers la référence.

Les références croisées manuelles seraient difficiles à maintenir —&nbsp;en particulier leurs références de pages&nbsp;— donc LibreOffice les met à jour automatiquement au fur et à mesure que vous ajoutez et supprimez du contenu et que vous fermez et ouvrez les documents. Vous pouvez également mettre à jour manuellement en cliquant sur ``Outils > Actualiser > Champs`` (ou touche ``F9``).

Pour ajouter une référence croisée, vous avez besoin de deux éléments&nbsp;: la ``Sélection`` (la source) et la ``Référence`` (la cible). Les sélections sont choisies soit parmi le contenu des niveaux hiérarchiques, soit parmi les signets ou les marqueurs ajoutés manuellement.

Normalement, vous ajoutez des références croisées au fur et à mesure que votre document est en phase de finition. De cette façon, vous évitez de briser les liens et d’avoir à les recréer au fur et à mesure que vous déplacez des passages ou renommez des fichiers. De plus, vous pouvez garder la fenêtre de dialogue des références croisées ouverte et faire toutes les références croisées en une seule (et parfois longue) opération.

<div style="astuce">
Astuce

Si vous prévoyez d’utiliser des références croisées pour les tableaux, les images et d’autres objets, donnez leur une légende, puis créez un ``Renvoi`` vers cette légende via ``Insertion > Renvoi > Type > Illustration``. Si le style de paragraphe que vous utilisez pour les légendes n’en a pas encore (au contraire du style par défaut ``Illustration``), attribuez un niveau de plan à ce style, ce qui le fera apparaître dans la liste des «&nbsp;chapitres&nbsp;» comptabilisés dans la fenêtre de dialogue ``Insertion > Renvois``.
</div>

![Insérer une référence croisée.](Captures/CH11-renvois.png)

### Références croisées dans un document

Les niveaux hiérarchiques fournissent des marqueurs automatiques à utiliser avec des références croisées. Ajoutez des titres au fur et à mesure que vous écrivez, puis suivez ces étapes lorsque vous ajoutez des renvois&nbsp;:

1. Placez le curseur à la position de la référence croisée.
2. Sélectionnez ``Insertion > Renvoi``. La fenêtre de dialogue des ``Champs`` s’ouvre et affiche l’onglet ``Renvoi``.
3. Dans le volet ``Type``, sélectionnez le type de source à utiliser. Utilisez les titres chaque fois que c’est possible, puisque vous les ajoutez de toute façon et qu’ils ont tendance à être relativement courts. Sinon, considérez les paragraphes numérotés ou les signets. 
4. Choisissez la source dans le volet ``Sélection``.
5. Choisissez le format de la référence dans le volet ``Insérer une référence à``. 

Cliquez sur le bouton ``Insérer`` pour insérer la référence croisée dans votre document. La fenêtre de dialogue ``Champs`` reste ouverte.

Dans le document, ajoutez les mots pour introduire le renvoi. Réutilisez la fenêtre ``Champs`` si vous devez compléter la référence, par exemple avec quelque chose comme «&nbsp;Voir chapitre 6, page 79&nbsp;». 

Répétez l’opération pour tous les autres renvois. Fermez la fenêtre de dialogue lorsque vous avez terminé.

### Renvoi à un autre fichier

L’ajout d’un renvoi à un autre document est un processus différent, même si le document source et le document cible se trouvent dans le même document maître.

La meilleure méthode consiste à utiliser des hyperliens (et leurs styles). Pour cela, voici une procédure&nbsp;:


1. Votre document source est ouvert. Sélectionnez le mot ou le groupe de mots que vous allez transformer en hyperlien.
2. Cliquez sur ``Insertion > Hyperlien`` ou appuyez sur les touches ``Ctrl + K`` ou cliquez sur le bouton ``Hyperlien`` de la barre d’outils.
3. Dans la fenêtre de dialogue, cliquez sur ``Document`` dans le volet de gauche.
4. Définissez le ``Chemin`` du document cible soit en l’entrant manuellement soit en utilisant le navigateur de fichiers via l’icône.
5. Vous pouvez définir le niveau hiérarchique exact dans le document externe en cliquant sur l’icône qui ouvrira alors le Navigateur affichant les références dans le document cible.
6. Les autres éléments de référence croisée, comme la page, devront être ajoutés manuellement. Cependant, si le document externe est mis à jour (sans que les cibles changent), votre lien renverra toujours au bon endroit.

![Créer un hyperlien vers une cible dans un document externe.](Captures/CH11-cibleshyperlien.png)

## Tables et index

«&nbsp;Tables&nbsp;» et «&nbsp;Index&nbsp;» sont les termes que LibreOffice utilise pour décrire les champs qui sont générés à partir du contenu d’un document. 

Les plus courantes sont les tables des matières, qui sont créés à partir des niveaux hiérarchiques, et les tables des illustrations ou des tableaux, qui sont créés à partir des légendes.
 
Toutefois, vous pouvez créer toutes sortes d’index et de tables. Par exemple&nbsp;:

- Un index alphabétique&nbsp;: une liste de mots-clés et leur apparition dans le texte.
- Une table des illustrations&nbsp;: une liste d’images, générée à partir des légendes.
- Une table des tableaux&nbsp;: une liste des tableaux, générée à partir des légendes.
- Une table des objets divers&nbsp;: une liste d’autres éléments, comme les graphiques.
- Une bibliographie&nbsp;: liste des documents de référence utilisés dans le document.

Vous pouvez également ajouter des marques d’index définies par l’utilisateur pour créer d’autres tables encore.

![Insérer un index ou une table.](Captures/CH11-tabledesmatieres.png)


### Création d’une table des matières

Chaque type de table a ses propres caractéristiques. Cependant, leurs procédures de construction sont similaires à celle de la table des matières&nbsp;:

1. Si nécessaire, personnalisez un style de page pour la table des matières et ajoutez-le au document.
2. Allez dans ``Insertion > Table des matières et index > Table des matières, index ou bibliographie > Type`` et sélectionnez ``Table des matières`` dans le champ ``Type``. Votre sélection détermine à la fois le titre par défaut et certaines des options avancées.
3. Cochez la case ``Styles supplémentaires`` pour ajouter des styles de paragraphe supplémentaires à la table des matières. Toute sélection que vous effectuez n’ajoute pas de styles aux niveaux hiérarchiques.
4. Si vos entrées de la table des matières sont des mots simples ou des phrases de quelques mots, vous pouvez économiser de l’espace en définissant 2 à 4 colonnes dans l’onglet ``Colonnes``. 
5. Dans l’onglet ``Entrées``, personnalisez les entrées de la table à l’aide des blocs de construction dans le champ ``Structure``. Un aperçu peut vous aider.
6. Cliquez sur le bouton OK pour ajouter la table. Vous pourrez cliquer avec le bouton droit de la souris sur la table pour la modifier ou la mettre à jour ultérieurement.
7. Modifiez les styles de paragraphe pour chaque entrée. Ces styles se composent d’un style de paragraphe d’en-tête (par exemple, le Titre d’une table des matières) et de styles pour chaque niveau d’entrée de la table des matières (par exemple, «&nbsp;Table des matières niveau 1…10&nbsp;»).


### Personnaliser la table avec les blocs

Contrairement à la plupart des traitements de texte, LibreOffice fournit les outils permettant de personnaliser chaque entrée pour une table. Ces outils se trouvent dans ``Insertion > Table des matières et index > Table des matières, index ou bibliographie > Entrées``.  Vous pouvez également modifier les index et les tableaux en personnalisant leurs styles de paragraphes.

![Utiliser les blocs de structure pour personnaliser la table des matières.](Captures/CH11-blocsdelatable.png)

Les outils se composent d’une zone ``Structure`` dans laquelle vous pouvez disposer des blocs de construction tels que le numéro de page et le numéro de chapitre, des caractères et des espaces pour créer une entrée. 

Sous cette zone se trouvent les blocs de construction inutilisés. Lorsque vous ajoutez un bloc au champ, il peut devenir gris et non sélectionnable. De même, lorsque vous supprimez un bloc de la zone, il réapparaît dans la liste des inutilisés sous la zone ``Structure``.

Lorsqu’un bloc de construction est sélectionné, il se grise et les choix de formatage du bloc de construction apparaissent dans la fenêtre. Chaque niveau de plan peut être personnalisé séparément mais il peuvent aussi l’être ensemble en appuyant sur le bouton ``Tout`` à droite de la fenêtre. Si les niveaux ont des éléments communs, formatez-les ensemble, puis modifiez chaque niveau séparément pour obtenir des caractéristiques uniques.

Lorsque vous travaillez, n’oubliez pas&nbsp;:

- Utilisez le DH (Début d’Hyperlien) au début de la zone de saisie et le FH (Fin d’Hyperlien) à la fin de la zone pour faire de l’entrée entière un hyperlien.
- L’espacement de toutes les tabulations est ajouté à l’indentation ``Avant le texte`` que l’on configure dans l’onglet ``Retraits et espacement`` pour le style de paragraphe de chaque entrée (1-10). Pour éviter les difficultés, laissez le champ ``Avant le texte`` à 0.
- Vous pouvez ajouter des espaces manuels et du texte ainsi que des blocs de construction à une entrée. Les espaces manuels ne sont pas élégants, mais peuvent parfois être une solution de contournement à des petits problèmes éventuels.

De manière générale, personnalisez votre table des matières autant que vous le pouvez, tout en restant dans les limites des conventions typographiques. La créativité n’a pas de limite&nbsp;! Il est important de ne pas se contenter des paramètres par défaut de LibreOffice, tout particulièrement si le format de page que vous utilisez n’est pas du A4/Letter. De même inspirez-vous des livres que vous connaissez, regardez comment sont agencés ces éléments et essayez de les reproduire.

Dans la mesure du possible évitez de laisser 3 ou 4 entrées de la table des matières en début de page&nbsp;: essayez d’équilibrer l’affichage en jouant sur les styles.

<div style="astuce">
Astuce

La typographie française distingue entre sommaire et table des matières. Un sommaire ne recense que les niveaux de titres principaux (généralement le premier niveau) et se place au début de l’ouvrage, généralement avant une introduction. La table des matières, elle, recense une majorité des niveaux hiérarchiques (pas forcément tous) et se place à la fin de l’ouvrage, généralement avant le colophon.

</div>

### Création d’un index

Un index est créé de la même manière qu’une table des matières. La principale différence est qu’il est construit à partir de balises de mots ou de phrases, plutôt qu’à partir de styles de paragraphes, ce qui ne fournirait pas le type d’information dont un index a besoin. Ces balises s’affichent dans le document sous forme de champs.

#### Ajout d’entrées d’index

La façon la plus simple d’ajouter une entrée est de sélectionner des mots ou des phrases et de les marquer avec ``Insertion > Table des matières et index > Entrée d’index``. Cependant, cette méthode est laborieuse et prend beaucoup de temps.

Au lieu de cela, vous pouvez automatiser la création d’entrées d’index en sélectionnant ``Appliquer à tous les textes similaires`` pour ajouter les autres occurrences d’une entrée dans le document (ne fonctionne pas pour des entrées insérées manuellement&nbsp;: il faut d’abord sélectionner du texte et insérer l’entrée d’index).

Notez que les index peuvent avoir une entrée principale et jusqu’à deux niveaux de sous-entrées.

![Créer une entrée d’index.](Captures/CH11-entreedindex.png)

#### Générer un index

Une fois toutes les entrées créées, ouvrez la fenêtre de dialogue ``Insertion > Table des matières et index > Table des matières, index ou bibliographie`` pour générer l’index. 

Un index standard est appelé ``Index lexical`` dans le champ ``Type`` de la fenêtre de dialogue. Vous pouvez cependant en modifier le titre.

Si vos entrées sont courtes, vous pouvez enregistrer des pages en utilisant des  colonnes (dans l’onglet ``Colonnes``), et en réglant l’index en conséquence.

Si vous voulez ajouter des en-têtes avec les lettres de l’alphabet, sélectionnez ``Séparateur alphabétique`` dans l’onglet ``Entrées``. Ces séparateurs sont en fait des sous-titres, un pour chaque lettre de l’alphabet.


<div style="astuce">

Astuce

Une autre méthode pour créer un index consiste à éditer un fichier de concordance. Il s’agit de la liste des mots qui doivent être référencés dans un index lexical. Ce fichier obéit à une mise en forme particulière (mais simple à comprendre et à réaliser). Le fichier de concordance peut être édité à partir de l’onglet ``Type`` de la boite de dialogue de création d’index. Pour obtenir une procédure bien expliquée, reportez-vous au wiki d’aide de LibreOffice sur ``help.libreoffice.org`` à la page ``Éditer le fichier de concordance``.

</div>

## Gestion de bibliographie

Comme les tables des matières et les index, les bibliographies sont générées avec la fenêtre de dialogue Index/Table. Cependant, le contenu est basé sur des citations qui font référence aux entrées de la base de données ``Outils > Base de données bibliographiques``. 

La base de données bibliographiques doit couvrir de nombreux médias et circonstances différentes, c’est pourquoi elle contient tant de champs. Dans la pratique, une entrée dans la bibliographie n’a besoin que d’une demi-douzaine de champs à remplir, quel que soit le format de citation que vous utilisez. Ce qui diffère, ce sont les champs nécessaires pour chaque type de document source et l’ordre des champs dans chaque style de citation.

Toutes les citations utilisent le champ ``Abrégé`` (l’identificateur de l’entrée) et le type (livre, article, conférence, etc.) pour définir le format d’une citation dans le document. Tous les renseignements nécessaires doivent être entrés avant la création de toute référence dans le texte. 

![Gestion de la base de données bibliographiques.](Captures/CH11-bddbiblio.png)

### Construire la bibliographie

Voici une procédure générale décrivant les étapes de création de citations et de bibliographie&nbsp;:

1. Entrez dans la base de données les informations correctes pour chaque source que vous utilisez. Par exemple, une référence à un article de revue nécessite des informations différentes que pour une référence à un livre.
2. Dans le champ ``Abrégé``, afin de pouvoir les identifier ultérieurement, entrez par exemple un identifiant qui comprend l’auteur et l’année (Dupont1956).
3. Positionnez le curseur dans le texte et cliquez sur ``Insertion > Table des matières et index > Entrée bibliographique``. Utilisez la liste déroulante dans le champ ``Abrégé`` pour choisir la citation parmi celles que vous avez préparées dans la base de données, puis cliquez sur le bouton ``Insérer``. La fenêtre de dialogue reste ouverte, ce qui vous permet d’insérer d’autres références à la suite.
4. Placez le curseur à l’endroit où vous voulez que la bibliographie apparaisse dans le texte et sélectionnez ``Insertion > Table des matières et index > Table des matières, index ou bibliographie``. 
5. Dans le champ ``Type``, sélectionnez ``Bibliographie``.
6. Utilisez l’onglet ``Entrées`` pour structurer chaque type de source utilisée dans le document. 
7. Dans l’onglet ``Entrées``, définissez ``Trier par``. Dans la plupart des styles de citation modernes, vous préférez ``Contenu`` (ordre alphabétique), avec l’auteur comme critère de tri.
8. Ajuster les styles de paragraphes pour la bibliographie si besoin dans l’onglet ``Styles``.

Enfin, dans le champ ``Structure et formatage`` de l’onglet ``Entrées``, formatez vos types d’entrées bibliographiques (articles, livres, rapports, etc.) en utilisant les blocs. Les champs bibliographiques (comme Auteur, Date, Titre, Année de publication, etc.) ne s’affichent pas toujours de la même manière selon le style bibliographique que vous adoptez. La norme ISO 690 donne les principes directeurs de présentation des références bibliographiques.

### Gérer la bibliographie autrement

Gérer une bibliographie en utilisant la base de données de LibreOffice est aujourd’hui une méthode un peu dépassée. En effet, les outils de gestion de base de données bibliographiques libre ou open source ne manquent pas. Ces logiciels permettent d’entrer des références de manière plus ou moins rapide et efficace, notamment à partir de bases de données en ligne.

Vous avez donc le choix entre importer une base de données existante dans la base de LibreOffice (ce qui est faisable en l’important d’abord dans Calc pourvu que sa construction s’y prête) ou utiliser une extension à LibreOffice. Cette dernière solution est de loin préférable, notamment avec un logiciel comme Zotero (``zotero.org``), un logiciel libre de gestion de base de données bibliographiques plébiscité par de très nombreux utilisateurs.

Zotero dispose d’une solution d’extension avec Firefox et LibreOffice, de sorte que vous pouvez travailler une base en incluant rapidement des entrées et en les insérant tout aussi rapidement dans LibreOffice. Zotero utilise aussi un système de fichiers CSL (Citation Style Language) qui permet de configurer le style d’affichage de votre bibliographie en fonction de vos besoins ou selon des styles prêts à l’emploi, sans passer de temps à gérer les styles des blocs structurels dans LibreOffice.

## Utilisation des documents maîtres

Les documents maîtres sont des méta-documents&nbsp;: ils sont issus d’une collection de documents Writer (``Nouveau > Document maître``). Ils fonctionnent mieux avec une utilisation cohérente des modèles et des styles. 

Vous visualisez la structure d’un document maître grâce à une version spécialisée du Navigateur. Un document maître contient des liens vers ses sous-documents. Lorsque des sous-documents sont ouverts, ils sont reformatés selon le modèle du document maître. Vous pouvez imprimer à partir d’un document maître, mais tous les sous-documents doivent être ouverts séparément pour les éditer.

### Quand utiliser un document maître

Envisagez d’utiliser des documents maître lorsque&nbsp;:

- La mémoire de votre ordinateur est limitée, vous travaillez donc avec des petits documents. 
- Les contenus sont utilisés à différents endroits. Un sous-document peut être inclus dans plus d’un document maître. 
- Un document (comme un livre) a plusieurs auteurs. Les auteurs peuvent travailler sur des sous-documents (comme les chapitres), puis vous utilisez le document maître pour assembler le document complet.
- Vous voulez produire deux documents ou plus qui sont similaires, sauf dans certaines parties. Vous pouvez ajouter tous les fichiers, puis masquer ou non des sections.

### Qu’est-ce qu’un document maître

Les documents maîtres sont construits à partir de trois sources&nbsp;:

- Sous-documents&nbsp;: fichiers individuels plus petits. Vous pouvez en éditer un en le sélectionnant dans le Navigateur et en sélectionnant ``Éditer`` dans le menu contextuel.
- Index et tableaux d’objets insérés dans les zones de texte du document maître&nbsp;: ils peuvent être remplacés par des sous-documents.
- Texte&nbsp;: zones entre les sous-documents qui font partie du document maître. Dans la vue du Navigateur, chaque zone de texte est étiquetée comme du texte simple, de sorte que leur utilisation doit être minimisée pour éviter toute confusion. Les parties du document maître seront plus faciles à suivre si vous évitez le texte et utilisez exclusivement des sous-documents.

### Préparer un document maître

Voici quelques conseils pour préparer efficacement votre document maître&nbsp;:

- Si possible, essayez d’utiliser le même modèle pour créer le document maître et tous ses sous-documents. Si vous utilisez un sous-document dans plus d’un document maître, avec des modèles différents, ignorez ce conseil mais faites attention à la manière dont les styles vont être gérés.
- Concernant votre organisation, placez un document maître et tous ses sous-documents dans le même répertoire. 
- En général, chaque partie d’un document maître commence sur une page séparée. Vous pouvez configurer ce format automatiquement en utilisant la section ``Sauts`` de l’onglet ``Enchaînements`` des styles de paragraphe pour commencer une nouvelle page après le style de paragraphe qui commence tous les sous-documents (souvent, il s’agit d’un titre de niveau 1 ou 2).
- La convention est de commencer chaque nouvelle partie d’un long document sur une page de droite (impaire). Insérez un saut de page si besoin. 
- Outre les tableaux, les index et les sauts de page, minimisez le contenu qui est ajouté directement au document maître au profit des sous-documents.
- Utilisez les styles de page et/ou les sauts de page manuels pour utiliser une numérotation différente pour les parties du document (préface, introduction, corps principal).

### Création d’un document maître

Les documents maîtres peuvent être fragiles à utiliser. Respecter cet ordre devrait minimiser les problèmes&nbsp;:

1. Créez les sous-documents comme vous le pouvez, même s’ils sont vides. Idéalement, les sous-documents devraient tous utiliser le même modèle.
2. Utilisez ce même modèle pour créer le document maître. Cliquez sur ``Fichier > Nouveau > Document maître`` et enregistrez le document.
3. Le Navigateur s’ouvre dès que vous enregistrez le document maître. Utilisez ses fonctions pour ajouter et positionner tous les sous-documents.
4. Ajoutez toutes les tables des matières, index et bibliographies directement au document maître lorsque les sous-documents sont complets.

### Références croisées entre sous-documents

L’ajout de ``Renvois`` entre deux sous-documents dans un document maître est similaire à l’ajout de ``Renvois`` dans un autre document. Cependant, comme les titres ne s’affichent pas, vous devez définir les références manuellement. Voici une procédure&nbsp;:

1. Ouvrez le sous-document auquel vous souhaitez faire référence (le document source). Vous pouvez l’ouvrir à part ou à partir du document maître.
2. Mettez en surbrillance le texte de la référence et cliquez sur ``Insertion > Renvoi > Définir une référence``. La fenêtre de dialogue ``Champs`` s’ouvre à l’onglet ``Renvois``.
3. Entrez un nom pour la référence. Cliquez ensuite sur le bouton ``Insérer``. Le texte sélectionné est maintenant grisé (il marque un champ). Choisissez un nom qui est unique non seulement dans le sous-document courant, mais aussi dans tous les autres sous-documents.
4. Enregistrer le sous-document source avec la référence.
5. Ouvrez le document cible et sélectionnez ``Insertion > Renvoi > Insérer une référence``.
6. La fenêtre de dialogue ``Champs`` s’ouvre à l’onglet ``Renvois``.
7. Saisissez le nom de la référence dans le document cible.
8. Effectuez une sélection dans le volet ``Insérer une référence à``. ``Chapitre`` est le numéro de chapitre, ``Référence`` est le texte de la référence.
9. Dans le document cible, entrez le texte principal, puis sélectionnez le format suivi du bouton ``Insérer``. Comme le sous-document cible ne peut pas trouver la référence dans le sous-document source, le message ``Erreur&nbsp;: Source de référence non trouvée`` s’affiche.
10. Enregistrer le sous-document cible avec la référence croisée. Lorsque vous rouvrez le document maître, il sera en mesure de localiser la référence et la référence croisée s’affichera à la place du message d’erreur. Les problèmes de pagination dûs au message d’erreur sont également corrigés. Si vous ouvrez le sous-document contenant la référence croisée à l’extérieur du document principal, les zones de référence croisée affichent à nouveau le message d’erreur.

## Au-delà de Writer

Après ces dix chapitres, vous devriez ne plus avoir aucun doute sur l’importance des styles dans Writer. Lorsque vous n’utilisez pas de styles, vous perdez du temps et limitez vos actions. C’est aussi simple que ça.





