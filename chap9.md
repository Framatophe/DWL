# Cadres et images

Les styles de cadres sont souvent les moins bien compris dans LibreOffice. En fait, vous n’avez pas besoin de les comprendre, car Writer ajoute automatiquement des cadres pour contenir des objets comme les images et leurs légendes.  À bien des égards, les styles de cadres servent autant au formatage automatique de Writer qu’aux utilisateurs. 

Une autre raison des angoisses concernant les cadres est qu’il s’avère parfois difficile de conserver les images là où vous les placez. En général, les images restent ancrées dans la page, le paragraphe ou le caractère auquel vous les assignez, et certains utilisateurs n’ont jamais de problèmes. En même temps, on accuse parfois Writer de spontanément changer l’emplacement de l’image, de sorte qu’elles se déplacent sur la page sans raison apparente. Changer les ancres, déplacer des objets, copier-coller, parfois n’importe quel montage semble mettre les objets hors de contrôle.

Ce chapitre concerne à la fois la façon dont les choses sont censées fonctionner (et devraient fonctionner pour vous, avec un peu de chance), et les solutions de contournement dont vous pourriez avoir besoin. 

## Comment fonctionnent les styles de cadre

Lorsqu’un objet est ajouté à Writer, il est automatiquement intégré dans un cadre d’un type prédéfini. Le cadre détermine la façon dont l’objet est placé sur la page, ainsi que la façon dont il interagit avec les autres éléments du document. 

Vous pouvez éditer le cadre en modifiant le style de cadre qu’il utilise, mais vous devez éviter les surcharges manuelles chaque fois que c’est possible. 

Malheureusement, les éléments qui ne sont pas inclus dans un style de cadre, comme l’ancrage, l’alignement, la superposition et la couverture d’une image doivent généralement être édités séparément pour chaque cadre, ce qui rend ce conseil plus facile à donner qu’à appliquer.

<div style="attention">
Attention
Si vous avez des difficultés à positionner quelque chose, la première chose à faire est de vérifier si vous éditez le média (image ou autre) au lieu du cadre, ou le cadre au lieu de l’image.
</div>


## Comprendre les cadres prédéfinis

En raison des problèmes qui surviennent parfois avec les styles de cadres, il est préférable d’éviter de créer des styles personnalisés. Au lieu de cela, préférez éditer les styles de cadres prédéfinis (ceux que l’on trouve dans la fenêtre des ``Styles`` —&nbsp;styles de cadre). Souvent, ils seront tout ce dont vous avez besoin.

Les noms de certains styles de cadres prédéfinis sont explicites, tels que ``Images``, ``Étiquettes`` et ``Formule``. D’autres doivent être expliqués&nbsp;:

- ``Cadre``&nbsp;: à la fois un cadre général et un bloc texte spécifique. C’est le style par défaut.
- ``Note en marge``&nbsp;: cadre qui se trouve sur le côté gauche du bloc de texte principal, créant un bloc de notes dans la marge.
- ``OLE``&nbsp;: *Object Linking and Embedding* (liaison et incorporation d’objets). L’expression est techniquement obsolète, mais se réfère maintenant à un document imbriqué dans un document, y compris les graphiques. De cette façon, un document peut être facilement mis à jour et utilisé dans différentes situations.
- Filigrane&nbsp;: cadres qui positionnent un graphique derrière le texte. Vous devrez préparer le graphique pour l’utiliser avant de l’ajouter. 

## Usage des cadres


L’ajout de graphiques et d’autres objets est souvent un travail secondaire, fait avec un minimum de réflexion sur la mise en page.
Cependant, vous pouvez améliorer votre mise en page en choisissant une stratégie générale.  Voici les tactiques à considérer &nbsp;:

- Placer les images sur des pages séparées du texte. Dans ce cas, vous pouvez utiliser un style de page séparé dont l’utilisation crée automatiquement une nouvelle page.
- Ne pas utiliser de flottant, c’est-à-dire avec du texte tout autour du cadre. Préférez une configuration telle que le flux de texte se fasse au-dessus et au-dessous du cadre, mais pas à sa gauche ou à sa droite. Si vous manquez de temps, c’est toujours une tactique efficace et très courante dans les manuels techniques.
- Décider de l’espace à placer autour du cadre. La mesure de cet espacement doit être un multiple de l’interligne.
- Utiliser une bordure ou un espace blanc supplémentaire lorsque les images ont le même arrière-plan que les pages de votre document (mais la légende apparaît alors en dehors).

Avant de composer, essayez plusieurs solutions pour positionner plusieurs images sur la même page. 

## Préparer vos images

Vous pouvez ajuster l’affichage d’une image à l’aide des outils présents dans les onglets ``Image`` et ``Rognage``. Vous pouvez y accéder en faisant un clic-droit sur l’image puis ``Propriétés``. 

Ces outils n’affectent pas l’image elle-même, seulement son affichage. L’onglet ``Image`` n’a pas de fonction d’annulation, mais vous pouvez facilement changer les paramètres jusqu’à revenir à l’original.

Dans l’onglet ``Image``, vous pouvez retourner une image verticalement ou horizontalement. Vous pouvez affiner l’édition en définissant les pages sur lesquelles l’image peut être retournée. 

Dans le champ ``Lier > Nom``, l’ajout d’un chemin d’accès remplace une image intégrée au document en un fichier séparé et lié. 

![L’onglet Image.](Captures/CH9-ongletimage.png)

### Rognage

Le rognage est l’affichage d’une partie seulement d’une image. Il aide les lecteurs à se concentrer sur la partie pertinente d’une image, mais au risque de perdre le contexte. 

Dans l’onglet ``Rogner``, vous pouvez modifier les dimensions globales d’une image ou n’afficher qu’une partie de l’image. Si vous n’affichez qu’une partie de l’image, vous pouvez sélectionner ``Conserver la taille de l’image`` ou ``Conserver l’échelle`` (en pourcentage de sa taille originale). Vous pouvez annuler ces modifications en cliquant sur le bouton ``Taille d’origine``. 

![L’onglet Rogner.](Captures/CH9-ongletrogner.png)

Tous ces outils peuvent sembler plus pratiques que l’ouverture d’un logiciel de graphisme pour faire des changements. Cependant, étant donné l’instabilité potentielle des cadres, les outils des onglets ``Image`` et ``Rogner`` peuvent être des pièges.

### Édition et résolution de l'image

Vous pouvez utiliser des logiciels libres tels The Gimp, Krita ou Inkscape qui vous permettront d’éditer et de créer des images, en leur donnant de surcroît une résolution correcte, tout particulièrement si vous destinez votre document à l’impression. Ces logiciels peuvent s’installer sur tous les systèmes d’exploitation où fonctionne LibreOffice. 

Vous avez ensuite le choix&nbsp;: soit vous préparez vos images directement, en amont de votre composition, avec ces logiciels spécialisés, soit, une fois l’image insérée dans Writer, vous ouvrez le menu contextuel (clic-droit) et sélectionnez ``Éditer avec un outil externe``. Cette dernière option vous permet d’ouvrir l’image avec l’un des logiciel de graphisme installé par ailleurs sur votre système.

Concernant la résolution de l’image, tout dépend de la destination de votre document. Pour un affichage écran, une résolution de 96 dpi est suffisante. Pour l’impression, en particulier chez un imprimeur professionnel, vous devez réaliser des images d’une résolution au moins égale à 300 dpi. Cela signifie deux choses&nbsp;:

- plus la résolution est grande plus l’image est lourde et vous ne pourrez pas facilement manipuler le fichier final, en particulier si vous voulez l’envoyer à un correspondant,
- toutes les images ne sont pas destinées à l’impression,
- pensez à diminuer la résolution et/ou le degré de compression de vos images si vous voulez que votre fichier soit léger. 

En d’autres termes, pensez à vous renseigner sur les domaines liés à la retouche photographique ou la création d’images (vectorielles ou matricielles)&nbsp;: vous gagnerez largement en efficacité. Comme ce n’est pas le sujet de cet ouvrage, c’est le meilleur conseil que l’on puisse donner.

### Bordures

Vous pouvez ajouter une bordure à une image dans LibreOffice. Cependant, les bord «&nbsp;naturels&nbsp;» d’une image sont tout aussi efficaces, à moins que la couleur de l’image se perde dans le fond de votre page. Si possible, recadrez votre image de manière à ce que tous ses bords soient d’une couleur différente de celle de l’arrière-plan du document. Éventuellement, sans ajouter  de bordure, vous pouvez inclure une ombre, ce qui crée une délimitation, à condition qu’elle soit discrète.

## Insérer une image

Dans LibreOffice, les cadres vous poseront moins de problèmes si vous suivez ces conseils&nbsp;:

- Ajoutez des objets lorsque la mise en page et l’écriture sont terminées. Les objets sont moins susceptibles de se déplacer.
- Si possible, formatez le style du cadre, pas les cadres individuellement. 
- Ajustez les objets immédiatement après les avoir ajoutés. Si nécessaire, essayez d’abord les réglages exacts, en prenant note de tous les réglages. Ensuite, supprimez et ajoutez à nouveau le cadre, en appliquant les paramètres au fur et à mesure.
- Évitez de copier et coller des cadres ou des objets. Supprimez un cadre et recommencez à zéro si vous voulez déplacer un objet.
- Évitez de glisser un objet pour le redimensionner ou le repositionner. Utilisez le menu contextuel.
- Évitez d’ajouter des espaces ou des lignes vides pour positionner des objets. Au lieu de cela, utilisez toujours des styles.
- Évitez de mettre deux ou plusieurs images l’une après l’autre, non séparées par du texte (préférez l’utilisation d’un style de page spécialement dédié à l’insertion d’illustrations).


Lorsque vous sélectionnez une image dans le gestionnaire de fichiers à l’aide de ``Insertion > Image``, LibreOffice incorpore par défaut l’image en tant que partie du document (le fichier final est en réalité un conteneur&nbsp;: si on le décompresse, on y trouve le texte, diverses informations de configuration et surtout un dossier *pictures* qui contient les éventuelles images que l’on a intégrées au document).

Cependant, vous avez la possibilité d’utiliser la case ``Insérer comme lien`` dans le coin inférieur gauche du gestionnaire de fichiers. Si vous sélectionnez cette option, votre document utilisera le fichier original de l’image chaque fois qu’il se chargera (on fera donc appel à un fichier externe qui n’est donc pas inclus dans le fichier du document lui-même).

Voici quelques avantages et inconvénients à prendre en compte&nbsp;:

| Incorporer                    | Lier                          |
|-------------------------------|-------------------------------|
| Le document est plus lourd, car des images sont incluses. | Le document gagne en légèreté. |
| Les images ne peuvent pas être perdues, car elles font partie du document. | Les images peuvent être facilement perdues, il faut bien organiser ses répertoires. |
| Lorsque vous le partagez, vous savez que le document est complet. Il n’y a pas de fichiers supplémentaires à trouver. | Lorsque vous partagez un document, vous devez également envoyer les fichiers image. |
| Les images peuvent être éditées depuis LibreOffice, ce qui est plus pratique. | Les images doivent être éditées dans un éditeur graphique, ce qui donne plus d’options. |
| Les images sont mises à jour en sélectionnant ``Remplacer l’image`` dans le menu contextuel. | Les images sont mises à jour en écrasant le fichier image existant avec un nouveau fichier. Si vous n’utilisez pas ``Outils > Actualiser > Tout actualiser``, Writer met à jour l’image seulement lors de la prochaine ouverture de LibreOffice. | 


<div style="attention">
Attention

Changer l’image peut ne pas fonctionner si vous avez essayé d’échanger une image portant le même nom que l’original. À la place, supprimez l’image originale et insérez la nouvelle.
</div>



## Formatage des cadres et des images

Le menu contextuel comprend des éléments de mise en page pour les cadre et les images. Il est cependant préférable d’utiliser des fenêtres de dialogue.

![Les cadres et les images partagent une fenêtre de dialogue presque identique. Sauf un onglet dédié aux colonnes pour les cadres.](Captures/CH9-boiteialoguecadre.png)

### Redimensionner les cadres et les images

Vous pouvez redimensionner les cadres et les images dans l’onglet ``Type`` de la fenêtre de dialogue. Vous y trouverez plusieurs options&nbsp;:

- Régler la largeur et la hauteur séparément. Attention&nbsp;: ce choix peut facilement déformer l’image. 
- Vous pouvez donc sélectionner ``Conserver le ratio``, puis modifier la largeur ou la hauteur de manière à respecter automatiquement les proportions.
- Utiliser les champs ``Relatif à`` pour définir une largeur et une hauteur correspondant à un pourcentage de la largeur et de la hauteur du paragraphe ou de la page.
- Sélectionner ``Taille automatique``, ce qui permet d’ajuster automatiquement la largeur et la hauteur lors de l’ajout d’un cadre (les changements manuels annulent cet effet).

Pour être sûr de maintenir les images en place, faites les ajustements nécessaires au moment où vous ajoutez l’image. Si nécessaire, faites d’abord l’expérience en prenant des notes sur les dimensions, puis supprimez et ajoutez à nouveau l’image.

### Positionnement des cadres

Les options de la section ``Position`` de l’onglet ``Type`` sont l’équivalent des options d’alignement horizontal et vertical du menu contextuel. 

Les options horizontales sont ``Gauche``, ``Droite``, ``Centre`` et ``De gauche`` (qui s’ajuste avec une mesure). Chacun de ces alignements peut être relatif à divers points de référence. Habituellement, le point de référence le plus utile est la bordure gauche de la page. Par contre, vous ne devriez utiliser le bord de la feuille comme point de référence que si le document est destiné à l’écran ou si votre imprimante est capable de gérer les fonds perdus (c’est-à-dire d’imprimer jusqu’au bord de la page).

Les options verticales sont ``Haut``, ``Bas``, ``Centre`` et ``Du haut`` (qui s’ajuste avec une mesure). Habituellement, le point de référence le plus utile sera la zone de délimitation du texte (la marge). Même remarque que précédemment concernant le bord de la feuille.

<div style="attention">
Attention

Notez que l’option ``Respecter les enchaînements`` est présente pour assurer la compatibilité avec les documents issus des versions anciennes d’OpenOffice.org.
</div>


### Placer des ancres

Une ancre est un point de référence pour le positionnement des cadres et des images. Vous définissez l’ancrage depuis l’onglet ``Type`` pour une image ou un cadre.

La section ``Ancrer`` dispose quatre options&nbsp;: ``À la page``, ``Au paragraphe``, ``Au caractère`` et ``Comme caractère``. Les trois premières indiquent par rapport à quoi un objet est positionné. L’option ``Comme caractère`` indique que l’objet est traité comme un caractère, c’est-à-dire que la ligne sur laquelle l’objet est placé a une hauteur suffisante pour l’afficher… comme un caractère. La sélection par défaut, lorsque vous insérez une image est ``Au paragraphe``. Cependant, l’option ``Comme caractère`` permet de mieux maîtriser l’emplacement, car il peut arriver, en cas d’ancrage au paragraphe, que l’image bascule au-dessus de l’ancre.


### Habillage du texte

L’habillage fait référence à la façon dont le corps de texte est positionné par rapport à un cadre. Les fonctions de l’onglet ``Adaptation`` définissent la façon dont le texte extérieur se déplace autour du cadre ainsi que l’espacement entre le texte et le cadre. En général, un espacement généreux améliore l’apparence de la page.

![Adaptation de l’image.](Captures/CH9-adaptationimage.png)


Writer propose six réglages dans l’onglet ``Adaptation`` pour les cadres et les images&nbsp;:

- ``Aucun``&nbsp;: le cadre interrompt le texte, il n’y a donc pas de texte ni à gauche ni à droite du cadre. C’est un choix courant dans les manuels techniques, parce qu’elle nécessite un temps de positionnement minimal. Cependant, si les images sont petites, l’espace blanc est trop important.
- ``Avant``&nbsp;: le texte s’enroule en haut, en bas et à gauche, en laissant un espace blanc à droite. Ce réglage est particulièrement utile pour les images placées contre la marge droite.
- ``Après``&nbsp;: le texte s’enroule en haut, en bas et à droite, en laissant un espace blanc à gauche. Ce réglage est particulièrement utile pour les images placées contre la marge gauche.
- ``Parallèle``&nbsp;: le texte s’enroule de manière égale sur tous les côtés du cadre, en commençant par la gauche, puis en sautant à travers l’image vers la droite. À moins que le texte ne soit très court, évitez ce positionnement, car la plupart des lecteurs devront se concentrer pour lire le texte.
- ``Continu``&nbsp;: le cadre est placé au-dessus du texte, en le cachant. Si vous utilisez cette option, vous devez également cocher l’option ``À l’arrière-plan`` pour que le texte soit visible. Vous pouvez aussi jouer sur la transparence de l’image.
- ``Optimal``&nbsp;: enveloppe automatiquement le texte sur tous les côtés du cadre. Si le cadre est à moins de 2 centimètres d’une marge, le texte n’est pas enveloppé de ce côté. 

Généralement, ``Optimal`` fait un réglage par défaut raisonnable si un côté du cadre est proche de la marge gauche ou de la marge droite. Cependant, si le cadre est centré, les yeux des lecteurs sautent continuellement, ce qui devrait être évité.

![Six possibilités pour adapter l’image et le texte.](Captures/CH9-adaptationimageprecis.png)


### Espacements autour d’un cadre

Dans l’onglet ``Adapter``, le champ ``Espacement`` permet de définir l’espacement autour de chaque coté du cadre. Il permet de décoller le texte du bord du cadre ou de l’image. Ces réglages sont souvent ignorés, mais ils sont aussi importants que le réglage de l’habillage du texte. Pas assez d’espacement et la page semble à l’étroit, alors que trop d’espacement affaiblit l’association du contenu du cadre avec le texte qui l’entoure.

En règle générale, l’espace blanc minimal autour d’un cadre doit correspondre à la moitié de la hauteur de l’interligne du corps de texte. Il peut aller jusqu’à trois fois la hauteur de l’interligne, mais il est rare d’aller au-delà.

La cohérence est essentielle pour vos compositions. Pour cette raison, vous devez développer une stratégie globale pour l’intégration de vos images et autres objets dans le document.

Que se passe-t-il lorsque l’espacement au-dessus d’une image est ajouté à l’espacement en dessous d’un titre&nbsp;? Trop d’espace blanc. Pire encore, le résultat est que le titre, dont vous avez soigneusement déterminé les espacements dans le style correspondant, se trouve maintenant à une position qui ne correspond à rien de ce que vous souhaitiez. Pour éviter ce genre de problèmes, vous devez décider que, dans certains cas, une image devrait utiliser moins d’espace blanc que d’habitude.

Voici un exemple de stratégie qui a été utilisée dans une des versions originales de ce livre&nbsp;:

- Les images, les tableaux et les paragraphes ont tous un minimum de 8 points d’espace blanc au-dessus et en dessous.
- Si un autre titre ou un autre élément donne suffisamment d’espace blanc au-dessus ou en dessous, alors un cadre ne peut pas ajouter d’espace blanc lui-même. Il s’agit d’être cohérent et de ne pas avoir de grands écarts.
- La largeur par défaut de toutes les images est de 280 points (la largeur entre les marges).
- Les images peuvent être inférieures à la largeur totale de la page.
- Les images sous une puce ou un élément numéroté, ou un paragraphe en retrait, s’aligneront avec le début du texte au-dessus d’eux. Cette règle consiste à rendre les images 8 points plus étroites que la normale, et à mettre les tables en retrait de 16 points à partir de la gauche dans l’onglet ``Tableau``.
- Les grandes images ou tableaux seront sur des pages séparées, introduites par un titre qui force un nouveau saut de page.

### Autres options d’habillage

L’onglet ``Adapter`` comprend plusieurs ``Options`` qui modifient le fonctionnement des ``Paramètres``&nbsp;:

- ``Premier paragraphe``&nbsp;: commence un nouveau paragraphe sous le cadre si vous appuyez sur la touche ``Entrée``. Puisque ce choix ajoute de l’espace en fonction de la taille de l’objet, vous aurez plus de contrôle si vous ignorez ce paramètre et ajustez exactement l’espace blanc en utilisant les champs relatifs à l’``Espacement``.
- ``À l’arrière-plan``&nbsp;: disponible uniquement avec le paramètre ``Continu``. Il s’agit de traiter le cadre comme un objet dans une pile d’objets, en envoyant son contenu à l’arrière pour que le corps du texte soit à l’avant. Cette option équivaut à cliquer avec le bouton droit de la souris sur l’image ou le cadre et à sélectionner ``Disposition > Envoyer vers l’arrière``.
- ``Contour``&nbsp;: enveloppe le texte plus étroitement à la forme de l’objet dans le cadre que si vous choisissez un paramètre d’habillage. Ce réglage peut donner un habillage plus exact, mais il peut aussi donner à la page un aspect distrayant et occupé, surtout lorsque le cadre est petit ou a une forme compliquée.
- ``Uniquement à l’extérieur``&nbsp;: comme ``Contour``, mais en ignorant les espaces vides dans l’objet. Il ne peut pas être utilisé avec des cadres (des cadres de texte), probablement parce que le texte est trop régulier pour que LibreOffice puisse faire la différence. 

### Options générales

L’onglet ``Options``, nommé de façon ambiguë dans la fenêtre de dialogue, contient diverses options d’édition et d’impression&nbsp;:

- ``Nom``&nbsp;: pour le cadre ou une image (dans un cadre), Writer ajoute un nom automatiquement, tel que Image1, mais un nom plus descriptif est utile si vous utilisez le ``Navigateur``. Les autres champs sont ``Alternative`` et ``Description``. Leur utilité est de l’ordre de l’accessibilité d’un texte&nbsp;: les images ou les cadres seront ainsi accompagnés de balises qui permettent, au cas où un dispositif de lecture ne permet pas d’afficher l’image ou le cadre, de remplacer l’objet par son titre et sa description. C’est particulièrement important pour les utilisateurs malvoyants et les logiciels de lecture audio qui lisent ces balises.
- ``Protéger``&nbsp;: empêche l’édition du contenu, de la position et de la taille. Ces options sont utiles lorsqu’un document a plusieurs éditeurs.
- ``Imprimer``&nbsp;: inclut le cadre ou l’image lorsque vous imprimez.


![L’onglet des options.](Captures/CH9-ongletoption.png)

### Ajouter des colonnes

Vous pouvez définir plusieurs colonnes pour un bloc texte d’un cadre dans l’onglet ``Colonnes`` de la fenêtre de dialogue. Vous préférerez peut-être insérer une section à partir de ``Insertion > Section``&nbsp;: en effet les sections ont plus d’options que les cadres. 

### Ajout d’hyperliens et de macros

Dans les documents destinés à une lecture écran, vous pouvez ajouter des hyperliens (liens) et des macros qui s’exécutent lorsque vous cliquez sur un cadre ou une image.

Vous avez besoin du nom du cadre dans l’onglet ``Options`` pour configurer un hyperlien. 

Vous pouvez utiliser des macros prédéfinies, mais pour enregistrer une macro dans LibreOffice, vous devez d’abord sélectionner ``Outils > Options > LibreOffice > Avancé > Fonctionnalités optionnelles > Activer l’enregistrement de macro (peut-être limité)``.

## Les légendes

Le but d’une illustration peut être clair à partir du texte qui l’entoure, surtout si le paragraphe au-dessus l’introduit avec un double point.

Cependant, une légende ajoute de la clarté en utilisant des mots-clés du texte. Une légende peut également expliquer la pertinence de l’image ou des parties de l’image. Utilisée avec soin, elle peut aider à donner des informations détaillées avec moins d’espace qu’elle n’en prendrait dans le corps du texte. Un autre intérêt est que dans les documents électroniques, la légende peut être utilisée comme référence croisée.

Lorsque vous composez une légende, vous pouvez penser que c’est une évidence mais ce qui est évident pour vous l’est souvent beaucoup moins pour les lecteurs.



![Fenêtre de dialogue pour les légendes.](Captures/CH9-legende.png)

Pour ajouter une légende&nbsp;:

1. Cliquez avec le bouton droit de la souris n’importe où dans l’image et sélectionnez ``Insérer une légende`` dans le menu contextuel. La fenêtre de dialogue s’ouvre.
2. Ajouter la légende au champ ``Légende``.
3. Définissez éventuellement les champs ``Catégorie`` et ``Numérotation`` (et le séparateur entre le numéro et le texte).
4. Réglez la ``Position`` de la légende au-dessus ou en dessous. Généralement on trouve les légendes sous les objets. 
5. Pour préfixer le numéro de l’image avec un niveau de titre (chapitre, section, sous-section, etc.), cliquez sur le bouton ``Options`` et sélectionnez le niveau de titre désiré. Vous pouvez également choisir un style de caractère spécial pour l’écriture de ce niveau de titre. 
6. Dans la sous-fenêtre ``Options``, décidez s’il faut cocher la case ``Appliquer des bordures et des ombres`` à la légende. Évitez de les utiliser si vous ne pouvez pas expliquer à quoi elles servent. Souvent, vous pouvez vous passer de l’un ou de l’autre.
7. Dans la sous-fenêtre ``Options``, définissez l’ordre des légendes, en plaçant le numéro du titre ou la catégorie en premier. Pour ajouter un numéro de chapitre, vous devez attribuer un style de numérotation à un style de paragraphe, puis affecter le style de paragraphe à un niveau de titre.



### Mise en page des légendes

Les paragraphes de légende sont formatés à l’aide du style ``Légende``. Contrairement à l’usage courant, il est en italique par défaut, mais une écriture droite est plus courante.

De même, le style n’a pas besoin d’être plus petit que le corps du texte. Si vous voulez économiser de l’espace, utilisez une police de caractères étroite ou condensée pour les légendes. 

De plus, la légende devrait avoir moins d’espacement entre elle et l’image qu’entre elle et le texte en dessous ou au-dessus. La proximité est l’un des moyens fondamentaux par lesquels on indique que deux parties du document sont liées. 

### Légendes automatiques

Les légendes automatiques ajoutent immédiatement une légende à tous les objets spécifiés, en utilisant le style de paragraphe de légende. Il faut pour cela activer  ``Outils > Options > LibreOffice Writer > Légende automatique`` puis configurer selon les mêmes principes que l’insertion d’une légende.
 
![Configurer les légendes automatiques.](Captures/CH9-legendeautomatique.png)

## Tenir les images en place

La plupart des utilisateurs n’ont aucun problème avec les images. Beaucoup d’autres trouvent qu’elles se déplacent constamment. Bien que personne n’ait suggéré une raison pour cela, elles peuvent être dues aux différentes habitudes de travail des utilisateurs, ou au système d’exploitation ou encore à la version de LibreOffice utilisée. 

Cependant, deux solutions de contournement sont connues pour Writer&nbsp;: la solution de Haugland et celle des tableaux. La solution des tableaux limite certaines options, il faut donc l’utiliser qu’après l’échec de la première solution.

### Solution de Haugland

En 2009, Solveig Haugland, l’une des premières personnes à avoir écrit sur OpenOffice.org, a conçu une solution pour rendre les images «&nbsp;raisonnablement gérables&nbsp;». Sa solution est toujours la plus fiable qu’on puisse trouver.

La voici, reformulée et légèrement retravaillée&nbsp;:

1. Créez une nouvelle ligne et cliquez sur ``Insertion > Image`` pour ajouter l’image.
2. Cliquez sur ``Propriétés`` dans le menu contextuel et allez dans l’onglet ``Type``.
3. Ajustez la largeur et la hauteur de l’image en utilisant les champs correspondants. Vous pouvez aussi cocher la case ``Conserver le ratio``, pour éviter de déformer l’image.
4. À l’aide du menu contextuel, réglez l’ancre sur ``Comme caractère``. Ce choix permet de traiter l’image de la même façon qu’une lettre ou un chiffre.
5. Réglez l’alignement horizontal et vertical si nécessaire. L’alignement vertical a rarement besoin d’être ajusté, alors que l’alignement horizontal peut généralement rester sur la gauche.
6. Effectuez les autres changements de formatage que vous désirez et fermez la fenêtre de dialogue. 
7. Si vous trouvez que vous avez besoin de faire des changements plus tard, l’approche la plus sûre est de supprimer l’image et de l’ajouter à nouveau avec les changements. 

### Solution du tableau

Si vous continuez à avoir des problèmes avec les cadres et les objets, essayez de les remplacer par des tableaux. En effet, le tableau agit comme un cadre, sauf que les images dans un tableau sont moins susceptibles de se déplacer. Cette solution est similaire à l’utilisation de tableaux pour la mise en page d’une page Web (aujourd’hui ce genre de procédé n’a plus court).

Cette solution de contournement vous donne seulement la possibilité d’envelopper votre objet par du texte au-dessus et en dessous du tableau. Vous devrez aussi insérer la légende dans une cellule du tableau.

Pour utiliser cette solution&nbsp;:

1. Créez un style de paragraphe que vous nommez ``Espacement d’image`` avec ``Retraits et espacement > Interligne`` réglé sur ``Fixe``, et un interligne de 0 points.
2. Vérifiez que la légende automatique des tableaux est désactivée dans ``Outils > Options > LibreOffice Writer > Légende automatique``.
3. Placez le curseur sur une ligne vide. Si nécessaire, changez le style du paragraphe à ``Style par défaut`` pour éliminer les nombres, les puces ou les indentations inutiles.
4. Sélectionnez ``Tableau > Insérer un tableau``, et faites un tableau avec 1 colonne et 2 lignes (ou 1 colonne et 1 ligne si vous n’utilisez pas de légende).
5. Avant d’insérer, désélectionnez ``En-tête`` et ``Bordure`` et sélectionnez ``Ne pas scinder le tableau à travers les pages``.
6. Placez le curseur dans le tableau, cliquez avec le bouton droit de la souris (ou allez dans le menu ``Tableau``) et sélectionnez ``Propriétés du tableau`` pour ajouter des espaces au-dessus et au-dessous du tableau. Comme la plupart des mesures, celles-ci doivent généralement être des multiples de l’interligne. Cependant, si les paragraphes au-dessus ou en dessous ont déjà un espacement supplémentaire, ajustez l’espacement de la table pour éliminer les éventuelles additions.
7. Si vous voulez une indentation à partir de la gauche, définissez l’alignement sur ``De gauche`` et définissez l’indentation dans le champ ``Espacement > Gauche``. Cela permet d’aligner l’image avec le texte en retrait, ce qui est un moyen utile de montrer à quel texte l’image se réfère.
8. Placez le curseur sur la ligne supérieure du tableau et cliquez sur ``Insertion > Image`` pour ajouter l’image. 
9. Cliquez avec le bouton droit de la souris sur l’image, cliquez sur ``Propriétés`` et rendez-vous dans l’onglet ``Adapter``. Réglez l’espacement entre le haut et le bas à 0 pour placer l’image dans le coin supérieur gauche du tableau. De cette façon, vous n’avez qu’à déplacer la table, et vous pourrez oublier l’image.
10. Sous l’image dans la rangée du haut, appliquez le style de paragraphe ``Espacement d’image`` que vous avez créé. Le bas de l’image est maintenant aligné avec le côté inférieur de la rangée du haut.
11. En utilisant le style de paragraphe ``Légende``, ajoutez la légende à la deuxième ligne. Assurez-vous que votre formatage positionne la légende plus près de l’image que du texte sous le tableau. N’utilisez pas de légendes automatiques car c’est le tableau qui serait alors légendé.

Bien que toutes les options d’habillage ne soient pas disponibles avec cette solution de contournement, vous pouvez utiliser deux colonnes dans le tableau, au lieu de deux lignes, et placer la deuxième colonne à gauche ou à droite de l’image. 


## Utilisation avancée des cadres

Le formatage de base des images est simple. Cependant, certaines utilisations de cadres sont moins évidentes. Cette section couvre quelques usages.

### Créer des marginalia

Une fois formatés, la plupart des cadres sont appliqués automatiquement. Une exception majeure sont les marginalia —&nbsp;des titres ou des petits textes qui sont placés à gauche d’un corps de texte dans une deuxième colonne. Les marginalia peuvent causer beaucoup d’espace blanc à une page mais, selon la conception, tel sera l’effet esthétique recherché.

Les marginalia ont été nommées pour la première fois au Moyen Âge, lorsque le papier était si cher qu’il n’était pas gaspillé. Au lieu de prendre des notes sur une nouvelle feuille de papier, les copistes ajoutaient des commentaires ou même des dessins dans les marges. Dans Writer, les marginalia ne sont techniquement pas placés dans les marges - ils en donnent juste l’impression. Cela signifie que vous pouvez souvent vous en tirer avec une marge gauche plus étroite que d’habitude.

Le principal inconvénient des marginalia est qu’elles doivent être ajoutées manuellement.

![Exemple de marginalia.](Captures/CH9-marginalia.png)


Pour créer des marginalia&nbsp;:

1. Dans votre style corps de texte, définissez une indentation  dans ``Retraits et espacement > Avant le texte`` de sorte que le texte commence à droite du cadre de marginalia. Laissez un espace généreux.
2. Ajoutez le texte principal au document.
3. Sélectionnez ``Insertion > Cadre`` pour ajouter un bloc de texte. Utilisez le format suivant&nbsp;:
      - Ancrez-le au paragraphe. 
      - Ôtez la bordure.
4. L’ajout du cadre ajoute le style de paragraphe ``Contenu de cadre``. Modifiez ce style pour formater le contenu des marginalia. 
5. Donnez à chaque cadre marginalia la même largeur que le premier.
6. Ajoutez des images et d’autres objets pour qu’ils s’alignent avec le début du texte.

### Création de filigranes

À l’origine, un filigrane était un logo qui identifiait le fabricant de papier. 
Aujourd’hui, il s’agit d’une image discrète dans l’arrière-plan d’une page. Elle peut éventuellement donner le statut du texte, par exemple, «&nbsp;Brouillon&nbsp;» ou «&nbsp;Confidentiel&nbsp;». 

Le texte et les objets du document apparaissent au-dessus du filigrane. Le style de cadre de filigrane positionne un cadre d’image pour une utilisation comme filigrane. 

Pour créer un filigrane&nbsp;:

1. Créez un cadre et ajoutez-y une image ou un texte. 
2. Sélectionnez le cadre et appliquez le style de cadre ``Filigrane``.
3. Si ce n’est déjà fait, dans l’onglet ``Adapter``, sélectionnez ``Continu`` et cochez ``À l’arrière-plan``.

Si vous utilisez une image, le mieux est encore de la créer avec un logiciel dédié, et lui donner un effet de transparence. Si vous utilisez du texte, créez votre cadre, donnez au texte une couleur très légère (par exemple du gris très clair) et appliquez le style de cadre ``Filigrane``.

![Exemple de filigrane.](Captures/CH9-filigrane.png)



### Flux de texte entre cadres

Dans les mises en page un peu complexes, l’utilisation de plusieurs blocs de texte peut être plus facile que d’utiliser des sections ou des colonnes. Par exemple, si vous créez une brochure sur une seule feuille, les pages intérieures (pages 2-3) sont deux cadres reliés. 

Pour que le texte passe d’un cadre à l’autre&nbsp;:

1. Créez deux ou plusieurs cadres à l’aide de ``Insertion > Cadre``, et laissez-les vides.
2. Sélectionnez le premier cadre.
3. Cliquez sur l’icône ``Enchaîner les cadres`` dans la barre d’outils ``Cadres`` (la barre d’outils ne s’affiche que lorsqu’un cadre est sélectionné).
4. Sélectionnez le cadre cible (celui dans lequel le texte s’affichera à partir du premier cadre). Lorsque les cadres sont connectés, une ligne bleue passe entre eux quand un cadre est sélectionné.
5. Modifiez les cadres au besoin.


<div style="astuce">
Astuce

Vous pouvez ancrer deux cadres de texte dans l’en-tête d’un style de page, puis positionner les blocs sur la page et les lier&nbsp;: la même disposition des blocs apparaît sur chaque instance du style de page.
</div>

![Deux cadres liés.](Captures/CH9-2cadreslies.png)

Si vous avez des problèmes en liant deux cadres, les raisons peuvent être les suivantes&nbsp;:

- le cadre cible a déjà du contenu,
- le cadre cible est déjà lié avec un autre cadre (un cadre ne peut recevoir de texte que d’un seul cadre et n’envoyer le flux qu’à un seul autre cadre),
- les deux cadres appartiennent à des sections différentes ou sont contenus dans des en-têtes ou des pieds de page différents,
- un cadre est imbriqué dans l’autre.


## Trouver des solutions

Si vous pensez que travailler avec des styles de cadres ou des images est inutilement complexe… vous avez raison. Une partie de cette complexité est due au fait que LibreOffice offre plus d’options qu’un logiciel de traitement de texte moyen, mais la plus grande difficulté est de garder les images là où vous les placez.

