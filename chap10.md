# Listes et tableaux

Les listes et les tableaux sont parfois appelés des structures de données. C’est ce qui les oppose à l’absence relative de corps de texte. 

Le texte structuré est facile à lire, mais il prend plus d’espace que le corps de texte. Cet espace supplémentaire n’est pas un problème pour une lecture à l’écran, mais il a tendance à manger du papier.

Dans la plupart des logiciels de traitements de texte, les options de liste sont incluses dans les styles de paragraphe, alors que les styles de tableau ne sont pas  fournis. Ce n’est pas le cas de Writer.

Les styles de liste et de tableaux ont plusieurs avantages&nbsp;:

- les listes ont plus de choix de formatage,
- le même style de liste peut être utilisé avec plusieurs styles de paragraphes, évitant ainsi la duplication du travail de conception,
- le style de liste d’un paragraphe peut être modifié avec une seule sélection,
- les styles de tableaux permettent de formater des tableaux en quelques clics.


## Comprendre les types de liste

Avec les styles de liste, vous pouvez créer&nbsp;:

- Des listes à puces&nbsp;: listes non ordonnées dont les éléments commencent par une puce, un caractère spécial, un symbole ou une image.
- Des listes numérotées&nbsp;: listes ordonnées dont les éléments commencent par un chiffre, des lettres majuscules ou minuscules, ou des chiffres romains majuscules ou minuscules.
- des listes multi-niveaux&nbsp;: chaque niveau a son propre système de numérotation.

Tous ces types de listes sont d’usage courant aujourd’hui. Cependant, ce que la plupart des gens n’apprécient pas, c’est que chacun a ses propres conventions sur la façon dont elles doivent être structurées et utilisées —&nbsp;des conventions qui sont rarement enseignées dans les écoles et que la plupart des gens n’observent ou n’apprennent jamais.

## Comprendre les listes à puces

Les puces proviennent probablement des points médians, le séparateur universel utilisé dans les manuscrits européens médiévaux. Cependant, les points médians ont été utilisés pour remplir autant que possible chaque ligne parce que le parchemin et les autres matériaux d’écriture étaient coûteux.

En revanche, les puces prennent plus de place. Elles ont profité de la faible valeur  du papier par rapport au vélin pour l’impression.

Plus récemment, bien sûr, les formats électroniques n’ont plus de problème d’espace de sorte que les puces peuvent être utilisées à volonté sans augmenter les frais de publication. Si les listes à puces sont devenues populaires dans les documents techniques, les diaporamas ont répandu largement leur utilisation.

![Types de puces.](Captures/CH10-typesdepuces.png)


## Conventions pour les listes à puces

L’utilisation des listes à puces suit des règles bien définies&nbsp;:

- Elles ne sont utilisés que lorsque l’ordre des points n’est pas pertinent. Vous pourriez vouloir arranger les items pour obtenir un effet rhétorique, mais les lecteurs ne devraient pas avoir besoin de connaître un item avant un autre.
- Elles doivent avoir trois points ou plus. Si vous n’avez que deux item, ils peuvent rester tous les deux dans le corps du texte.
- Elles ne sont généralement pas plus longue de 6 à 8 item. Au-delà, la facilité de lecture est perdue.
- Elles sont introduites dans la dernière phrase du corps du texte au-dessus, soit sans ponctuation, soit avec un double point.
- Elles ont une cohérence grammaticale.

Plus précisément, pour la forme&nbsp;:

- Commencez par une puce dans la marge de gauche, puis indentez le texte. S’il s’agit d’une liste à l’intérieur d’une autre, la puce a la même indentation que le niveau supérieur.
- Terminez chaque point par la même ponctuation. Les virgules, les points-virgules et les points sont tous des choix valables. Ce qui importe, c’est la cohérence des éléments de la liste et de toutes les listes dans le document.
- N’utilisez pas un «&nbsp;et&nbsp;», un «&nbsp;ou&nbsp;», ou tout autre mot pour introduire un item de liste.

<div style="astuce">

Astuce

Pour la typographie française, il est préférable d’utiliser des tirets (—) et non des traits d’union (-) au lieu de points (*bullets*) comme c’est l’usage en particulier sur le Web ou dans des diaporamas.

Pour cela il vous devez créer un style de puce. Puis, dans la boîte de dialogue, rendez-vous dans l’onglet ``Personnaliser``. Sélectionnez le niveau de liste concerné. Pour le ``Nombre`` choisissez ``Puce`` et pour le ``Style de caractère``, choisissez aussi ``Puce``. Cliquez ensuite sur le bouton ``Sélectionner`` dans le champ ``Caractère``. De préférence, choisissez dans la liste la police que vous utilisez pour le corps de texte&nbsp;: elle devrait comporter différents tirets dont vous sélectionnerez celui d’une dimension moyenne (appelé ``en dash``).
Continuez d’éditer votre style de puce et validez.
</div>

## Comprendre les listes ordonnées

Bien que beaucoup d’utilisateur ont tendance à toujours numéroter leurs listes, la convention veut que les listes numérotées ne devraient être utilisées que lorsque l’ordre de l’information a de l’importance. C’est typiquement le cas lorsque vous vous décrivez une procédure. 

![Numérotation de liste.](Captures/CH10-typedelistenumerotee.png)

## Conventions pour les listes numérotées

Les listes numérotées obéissent à quelques règles&nbsp;:

- Elles sont utilisées lorsque l’ordre des items est important. Si l’ordre n’est pas pertinent, utilisez plutôt des listes à puces.
- tout comme les listes à puces, elles ne sont utilisées qu’à partir de trois item ou plus.
- Elles sont généralement introduites dans le corps du texte par un résumé de ce qu’elles décrivent et qui se termine par un double point. 
- Chaque item peut comporter plusieurs paragraphes, la plupart non numérotés.

## Comprendre les listes multi-niveaux

Les listes multi-niveaux sont plutôt adaptées à la structure d’un document long. Dans les documents techniques et juridiques, elles sont utilisées dans les niveaux de titre pour rendre la structure évidente, bien que cette utilisation soit de moins en moins courante.

Dans Writer, les styles de liste créent une méthode qui utilise un style de paragraphe unique. Lorsqu’un tel style de paragraphe est utilisé, vous pouvez changez le niveau et la numérotation en appuyant sur la touche ``Tab`` pour descendre d’un niveau, et ``Maj + Tab`` pour monter d’un niveau. 

![Onglet Plan dans un style de liste.](Captures/CH10-listnivplan.png)


Quelques règles doivent être suivies&nbsp;:

- Un système de numérotation différent est utilisé pour chaque niveau afin de les distinguer. 
- Les niveaux peuvent être introduits en chiffres arabes, en chiffres romains capitales et minuscules, et en lettres capitales et minuscules.
- Les chiffres romains en majuscules sont généralement réservés au niveau supérieur, et les lettres capitales sont utilisées avant les lettres minuscules. On peut aussi utiliser les chiffres arabes suivis de lettres minuscules. Toutefois, ces règles ne sont pas rigides.
- Dans les manuels techniques, il était courant de voir plusieurs niveaux dans une rubrique (par exemple, I.A.2 ou 1.1.1.1.1). Ces numérotations de titres ont largement disparu de la pratique, sauf dans quelques cas spécialisés, comme les documents juridiques. De plus, si chaque niveau est indenté, comme c’était courant à l’époque des machines à écrire où le choix de formatage était limité, après deux ou trois niveaux, il ne reste presque plus d’espace pour le texte.

## Nommer les styles de listes

Dans la fenêtre des ``Styles de liste``, LibreOffice utilise les noms ``Puce 1-5`` pour les listes à puces et ``Numérotation (123, abc, ABC, ivx, IVX)`` pour les listes ordonnées. Cependant, même si ces styles couvrent une bonne partie des besoins, vous serez amené à créer vos propres styles de listes. Tout particulièrment si vous liez vos styles de listes à vos styles de paragraphes, il est préférable de donner des noms qui correspondent entre eux.


Dans la fenêtre des ``Styles de paragraphes``, LibreOffice inclut également les styles de liste se terminant par ``début``, ``fin`` et ``suite`` (pour les numérotations) ou ``suivante`` (pour les puces). Vous pouvez utiliser ces styles pour personnaliser les listes. Par exemple, le style de liste de début peut avoir un espacement supplémentaire au-dessus pour séparer la liste du corps du texte, et le style de liste de fin peut comporter un espacement supplémentaire en dessous.

Le style de liste numérotée de ``suite`` est parfois utilisé pour les paragraphes non numérotés d’une liste qui ont un format différent. Cependant, le nom suggère de l’utiliser avec un style de liste dont le champ ``Numérotation`` est défini sur ``Aucun`` dans l’onglet ``Plan et numérotation``. Idem pour les listes à puce (``suite``) ou l’équivalent dans le champ ``Avant``. 


![liste ordonnée début, suite et fin.](Captures/CH10-Numerotationdebutsuitefin.png)

## Créer et appliquer un style de liste


L’approche la plus efficace consiste à créer un style de paragraphe auquel on attache un style de liste. Créez un style de paragraphe (donnez-lui un nom, un style de suite, etc.) puis allez dans l’onglet ``Plan & Numérotation`` et renseignez le champ ``Style de numérotation``. Sélectionnez le style de liste dans la liste déroulante. 

Vous pouvez ensuite appliquer ce style comme n’importe quel autre style, en plaçant le curseur dans un paragraphe, puis en sélectionnant le style de liste dans la fenêtre des ``Styles``.

## Configurer les styles de liste

Vous avez deux manière de formater les puces et les chiffres dans les styles de liste. La plus rapide est de sélectionner un style dans les onglets ``Puces``, ``Style de numérotation``, ``Plan`` ou ``Image``. Chacun de ces onglets donne une variété d’options, bien qu’il ne s’agisse pas d’une liste exhaustive.

Cependant, vous devriez éviter d’utiliser n’importe quel style dans l’onglet ``Image`` à moins que vous ne vouliez un look rétro du milieu des années 1990.

![Des puces amusantes.](Captures/CH10-puces1990.png)




La deuxième manière, plus pratique, est de personnaliser les puces ou les listes numérotées pour vous-même, à l’aide des onglets ``Position`` et ``Personnaliser``.

Ces onglets proposent jusqu’à dix niveaux. Ce paramètre est surtout utile pour créer des listes multi-niveaux. Pour la plupart des listes, vous pouvez vous contenter de réglez le niveau à 1 ou appliquer à tous les niveaux 1-10. Si vous vous embrouillez dans la configuration du style de liste que vous créez, vous pouvez réinitialiser avec le bouton correspondant.

### Positionner des puces, des nombres et les items

L’onglet ``Position`` permet de définir l’espacement avant les puces ou les chiffres, et entre la puce ou le chiffre et le texte.

Lorsqu’un style de liste est lié à un style de paragraphe, l’édition des champs de l’onglet ``Position``  entraîne des modifications des paramètres ``Retraits et espacement`` (``Avant le texte`` et ``Première ligne``) pour ce style de paragraphe.

![Onglet Position pour un style de liste.](Captures/CH10-ongletpositionstyleliste.png)

L’inverse est également vrai. Cependant, pour éviter les complications, effectuez tous les changements dans l’onglet ``Position`` pour le style de liste. Non seulement c’est l’endroit logique pour rechercher des changements dans le style de la liste, mais l’ajustement des paramètres de paragraphe implique généralement des entrées négatives pour le champ ``Première ligne``, ce qui peut compliquer énormément l’édition.

![Les différents champs de l’onglet Position.](Captures/CH10-alignementsliste.png)

Lorsque vous définissez un nouveau style de liste, les champs importants de l’onglet ``Position`` sont&nbsp;:

- ``Aligné à``&nbsp;: la position pour les nombres, mesurée à partir de la marge gauche.
- ``Alignement de la numérotation``&nbsp;: définit comment la puce ou le numéro est aligné. La plupart du temps, vous pouvez laisser ce champ par défaut à gauche, mais si vous avez de la difficulté à positionner le texte, changer l’alignement au centre ou à droite peut parfois résoudre le problème, surtout pour les listes ou les niveaux qui nécessitent des numéros à deux chiffres.
- ``Numérotation suivie par``&nbsp;: définit l’espacement entre le numéro ou la puce et le texte. Le choix qui permet de contrôler au mieux cette valeur est ``Tabulation à``. 
- ``Aligné à```: situe le numéro ou la puce sur la ligne. Zéro point signifie contre la marge. 
- ``Retrait à``&nbsp;: Définit le retrait du début du texte. Ce paramètre doit être égal ou supérieur à la tabulation définie pour la ``Numérotation suivie par``.


![L’alignement des nombres ou des puces.](Captures/CH10-alignementsliste2.png)

### Formatage de listes ordonnées (numérotées)

Pour créer une liste numérotée, sélectionnez un style de numérotation dans le champ ``Nombre`` de l’onglet ``Personnaliser``. La liste déroulante commence par des choix typiques pour les langues d’Europe occidentale&nbsp;: chiffres arabes, lettres majuscules et minuscules, et chiffres romains majuscules et minuscules. Faites défiler vers le bas, et des options pour le bulgare, le russe, le serbe et le grec sont disponibles.

![Choix des types de numéros.](Captures/CH10-listestyledenombre.png)

#### Ajouter des caractères avant et après les numéros

Vous pouvez définir jusqu’à 40 caractères avant ou après le numéro à l’aide des champs ``Avant`` et ``Après``. Ces caractères sont ajoutés automatiquement chaque fois que le style de liste auquel ils sont attachés est appliqué.

Les caractères communs après un nombre peuvent être un point, une parenthèse ou les deux. Alternativement, vous pouvez mettre une parenthèse avant ou après le nombre, ou un texte. Par exemple l’occurrence «&nbsp;(Suite)&nbsp;» en le plaçant après une puce ou un numéro ou bien «&nbsp;Étape&nbsp;», situé avant un numéro si vous décrivez une procédure.

De manière plus élaborée, vous pouvez définir un style de paragraphe qui commence en haut de page, puis y joindre un style de liste avec du texte pour que ce style de paragraphe ajoute automatiquement le texte. 

Dans une liste numérotée, vous pouvez choisir le nombre de niveaux hiérarchiques dans la liste en ajustant le champ ``Afficher les sous-niveaux``. Par exemple, si vous avez décidé d’afficher trois sous-niveaux, la première utilisation du troisième sous-niveau sera numérotée 1.1.1.

#### Numéros à deux chiffres

Les numéros à deux chiffres dans les listes ordonnées peuvent déplacer les éléments de la liste, perturbant votre mise en page en décalant trop le texte.

Vous avez plusieurs possibilités pour corriger ce problème&nbsp;:

- Ne jamais avoir de liste de plus de neuf items. Vous pouvez réécrire les listes en les divisant en listes plus courtes.
- Ajoutez de l’espace supplémentaire entre le numéro et l’item en utilisant le champ ``Retrait à``. N’ajoutez pas trop d’espace pour ne pas perdre l’association entre les chiffres et les items.
- Ajustez la taille des nombres à l’aide de ``Personnaliser > Style de caractères``.
- Définissez l’alignement de numérotation à droite. Veillez à ce que ce changement n’étende pas les chiffres dans la marge de gauche.

#### Redémarrer le compteur d’une liste ordonnée

L’onglet ``Personnaliser`` d’un style de liste comprend un champ ``Commencer avec``. Toutefois, notez que cette zone se réfère à la première fois que le style de liste est utilisé dans un document. Ce n’est pas un outil pour recommencer la numérotation.

Pour redémarrer la numérotation dans une liste, cliquez avec le bouton droit de la souris sur un paragraphe de liste et sélectionnez ``Recommencer la numérotation`` dans le menu.

#### Inversion de l’ordre des numéros

Il peut arriver que vous ayez besoin de listes numérotées qui comptent à rebours. Malheureusement, LibreOffice ne fournit pas d’autre moyen qu’une macro personnalisée pour inverser l’ordre dans les listes.

Une liste à rebours doit être saisie manuellement, LibreOffice ne générera pas automatiquement des numéros.

## Listes non ordonnées (puces et images)

Le style de caractère par défaut pour les puces est point médian. Toutefois, vous pouvez modifier ce caractère à l’aide de la zone ``Style de caractère``. La sélection de ce champ ouvre une fenêtre de dialogue dans laquelle vous pouvez choisir n’importe quel symbole supporté par la police courante ou une autre police installée sur votre système (il est préférable de rester dans le choix de la police utilisée pour le corps de texte).

Si vous utilisez des listes à puces imbriquées —&nbsp;c’est-à-dire des listes de puces dans des listes de puces&nbsp;— vous pouvez créer un style de liste supplémentaire avec un nom comme ``Mespuces2``. Cependant, si vous utilisez plus d’un style de liste à puces, assurez-vous que leurs esthétiques soient compatibles. Normalement, l’indentation des listes imbriquées devrait être suffisant pour distinguer les puces des niveaux supérieurs et inférieurs.

L’utilisation d’une image au lieu d’une puce standard est le moyen d’ajouter de l’originalité à votre document. Cependant, vous êtes limité par la petite taille à laquelle la plupart des puces s’affichent. Pour cela, vous avez besoin d’images simples avec un fort contraste. Souvent, une image en noir et blanc sera plus efficace.

Les images utilisées pour les puces sont aussi un moyen de positionner une image sur une page. En particulier, elles peuvent être utilisés pour introduire des encarts (comme un petit panneau d’avertissement) dans un manuel technique.

Pour ce faire, sélectionnez une image à utiliser dans ``Personnaliser > Nombre`` dans la fenêtre de dialogue pour le style de liste. Choisir ``Image`` intègre l’image dans le document (l’option ``Galerie`` vous donne accès aux puces-images déjà intégrées par défaut dans LibreOffice). Une ``Image liée`` est une image qui ne sera pas intégrée dans le fichier final&nbsp;: il y fera appel à chaque fois qu’il en aura besoin.



<div style="attention">
Attention

Si l’image est coupée, vous devez soit ajuster la taille de l’image, soit changer l’espacement des lignes de paragraphe sur ``Au moins`` pour que la moitié supérieure des caractères d’une ligne ne soit pas coupée.
</div>

Après avoir choisi l’``Image``, la fenêtre liste un ensemble de champs pour éditer la puce&nbsp;:

- Le bouton ``Sélectionner`` ouvre un gestionnaire de fichiers pour sélectionner l’image.
- Les champs ``Largeur`` et ``Hauteur`` définissent la taille à laquelle l’image s’affiche. Ils n’affectent pas le fichier image original.
- Rappelez-vous qu’une hauteur trop grande nécessite de changer le réglage de l’espacement des paragraphes.
- La case ``Proportionnel``, lorsqu’elle est cochée, garantit le ratio entre la hauteur et la largeur lorsque vous changez l’une ou l’autre.
- L’alignement peut généralement être ignoré, mais il peut aider pour ajuster l’espacement entre la puce et le texte.

![Utiliser une image comme une puce.](Captures/CH10-puceimage.png)

## Les tableaux

Depuis la version 5.3, les styles de tableau ont fait leur apparition dans la fenêtre des ``Styles`` de Writer. 

Dans la fenêtre des ``Styles``, cliquez sur l’icône ``Styles de tableau`` et vous verrez affichés les styles de tableaux disponibles.

En réalité, ces styles de tableaux dans la fenêtre des ``Styles`` sont une extension des fonctionnalités d’``AutoFormat``. Comme vous allez le voir plus loin dans ce chapitre, les Auto-formats sont des caractéristiques de tableaux que l’utilisateur peut créer mais ne s’appliquent que sur un tableau à la fois.

Avec les styles de tableau vous ne pourrez ni modifier un style de tableau ni actualiser le style pour que tous les tableaux qui utilisent ce style soient automatiquement mis à jour. En cela, la fonction des styles de tableaux ne change rien par rapport aux ``Styles d’AutoFormat``.

En revanche, si vous créez un ``Style d’AutoFormat``,  il apparaîtra dans la fenêtre des styles de tableaux. Vous pourrez ainsi l’appliquer rapidement à vos tableaux. Plus loin dans ce chapitre, vous apprendrez à créer des ``Styles d’AutoFormat``.


## Préparez vos tableaux

Au fur et à mesure que vous concevez un modèle, demandez-vous à qui devront ressembler les tableaux que le document contiendra.

Les formes les plus courantes sont&nbsp;: 

- Plaine&nbsp;: on utilise des bordures, avec des lignes les plus fines possibles (pensez à la précision de votre imprimante).
- Pas de bordures ni d’arrière-plan&nbsp;: ce style est idéal pour les comparaisons sous forme de points. Vous devez néanmoins avoir des espaces blancs généreux pour remplacer les bordures.
- Ombré&nbsp;: on utilise un arrière-plan pour les lignes alternées à la place des bordures. Les différentes couleurs aident les lecteurs à suivre l’information horizontalement dans le tableau.



<div style="astuce">
Astuce

LibreOffice offre par défaut un choix de tableaux avec des arrière-plans différents pour les en-têtes  et les lignes ordinaires. Cependant, les conventions modernes minimisent l’utilisation des arrière-plans.
</div>


## Concevoir un tableau

L’objectif des tableaux est de présenter l’information de manière efficace. Les tableaux encombrés ou surdimensionnés vont à l’encontre de cet objectif. Si vous travaillez dans une langue de l’Europe de l’Ouest comme l’anglais ou le français, ces règles doivent garantir que vos tables remplissent correctement leur fonction&nbsp;:

- Les en-têtes doivent être horizontaux ou légèrement inclinés, mais jamais verticaux (sans quoi la lisibilité est compromise).
- Les polices doivent avoir la même taille que le corps de texte, bien qu’elles puissent être dans une police ou une graisse différentes. Les rendre plus petites permet d’économiser de l’espace au détriment de la lisibilité.
- Les bordures de cellules, l’ombrage pour les rangées alternées et les autres éléments de
mise en forme doivent être réduits au minimum.
- N’oubliez pas d’utiliser des espaces blancs à bon escient, en particulier lors du réglage de la distance du contenu par rapport aux bords d’une cellule.



<div style="astuce">
Astuce 

Pensez à laissez un espacement entre le tableau et le texte. Dans la fenêtre des ``Prorpiétés``, allez dans l’onglet ``Tableau`` et renseignez les valeurs d’espacement ``Àvant`` et ``Après``. 
</div>

Pour créer un nouveau tableau&nbsp;:

1. Sélectionnez ``Tableau > Insérer un tableau`` ou appuyez sur ``Ctrl + F12``. La fenêtre de dialogue  s’ouvre. 
2. Nommez le tableau pour que vous puissiez le trouver dans le ``Navigateur``, ou bien acceptez le nom par défaut. Le nom par défaut est ``Tableau``, suivi d’un numéro (suivant l’ordre dans lequel le tableau a été ajouté).
3. Indiquez le nombre de lignes et de colonnes que vous désirez (vous pourrez toujours en ajouter au besoin).
4. Sélectionnez l’une des options suivantes&nbsp;:
   - ``En-tête``&nbsp;: définit une ou plusieurs lignes comme une ligne d’en-tête de tableau en haut du tableau. Vous avez également la possibilité de répéter les lignes d’en-tête sur les nouvelles pages.
   - ``Ne pas scinder le tableau à travers les pages``&nbsp;: empêche l’impression des tableaux sur deux pages ou plus. Ce paramètre maintient les informations ensemble, mais peut également créer des problèmes avec les sauts de page.
   - Bordure&nbsp;: ajoute une bordure autour de toutes les cellules et des bords du tableau, constituée d’une ligne pleine de couleur noire et de 0,05 points d’épaisseur (il est possible de modifier ce paramètre par la suite en éditant les bordures).
5. Si vous le souhaitez, cliquez sur le bouton ``AutoFormat`` pour choisir un format déjà défini.

![Insérer un tableau.](Captures/CH10-crertableau.png)

### Éditer les parties d’un tableau

Toutes les composantes d’un tableau peuvent être modifiées via la fenêtre de dialogue de ses propriétés. Lorsque votre curseur est dans le tableau, faites un clic-droit et sélectionnez ``Propriétés du tableau`` ou bien allez dans le menu ``Tableau > Propriétés``.

Vous pouvez aussi utiliser le menu contextuel des tableaux pour éditer directement ses parties, notamment la possibilité de fusionner des cellules (ou les scinder), gérer les alignements, les couleurs, insérer des lignes et des colonnes, etc.

Pour supprimer un tableau entier, placez le curseur de la souris n’importe où dans le tableau et cliquez sur ``Tableau > Supprimer > Tableau`` dans le menu principal ou sélectionnez le tableau ainsi que l’espace avant et après le tableau et appuyez sur la touche ``Supprimer``. L’utilisation du menu est généralement plus facile.

### Les espacements

Par défaut, les tableaux occupent toute la largeur d’une ligne. Si vous avez besoin de connaître l’espace exact entre les marges, vous pouvez cliquer avec le bouton droit de la souris sur ``Propriétés du tableau`` et lire le champ ``Largeur`` dans l’onglet ``Tableau`` pour obtenir l’information. 

Pour ajuster la largeur totale d’une table&nbsp;: dans l’onglet ``Tableau``, cochez un autre alignement que ``Automatique`` et définissez la largeur.

![Définir la largeur d’un tableau.](Captures/CH10-largeurtableau.png)

Les options disponibles dépendent de votre choix d’alignement&nbsp;:

- ``Automatique``&nbsp;: le tableau remplit toute la largeur de la ligne.
- ``À Gauche``&nbsp;: le côté gauche du tableau s’aligne avec la marge gauche de la page. Vous pouvez utiliser ``Espacement > À droite`` pour indenter le côté droit de la table.
- ``De la gauche``&nbsp;: le tableau se décale de la valeur indiquées dans ``Espacement > À gauche``. Cette option peut déplacer le tableau dans la marge de droite, où une partie peut ne pas être imprimable.
- ``Droite``&nbsp;: le côté droit du tableau s’aligne avec la marge droite de la page. Vous pouvez utiliser ``Espacement > À gauche`` pour indenter le côté gauche de la table.
- ``Centrer``&nbsp;: le tableau se place au centre de la ligne. Si la largeur de la table est égale à la longueur totale de la ligne, ce réglage n’a pas d’effet visible.
- ``Manuel``&nbsp;: utilisez les champs ``Espacement`` pour ajuster la table à gauche et à droite.

Si le tableau est entouré de texte, placez le tableau plus près du paragraphe qui le précède que de celui qui le suit pour montrer leur relation. De même, si le style du paragraphe suivant est une légende, positionnez la légende plus près du tableau que le paragraphe suivant.

Pour ajuster l’espacement vertical avant et après le tableau, allez dans l’onglet ``Tableau > Espacement`` (au-dessus et en dessous).

Par défaut, le contenu est aligné sur le haut des cellules dans l’onglet ``Enchaînements``. Vous pouvez également l’aligner au centre ou au bas, bien qu’en pratique, il y ait rarement une raison de le faire.

#### Espacement des colonnes

Vous pouvez redistribuer l’espace horizontal entre les colonnes existantes en utilisant le curseur de la règle pour faire glisser les bords des cellules. Cette méthode peut sembler approximative, mais avec des règles verticales et horizontales dans la fenêtre d’édition, elle peut être aussi exacte que n’importe quel autre choix.

Vous pouvez également sélectionner ``Propriétés du tableau > Colonnes`` dans le menu contextuel et équilibrer les champs ``Largeur`` des colonnes. Vous pouvez aussi sélectionner ``Taille > largeur de colonne`` dans le menu contextuel (clic droit).

#### Espacement des lignes

Vous pouvez redistribuer l’espace vertical entre les lignes existantes en utilisant le curseur pour faire glisser les bords des cellules ou en sélectionnant ``Taille > Hauteur de ligne``  dans le menu contextuel (clic droit).

### Options d’enchaînements

Positionner un tableau sur papier peut être difficile, car vous obtenez souvent des sauts de page inégaux. Parfois, le réarrangement des lignes peut améliorer ces sauts, mais vous devez souvent intervenir manuellement avec l’un des outils disponibles&nbsp;:


- ``Répéter le titre``&nbsp;: Répéter la ou les lignes supérieures d’un tableau sur chaque page le cas échéant. Habituellement, vous n’avez besoin que de la première ligne lorsqu’elle sert d’en-tête de tableau.
- ``Autoriser le fractionnement des tableaux sur plusieurs pages et colonnes``&nbsp;: un tableau de plusieurs pages a tendance à aller à l’encontre de la facilité de lecture. Cependant, un tableau qui ne se divise pas donne souvent lieu à des sauts de page inégaux.
- ``Autoriser le fractionnement des lignes sur plusieurs pages et les colonnes``&nbsp;: cette option a le même objectif que celui qui permet de diviser le tableau entier, mais dans une moindre mesure. 
- ``Conserver avec le paragraphe suivant``&nbsp;: comme un tableau nécessite habituellement une introduction, ce réglage ne devrait être utile qu’occasionnellement (il peut s’avérer pertinent sur vous utilisez des légendes pour vos tableaux).

### Comportement en feuille de calcul

Les tableau dans Writer peuvent se comporter comme des feuilles de calcul (à l’image d’un tableur) mais seulement avec des fonctions limitées&nbsp;:

- ``Format numérique`` (menu contextuel)&nbsp;: ouvre une fenêtre de dialogue dans laquelle vous pouvez définir la façon dont les nombres sont interprétés et présentés. Par exemple, si vous réglez le format sur ``Date``, vous pouvez choisir parmi des formats tels AAAA-MM-JJ ou JJ-MM-AA.
- ``Reconnaissance des nombres`` (menu ``Tableau``)&nbsp;: identifie les entrées sous forme de chiffres et les aligne à droite.
- ``Formule``&nbsp;: sélectionnée à partir de ``Table > Formule`` dans le menu principal, cette option ouvre une barre d’outils pour insérer une douzaine de formules de feuilles de calcul courantes.


<div style="astuce">
Astuce

N’oubliez pas que Writer est un logiciel de traitement de texte. Si vous avez besoin d’un tableau pour un usage tableur, préférez l’utilisation de Calc puis insérez votre tableau comme un objet OLE.

</div>

## Ajouter des légendes

Si la phrase qui introduit un tableau est suffisamment claire, une légende peut être redondante. 
Cependant, si un tableau a besoin d’une légende, ou si vous voulez faire référence au tableau par un numéro, placez votre curseur dans le tableau et utilisez le menu ``Insertion > Légende``.

Si vous voulez toujours une légende à vos tableaux, configurez la légende automatique pour les tableaux dans ``Outils > Options > LibreOffice Writer > Légende automatique``.


## Styles et création d’AutoFormat


Les AutoFormats sont des styles qui ne sont pas tout-à-fait des styles de tableaux comme le sont les styles de paragraphes ou de caractères. Comme pour les autres styles, les AutoFormats peuvent être prédéfinis ou personnalisés. Contrairement aux autres styles, les AutoFormats ne peuvent pas être édités, sauf pour être renommés. Si vous voulez utiliser le même nom qu’un AutoFormat existant, vous devez d’abord supprimer l’AutoFormat existant.

![Exemple d’AutoFormats.](Captures/CH10-exemplesautoformat.png)

La création d’un AutoFormat est similaire à la création d’un nouveau style de paragraphe à partir d’une sélection.

Commencez par formater manuellement un tableau comme vous le souhaitez, puis, avec le curseur placé n’importe où dans le tableau, cliquez dans le menu ``Tableau > Styles d’AutoFormat``.

1. Dans la fenêtre de dialogue, en bas, cochez les caractéristiques de formatage que vous voulez enregistrer.
2. Cliquez sur le bouton Ajouter.
3. Nommez le nouvel AutoFormat.
4. Il sera alors disponible à la fois dans les choix d’AutoFormat et dans la fenêtre des ``Styles de tableau``.

## Une nouvelle forme d’écriture

L’utilisation de listes et de tableaux n’a que quelques centaines d’années. Pendant une grande partie de cette période, ils n’ont été utilisés que par une minorité de scientifiques et d’ingénieurs.

Aujourd’hui, les listes et les tableaux sont devenus monnaie courante dans les documents en ligne. Or, leurs usages ne sont jamais enseignés. Comment et quand utiliser des listes et des tableaux fait partie des connaissances de base pour la rédaction et l’organisation des idées. Apprenez leurs usages, et vous pouvez être sûr de présenter vos pensées sous le meilleur angle possible, à la fois lors de vos compositions avec LibreOffice mais aussi lorsque vous présentez votre travail en public.

