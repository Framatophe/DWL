# Alternatives libres pour les polices courantes

Tout comme les bureaux graphiques sous Linux, les polices sous licence libre ont commencé comme des imitations d’équivalents propriétaires. Aujourd’hui, les polices libres originales sont de plus en plus courantes, mais la demande d’équivalents gratuits de polices propriétaires demeure. Il est peu probable que cette demande disparaisse car, bien que la plupart des concepteurs professionnels pensent en termes propriétaires, les clients ne sont souvent pas disposés à les payer. De plus, les défenseurs du logiciel libre préfèrent que les polices libres accompagnent leurs applications libres.

Les équivalents exacts sont rares en raison de la crainte en restrictions du droit d’auteur. Un taux de concordance à 75 % est rare. Certains équivalents, comme les polices Liberation, ne sont que métriques, c’est-à-dire qu’ils occupent le même espace que leurs équivalents propriétaires, mais les lettres elles-mêmes sont différentes. Dans d’autres cas, les polices libres sont inspirées par leurs homologues propriétaires, mais le concepteur n’a jamais eu l’intention de faire des copies exactes, et tout ce à quoi vous pouvez vous attendre est une ressemblance générale. Quelques polices propriétaires, comme Optima, n’ont pas d’équivalent gratuit du tout, semble-t-il.

Pour ces raisons, le tableau ci-dessous propose seulement des équivalences les plus proches et rarement des répliques exactes.

Toutes ces polices peuvent être trouvées à partir des sources mentionnées à l’annexe C.

|  Proprietary                   |  Free Licensed             |
|--------------------------------|----------------------------|
| Alternate Gothic 1 | League Gothic |
| Arial | Liberation Sans^+^, Pt Sans, Open Sans Condensed, Lato |
| Arial Narrow | Liberation Sans Narrow^+^ |
| Avenir | Mint Spirit No2, Nunito |
| Baskerville | Baskervald ADF Standard, Libre Baskerville |
| Bembo | EB Garamond |
| Bodoni | Accanthis-Std, Oranienbaum, GFS Bodoni, Libre Bodoni |
| Cambria | Caladea^+^ |
| Calibri | Carlito^+^ |
| Caslon | Libre Caslon |
| Centaur | Coelacanth |
| Century Gothic | Muli |
| Comic Sans | Comic Relief |
| Courier | Liberation Mono^+^ |
| Courier 10 Pitch | Courier Code |
| Courier New | Cousine |
| Didot | GFS Didot |
| Eurostile | Jura |
| Frutiger | Istok Normal 400 |
| Futura | Mint Spirit No2, Nunito |
| Futura Light | Futura Renner Light |
| Garamond^++^ | Crimson Text, EB Garamond |
| Georgia | Nimbus Roman No. 9 |
| Gill Sans | Cabin, Gillius ADF, Hammersmith One, Railway Regular, Raleway  |
| Goudy Old Style^++^ | Goudy Bookletter 1911, Linden Hill, Sort Mills |
| Helvetica | Liberation Sans^+^, Pt Sans, Open Sans Condensed, Lato |
| Helvetica Narrow | Liberation Sans Narrow^+^ |
| Joanna | Fanwood |
| Letter Gothic | Josefin Sans, Josefin Slab |
| Myriad | Junction, Pt. Sans |
| News Gothic | News Cycle |
| Stone Sans | Nunito |
| Stone Serif | Lustria |
| Tahoma | Lucida Sans, Nimbus Sans |
| Times New Roman | Liberation Serif, Linux Libertine |
| Trajan | Cinzel |
| Univers | Universalist-std |
| Verdana | DejaVu Sans |


^+^&nbsp;: Équivalences métriques. 

^++^&nbsp;: Garamond et Goudy sont des noms génériques pour des polices inspirées par des concepteurs renommés, de sorte que les polices de caractères avec ces noms peuvent être très différentes l’une de l’autre.
