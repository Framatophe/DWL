# Questions d’espacements

Les styles de paragraphes sont le type de style le plus fréquemment utilisé dans LibreOffice. Cependant, ces styles interagissent si étroitement avec ceux des caractères qu’il est impossible d’en parler séparément. Ils ont même en commun certains onglets dans leurs fenêtres de dialogue.

Les styles de paragraphes définissent la mise en forme générale de tout document chargé en texte, tandis que les styles de caractères permettent des petites variations. Pour les courts passages de texte (par exemple un titre, une URL ou un point d’interrogation), les styles de caractères fournissent des exceptions qui donnent aux styles de paragraphe la flexibilité dont ils peuvent manquer.

Le chapitre précédent s’est aventuré dans certains des principes de base du paramétrage des paragraphes et des caractères tout en discutant des polices et de la manière de trouver l’interligne idéal. Le présent chapitre complète la discussion sur la mise en forme de base des paragraphes (y compris un approfondissement de la question des interlignes), et couvre les options relatives aux espacements verticaux et horizontaux.

## Préparez-vous

- Choisissez vos polices de caractères et l’interligne. 
- Préparez une calculatrice et une liste de multiples de l’interligne pour pouvoir vérifier les mesures.
- Définissez en points la mesure par défaut dans ``Outils > Options > LibreOffice Writer > Général > Paramètres > Unité de mesure``. Vous pourrez réinitialiser l’unité par défaut à centimètres lorsque vous aurez terminé, mais les points sont l’unité de mesure la plus couramment utilisée pour la typographie.

![Paramétrez l’unité de mesure en points.](Captures/CH5-generalparametreunite.png)

- Réglez le zoom à 100% pour que vous puissiez juger de la couleur de la page. Imprimez ou zoomez périodiquement pour obtenir des perspectives différentes.
- Dans la barre d’état en bas de la fenêtre d’édition, réglez LibreOffice pour afficher plusieurs pages, en sélectionnant le bouton ``Affichage multi-page``. À moins d’utiliser un trop petit écran et/ou une faible résolution, un espacement de deux pages vous aide à voir l’effet de vos choix de conception.

![Affichage multi-page.](Captures/CH5-affichmultipage.png)

- Ouvrez un second fichier dans lequel vous pourrez expérimenter et utiliser du texte factice pour tester les formats.
- Dans un premier temps, utilisez le formatage manuel. La création de styles à ce stade n’est qu’une étape supplémentaire à condition d’avoir terminé l’expérimentation. Lorsque vous aurez décidé des choix de mise en forme, vous pourrez toujours faire glisser un passage sélectionné dans la fenêtre des ``Styles`` pour créer un style à partir de celui-ci.
- Utilisez les titres de ce chapitre comme *checklist* pour vérifier tous les formats de base.

<div style="attention">
Attention

Au fur et à mesure que vous travaillerez, vous constaterez peut-être que certains paramètres que vous avez déjà choisis doivent être modifiés, même le nombre magique. Plutôt que de résister au changement, acceptez qu’il fasse partie intégrante du processus de conception. Le but est de développer la meilleure mise en forme possible, pas de s’accrocher à une cohérence inutile.
</div>

## Planifiez

Vous pouvez gagner du temps en travaillant autant que possible avec les styles prédéfinis de Writer. Plus tard, au fur et à mesure que vous écrirez, vous découvrirez que certaines fonctionnalités, comme le champ ``Insérer > Champs > Autres champs > Chapitre > Nom de chapitre``, dépendent des styles de paragraphe prédéfinis dans le document.

Les styles de caractères et de paragraphes prédéfinis devraient couvrir la plupart de vos besoins, mais vous pouvez ajouter d’autres styles au besoin…

Passez ensuite en revue la liste des styles prédéfinis et choisissez ceux dont vous n’avez pas besoin. Par exemple, les styles de titre des  niveaux 5 à 10 sont généralement en surplus. Pour chaque style inutile, sélectionnez ``Masquer`` dans le menu contextuel (clic droit). Si vous décidez que finalement vous avez besoin d’un style caché, vous pouvez aller au filtre ``Styles masqués`` et sélectionner ``Afficher``.
 
Toutes ces tâches demandent beaucoup de travail. Mais vous constaterez également qu’il existe de nombreux styles pour lesquels vous pouvez conserver les paramètres par défaut.

Par exemple, l’interligne peut ne pas avoir d’importance dans les titres, puisque ceux-ci sont souvent d’une longueur inférieure à une ligne.
 
De même, dans un document simple, une indentation automatique de première ligne peut supprimer complètement la nécessité de définir des tabulations. Le fait que de nombreux paramètres par défaut puissent être utilisés, inchangés ou totalement ignorés réduit considérablement la quantité de travail nécessaire pour élaborer un modèle.

## Espacement vertical

Le champ ``Interligne`` se trouve dans l’onglet ``Retraits et espacement`` des styles de paragraphe. Il est possible de régler des valeurs au  1/10^e^ de point, ce qui peut faire une grande différence visuelle.

Si vous avez utilisé votre choix de polices pour déterminer le nombre magique, vous avez déjà modifié cette option à l’aide du paramètre ``Fixe`` dans le champ ``Interligne``. ``Fixe`` reste de loin le meilleur réglage, car c’est la seule option qui donne une mesure exacte. 

![Réglage de l’interligne.](Captures/CH5-interlignefixe.png)

``Fixe`` présente également l’avantage de conformer les documents, que le paramètre ``Contrôle de repérage`` soit activé ou non. En d’autres termes, les lignes seront dans la même position horizontale sur toutes les pages, surtout si le document n’utilise qu’une seule police. 

De plus, si vous imprimez des deux côtés d’une page, les lignes pourront mieux se chevaucher, empêchant ainsi l’ombre de l’autre page d’apparaître à travers un papier mince.

Cependant, un problème avec certains styles utilisant des caractères de plus grande taille, comme les titres, est qu’ils peuvent avoir le haut ou le bas des caractères découpés lorsque ``Fixe`` est utilisé. Ce problème peut être résolu en définissant le champ ``Interligne`` à l’aide de l’option ``Au moins`` tout en maintenant la hauteur d’interligne inchangée. 

![Problème d’interlignage.](Captures/CH5-problemeinterlignefixe.png)

Les autres options disponibles sont en réalité différentes façons d’exprimer l’interligne. Sauf pour des documents rapides et uniques, évitez les options ``Simple``, ``1,5 ligne`` et ``Double``. Ces options utilisent les valeurs par défaut standard de Writer pour l’espacement des lignes, et sont optimales pour une police donnée seulement.
 
L’option la moins utile est ``Proportionnel``, dans laquelle l’interligne est exprimé en pourcentage d’un interligne unique déterminé automatiquement, et non en chiffres exacts. En d’autres termes, cette option vous place vous et votre machine complètement en dehors de la réalité.

Une autre option est ``Typographique``. Elle fait référence à l’époque de la mise en page manuelle, où l’on utilisait des restes de plomb ou n’importe quoi d’autre pour augmenter l’espacement des lignes. Cependant, LibreOffice utilise ce mot pour ne faire référence qu’à l’espace supplémentaire au-delà de la taille de la police. Par exemple, dans LibreOffice, un paragraphe mis à 12/15 serait placé avec ``Typographique`` à 3 points.

### Polices de petites tailles et interlignes

Vous pouvez parfois trouver des polices de caractères spécialement conçues pour être lisibles à des tailles inférieures à 10 points. Le plus souvent, les caractères de petite taille ont besoin d’un interligne assez grand pour être lisibles.

Malheureusement, LibreOffice, comme la plupart des logiciels de traitement de texte, traite l’interligne pour les polices de petite taille de la même façon que pour toute autre taille. En règle générale, sélectionnez ``Fixe`` pour donner aux polices de petite taille l’espacement supplémentaire dont elles ont besoin.

![En haut, Liberation Serif taille 8 et interligne simple, en bas, Liberation Serif en 8/12.](Captures/CH5-petitepoliceaveciterlig.png)


### Espacement entre paragraphes

L’espacement vertical est défini dans l’onglet ``Retraits et espacement`` puis dans ``Espacement > Au-dessus du paragraphe`` et ``Espacement > Sous le paragraphe``.

![Configurer l’espacement.](Captures/CH5-ongletespacements.png) 

L’espacement vertical est également utilisé pour accroître l’efficacité des titres. La règle est simple&nbsp;: un titre doit se situer plus près du contenu qu’il introduit, de manière à ce que cette relation soit évidente.

![Régler correctement l’espacement avant et après un titre.](Captures/CH5-espacementtitre.png)

L’espacement entre les paragraphes est une façon d’indiquer le début d’un nouveau paragraphe. L’autre consiste à placer une indentation pour la première ligne. Habituellement, l’espacement supplémentaire est utilisé dans les documents techniques, mais la seule règle est de ne pas utiliser les deux en même temps.

<div style="astuce">
La taille de la police du titre, et l’espace au-dessus et en dessous, doivent totaliser un multiple de l’interligne. De cette façon, les styles de titres correspondent à l’interligne selon une certaine fréquence (un titre «&nbsp;prend&nbsp;» *n* interlignes). Utilisez la même formule si l’espacement entre les paragraphes est utilisé au lieu d’une indentation de première ligne.
</div>

![37,5 pt = 15x 2,5.](Captures/CH5-espacementtitre2.png)


### Espacements indésirables


La combinaison des réglages ``Avant`` et ``Après`` pour deux styles de paragraphes qui se suivent peut provoquer des espacements importants et non souhaités.

Vous pouvez éviter ce problème en n’utilisant que le champ ``Avant le paragraphe`` et en laissant le champ ``Après le paragraphe à zéro`` pour la plupart des styles.

Parfois, il peut arriver que des espacements s’additionnent entre une image et un style de paragraphe. Dans ce cas, modifiez l’espace sous l’image pour qu’il corresponde à la convention que vous avez définie et laissez les paramètres de style de paragraphe seuls. Après tout, l’espacement autour d’une image ou de tout autre objet est généralement défini manuellement.

## Veuves et orphelines

Les bonnes pratiques de typographie évitent les veuves et les orphelines, c’est-à-dire respectivement la dernière ligne d’un paragraphe apparaissant seule en haut d’une page et la première ligne d’un paragraphe apparaissant seule en bas d’une page.

Bien sûr, ce n’est pas toujours évident et il peut arriver qu’un paragraphe ne comporte qu’une seule ligne. ``Outils > Autocorrection`` propose l’option de combiner des paragraphes courts, mais cela ne convient pas toujours au sens du contenu.

D’autres paragraphes ont un nombre de lignes qui ne correspond pas aux paramètres de Writer. Par exemple, si vous avez défini les derniers et les premiers paragraphes d’une page pour que chacun ait deux lignes, quelque chose doit être fait quand un paragraphe de trois lignes chevauche deux pages.

![Onglet des enchaînements.](Captures/CH5-enchainements.png)

L’onglet ``Enchaînements`` permet de contrôler les veuves et les orphelines. Pour le corps de texte et les styles apparentés, vous devez activer les deux options ``Traitement des orphelines`` et ``Traitement des veuves``. La valeur par défaut proposer de garder deux lignes ensemble, c’est une valeur minimum que vous pouvez augmenter. 

Vous n’avez pas besoin de ces paramètres pour les titres, ou dans le cas où tous les paragraphes sont courts (dans l’un ou l’autre cas, les commandes n’auront rien à ajuster).

Comme alternative, désactivez les contrôles des veuves et orphelines, sélectionnez alors ``Ne pas scinder le paragraphe``. Ce réglage permet de garder les informations importantes ensemble et donc les rendre plus faciles à lire. Le prix à payer est que cela peut obliger LibreOffice à créer des sauts de page bien avant le bas de la page.

Pour les titres, vous pouvez préférer ``Conserver avec le paragraphe suivant``. Lorsque les titres sont destinés à introduire le corps du texte en dessous, il n’est pas logique d’avoir le titre et le corps du texte sur des pages séparées. Toutefois, ce réglage peut également entraîner des sauts de page mal positionnés, utilisez-le avec parcimonie.

## Choisir l’alignement

Dans l’onglet ``Alignement``, quatre options vous permettent de régler l’alignement horizontal du texte&nbsp;: ``À gauche``, ``À droite``, ``Centrer``, ``Justifier``.

![Onglet Alignement.](Captures/CH5-ongletalignement.png)


L’alignement ``À droite`` est rarement utilisé sauf dans les documents courts avec des mises en page particulières telles les publicités ou les diagrammes. Le centrage (``Centrer``) est généralement réservé aux titres et sous-titres. Pour la plupart des styles de paragraphes, votre choix sera probablement ``À gauche`` ou ``Justifié``.

Quel que soit votre choix d’alignement, si vous utilisez des styles autorisant les césures, exécutez ``Outils > Langue > Coupure des mots`` en dernière étape avant la publication. Cet outil répare les choix non optimaux que le rédacteur aurait pu faire.

### Justifier le texte

Beaucoup d’utilisateurs préfèrent un alignement justifié, dans lequel toutes les lignes commencent à la même position sur la gauche et se terminent à la même position sur la droite. En effet, les publications commerciales utilisent souvent cet alignement (dit «&nbsp;au carré&nbsp;»), si bien que les utilisateurs croient qu’il semble plus professionnel. En langue française, cependant, l’usage de l’alignement justifié est unanimement répandu.

Cette préférence peut aussi être une réminiscence des premiers logiciels de traitement de texte, la première fois qu’un alignement justifié est devenu pratique. Les anciennes machines à écrire, bien sûr, ne pouvaient utiliser qu’un alignement à gauche.

Le problème, c’est qu’un alignement justifié demande souvent plus de travail. Trop souvent, il en résulte un espacement irrégulier entre les caractères ou les mots, ce qui est souvent bien pire que l’alignement à gauche. Vous avez presque toujours besoin de retoucher pour trouver la meilleure distribution de caractères et de mots sur une ligne.

De même, plus la ligne est courte, plus il est difficile de travailler pour justifier le texte. En règle générale, les lignes de moins de 40 caractères représentent trop d’efforts pour être justifiées. Un alignement à gauche peut toujours poser des problèmes, mais ils sont souvent moins sévères, surtout dans les colonnes ou les tableaux.

La façon la plus simple de savoir si un style de paragraphe peut facilement utiliser ``Justifier`` est de le configurer avec du texte factice puis de compter le nombre de lignes qui se terminent par un trait d’union et les taches d’espaces blancs irréguliers. Plus ces problèmes sont nombreux, plus vous avez besoin de modifier les césures, la police, la taille et/ou la largeur de la colonne dans l’espoir d’obtenir un meilleur ajustement. Vous pouvez même aller à l’onglet ``Position`` pour développer ou condenser l’espacement des caractères.

![Exemple de mauvais alignement pour la longueur de ligne. Les césures provoquent des ruptures gênantes et la colonne du milieu en comporte même trois d’affilée. Pire encore, plusieurs lignes sont des mots seuls.](Captures/CH5-mauvaisalignement.png)

### La dernière ligne d’un texte justifié

Il est rare que la dernière ligne d’un texte justifié soit une ligne complète (on parle de «&nbsp;ligne creuse&nbsp;»). Un traitement inadapté peut résulter en un étirement disgracieux des mots ou des caractères. LibreOffice offre la possibilité de gérer ou non ce type d’erreur.

![Gestion de la dernière ligne d’un paragraphe justifié.](Captures/CH5-derniereligne.png)

<div style="astuce">
Astuce

``Capturer à la grille du texte (si active)`` utilise la grille de positionnement que l’on peut activer via ``Outils > Options > LibreOffice Writer > Grille``.
</div>

Le seul choix esthétique est de régler le champ ``Dernière ligne`` sur ``Début``. Les autres possibilités doivent être mûrement réfléchies et réservées à des contextes particuliers.


![Réglages de la dernière ligne. De haut en bas&nbsp;: justifié, centré, étirer un mot, début.](Captures/CH5-derniereligneexemples.png)

### Alignement à gauche

Lorsqu’un paragraphe est aligné à gauche (ferré à gauche), toutes les lignes commencent au même endroit sur la gauche, mais peuvent se terminer n’importe où sur la droite. C’est pour cette raison qu’un alignement à gauche est parfois appelé «&nbsp;en drapeau droit&nbsp;» (en anglais&nbsp;: *ragged right*, déchiqueté à droite). C’est le choix par défaut dans Writer.

En général, plus la ligne est courte, plus vous devez faire des modifications pour que l’effet ``Justifié`` soit correct. En fait, l’alignement gauche est souvent le meilleur choix, surtout dans les colonnes ou les tableaux.

## Les césures

Les options de césure sont définies dans l’onglet ``Enchaînement > Coupure des mots``. L’utilisation des césures est l’une des décisions les plus importantes que vous prendrez lors de la conception d’un document.

La césure est une question litigieuse. La plupart des césures de traitement de texte se font au fur et à mesure que vous tapez, et même si le logiciel fait des ajustements au fur et à mesure que la longueur des lignes change, ces césures à la volée ne sont pas toujours inopportunes. Par contre, les césures dans les lignes courtes sont particulièrement difficiles à gérer automatiquement.

C’est l’une des raisons pour lesquelles beaucoup préfèrent un alignement à gauche ou une marge droite décalée. Un alignement à gauche ne produit pas toujours une utilisation optimale de la ligne, mais ses faiblesses sont rarement aussi graves que celles d’un alignement justifié.

Un autre choix consiste à désactiver complètement la césure, ce qui explique probablement pourquoi l’onglet ``Enchaînement`` ne propose pas l’activation automatique des césures par défaut. 

Pour les plus déterminés ou les plus patients, on peut améliorer la césure en ajustant les paramètres de la césure automatique. Le nombre de lettres à la fin et au début de la ligne doit être compris entre 1 et 4. La convention typographique est de ne pas permettre que plus de deux lignes consécutives se terminent par une césure.

![Contrôle des césures.](Captures/CH5-Cesureautomatique.png)

Les champs ``Caractères en fin de ligne`` et ``Caractères en début de ligne`` peuvent être modifiés pour améliorer les césures en les faisant jouer l’un contre l’autre. De son côté, le champ ``Nombre maximum de coupures consécutives`` peut également faire une différence. 

Dans de nombreux documents, seul le corps de texte et quelques autres styles de paragraphes sont assez longs pour être concernés par les césures. Les titres, qui s’étendent rarement sur  plus de deux lignes, n’en ont généralement pas besoin.

Vous pouvez aussi modifier les césures en ajustant le poids ou la taille de la police ainsi que le choix des polices de caractères. On peut encore modifier les paramètres de césure (coupure des mots) via ``Outils > Options > Paramètres linguistiques > Linguistiques > Options``.

Quant aux champs ``Échelle`` et ``Espacement`` de l’onglet ``Position``, ils peuvent servir aussi pour ajuster les césures. Cependant, les utiliser pour cela est une dernière mesure désespérée.

De plus, les coupures peuvent toujours être améliorées en exécutant ``Outils > Langue > Coupure des mots`` comme opération finale sur le document. Si ce dernier est terminé, cet outil fait parfois un meilleur travail que si vous aviez opéré des césures manuellement.

<div style="astuce">

Astuce

Pour agir encore plus finement, parcourez un document une fois qu’il est terminé, et effectuez si besoin des césures manuellement en positionnant le curseur entre les syllabes et en appuyant sur ``Ctrl + -``. Cette combinaison de touches crée un trait d’union conditionnel qui ne s’active que lorsqu’il se trouve dans la zone de césure près de la marge droite.
</div>

## Espacements horizontaux

Par défaut, les paragraphes vont de la marge de gauche à la marge de droite —&nbsp;ou, au moins, à une région juste avant la marge de droite que LibreOffice doit atteindre avant de commencer une nouvelle ligne, avec ou sans césure.

Dans l’onglet ``Retraits et espacement``, vous pouvez indenter un paragraphe à gauche en saisissant une valeur dans le champ ``Avant le texte``, ou à droite en saisissant une valeur dans le champ ``Après le texte``.
 
Les utilisations courantes pour une indentation sont les suivantes&nbsp;:

- le début d’un nouveau paragraphe,
- une citation de plus de trois lignes ou 100 mots (typiquement, ces longues citations sont en retrait à gauche et à droite et aucun guillemets n’est utilisé, puisque l’indentation est suffisante pour marquer la citation), 
- l’espacement entre une puce ou un nombre et un élément d’une liste (cet espacement est configuré dans l’onglet ``Position`` pour un style de liste),
- notes, conseils ou styles de paragraphes d’avertissement,
- paragraphes numérotés
- cas où les en-têtes et les pieds de page sont plus larges que le corps du texte.

### Contrôler le nombre d’indentations

Les styles de paragraphes qui comportent une indentation sont inévitables dans un document. Cependant, trop d’indentations différentes encombrent la composition, de sorte qu’elles devraient être réduites au minimum. Par exemple, l’indentation pour une longue citation peut être la même que l’indentation de la première ligne de paragraphe du corps de texte, ainsi que pour la position où le texte commence dans un élément de liste.

Quelle que soit la taille de la police ou de la page, les conventions typographiques suggèrent qu’une ligne de corps de texte devrait être longue de 50 à 75 caractères pour optimiser la lisibilité —&nbsp;ou, pour dire les choses autrement, deux à trois fois l’alphabet en caractères minuscules dans une mise en page à une seule colonne.  Dans les tableaux ou les mises en page multi-colonnes, la longueur doit généralement être de 30 à 50 caractères, quel que soit l’alignement.

![Pourquoi les indentations devraient être aussi réduites que possible. En haut, le texte comporte cinq indentions et semble encombré. En bas, elles sont réduites à trois.](Captures/CH5-indentationtexte.png)

### Exemple&nbsp;: un modèle de lettre


Vous pouvez personnaliser un modèle de lettre à partir de ``Fichier > Assistants > Lettre``. Ce modèle utilise des cadres pour positionner les différents éléments de la lettre. Voici une alternative&nbsp;: créer un modèle de lettre avec des styles.

L’exemple ignore les paramètres de page, car ils n’ont pas encore été abordés à ce point de l’ouvrage. Vous pourrez ajouter des marges et des en-têtes après avoir lu la section relative aux styles de page.

#### Choix des polices

Ce modèle utilise deux polices de caractères&nbsp;: l’une pour le corps de texte, et l’autre pour les informations telles que les adresses et la formule de politesse.

Après quelques expérimentations, on opte pour deux polices libres de la fonderie numérique Arkandis Digital Foundry. Baskervald ADF Std. imite la police classique du XVIII^e^ siècle Baskerville et elle est utilisée pour le corps de texte. Gillius ADF No. 2, qui imite Gill Sans, est utilisée pour les titres (c’est-à-dire tout ce qui ne fait pas partie du corps du texte). 

Si vous téléchargez et installez ces deux polices sur votre système, vous pourrez construire ce modèle.

#### Création de la palette de polices

Pour préparer Gillius ADF No. 2, appliquez ces réglages au style de titre&nbsp;:

- Puisque cette police n’est utilisée que pour des lignes courtes, ignorez les éléments comme l’indentation de première ligne ou le contrôle des veuves et orphelines, qui ne seront pas utilisés. 
- Définissez la taille de la police à 14 points pour une meilleure lisibilité depuis l’onglet ``Police``, et réglez l’interligne à 18 points. L’expérience montre qu’avec une valeur moindre, la couleur de la police est trop sombre sur la page.

Pour préparer Baskervald ADF Std, appliquez ces paramètres au style du corps de texte&nbsp;:

- Dans l’onglet ``Enchaînements``, cochez la case ``Automatiquement`` pour les options de césure, et cochez les cases ``Traitement des orphelines`` et ``Traitement de veuves``.
- Réglez la taille de police à 15 depuis l’onglet ``Police``. Les caractères de Baskervald utilisent plus d’espace blanc que la plupart des autres polices, et paraissent beaucoup plus petits que la taille réelle.
- Réglez l’interligne sur ``Fixe > 18 points`` depuis l’onglet ``Retraits et espacements``. Ce réglage fourni à Baskervald une couleur proche de celle de Gillius, ce qui donne un aspect uniforme sur la page.
- Vérifiez ces réglages en imprimant des échantillons d’au moins trois lignes pour les deux polices de caractères. Tous les autres styles seront basés sur les réglages de ces deux modèles, avec des variations selon les besoins ponctuels. 

#### Configuration des autres styles

La meilleure façon de configurer les autres styles est de commencer par le haut du document et de noter les styles nécessaires&nbsp;:

Utilisez le style ``Destinataire`` (présent par défaut) pour l’adresse d’envoi. Modifiez le champ ``Hériter de`` dans l’onglet ``Gestionnaire`` et choisissez ``Titre`` puis changez l’alignement sur ``À droite`` depuis l’onglet ``Alignement``.

En dessous de l’adresse d’envoi se trouve la date, suivie d’un espace blanc, puis de l’adresse. Créez les deux en tant que nouveaux styles (``Date`` et ``Adresse``) liés au style de titre. 

Apportez ensuite les modifications suivantes au style de date&nbsp;:

1. Positionnez sur ``Adresse`` le champ ``Style de suite`` dans le ``Gestionnaire``.
2. Définissez l’alignement à droite depuis l’onglet ``Alignement``.
3. Dans l’onglet ``Retraits et espacement``, réglez l’espacement ``Au-dessus du paragraphe`` sur 54 points, et ``Sous le paragraphe`` sur 126 points. Notez que ce sont des multiples de l’interligne fixe de 18 points utilisés pour le ``Titre`` et son style subordonné.
4. Dans le menu, sélectionnez ``Insertion > Champs > Autre champs > Document > Date``. Sélectionnez un format dans le volet ``Format``, puis cliquez sur le bouton ``Insérer``. Maintenant, à chaque fois que vous sélectionnerez le style ``Date``, la date actuelle sera automatiquement ajoutée.
5. Le style ``Adresse`` n’est pas modifié par rapport au style de titre. Cependant, cela vaut la peine de le créer pour que vous vous souveniez du style à utiliser. En outre, vous pourriez éventuellement décider de le modifier.
6. Le style suivant est la formule de politesse. Dans l’onglet ``Retraits et espacement``, réglez ``Au-dessus du paragraphe`` à 36 points (2 lignes) et ``Sous le paragraphe`` à 18 points (1 ligne). Ensuite, dans l’onglet ``Gestionnaire``, définissez le champ ``Style de suite`` sur ``Corps de texte``.
7. Le style de corps de texte est déjà créé. Cependant, il a besoin d’un réglage d’indentation de première ligne depuis l’onglet ``Retraits et espacement``. Réglez-le à 18 points, comme l’espacement des lignes. 
8. Créez un style ``Signature`` avec les paramètres suivants&nbsp;: depuis l’onglet ``Alignement``, réglez l’alignement sur ``Centrer``&nbsp;; dans l’onglet ``Retraits et espacement``, réglez ``Au-dessus du paragraphe`` sur 18 points (1 ligne) et ``Sous le paragraphe`` à 90 points (5 lignes). Laissez plus d’espace si vous avez une grande signature.

#### Les styles de caractères

Les seuls styles de caractères nécessaires avec ce gabarit sont ceux de l’``Accentuation`` (italique) et de l’``Accentuation forte`` (gras). Basez les deux sur le corps du texte (Baskervald ADF Std, 15 points).

#### Autres points

En suivant ces étapes, vous obtiendrez un modèle utile et bien conçu. La mise en forme consiste en six changements de styles de paragraphes, dont deux sont automatiquement modifiés lorsque vous appuyez sur la touche ``Entrée``. Au lieu de vous inquiéter de la mise en forme, vous pourrez vous concentrer sur ce que vous écrivez.

Cependant, vous pourriez organiser les styles de paragraphes différemment, et faire d’autres ajustements au-delà des éléments basiques donnés ici.

Construire un modèle est une question d’essais et d’erreurs, et il est peu probable que vous vous souveniez de tout —&nbsp;ou que vous obteniez tous les styles parfaits&nbsp;— après une seule séance.

Par exemple, après avoir utilisé ce modèle pour quelques lettres, vous pourrez réaliser que les marges par défaut créent un aspect un peu étroit. La modification de la marge gauche et droite à 72 points (un multiple de l’interligne du corps du texte) peut grandement améliorer la mise en page.

De même, vous pouvez vous apercevoir que le modèle fonctionne mieux pour les lettres courtes&nbsp;: ajoutez un style de page avec un pied de page contenant le numéro de page pour les lettres longues.


## Les fondamentaux

Les paramètres abordés dans ce chapitre sont ceux que vous êtes susceptible d’utiliser dans chaque document. Les deux chapitres suivants explorent les cas particuliers et les paramètres avancés que vous pouvez utiliser de temps à autre.
