# Introduction

L’origine des suites bureautiques remonte à celle de l’ordinateur personnel. Pourtant, plus de trente ans après, peu d’entre nous ont appris à les utiliser.

Certes, nous avons appris à faire des choses avec. La majorité d’entre nous sait mettre en page un document et l’imprimer, selon les styles à la mode. Mais ce que beaucoup n’ont pas appris, c’est réaliser ces tâches efficacement, en tirant parti de tous les outils disponibles. 

C’est comme si nous en savions assez sur les voitures pour se laisser glisser en roue libre dans une pente et aller le plus loin possible une fois sur le plat, mais sans jamais avoir entendu parler de l’allumage. Nous faisons ce qu’il faut, mais avec plus d’efforts et moins d’efficacité. Certaines tâches, comme le démarrage en côte, nous les pensons même impossibles à cause de notre ignorance.

Utiliser tout le potentiel d’une suite bureautique signifie savoir comment concevoir vos documents – et neuf dixièmes de la conception consistent à savoir utiliser les styles et les modèles. Apprendre comment utiliser les styles et les modèles, c’est comme si on vous donnait la clé de la voiture  tout en vous montrant la pédale d’accélérateur – soudain, vous pouvez prendre le contrôle du véhicule, au lieu de vous dépatouiller avec des bricolages maladroits.

C’est particulièrement vrai pour LibreOffice. Son traitement de texte Writer en particulier est structuré autour du concept de styles. Writer a non seulement  plus de styles que les autres logiciels de traitement de texte, mais il propose également de nombreuses fonctionnalités avancées, telles les tables des matières et les documents maîtres, qui nécessitent plus d’efforts pour être utilisés sans styles. Et après avoir passé du temps à mettre en place des styles, vous ne voudrez pas refaire le même travail pour les documents suivants, de sorte que les modèles deviendront également une partie importante de votre de travail.

D’autres applications de LibreOffice et OpenOffice sont moins dépendantes des styles que Writer, mais elles y sont toujours plus attachées que leurs équivalents dans Microsoft Office.

Pour une grande part, ce livre est conçu de manière à expliquer non seulement quels styles et modèles on trouve et comment les utiliser, mais aussi ce que vous devez prendre en compte lors de leur sélection. Pour le reste, il s’agit surtout des fonctionnalités qui ne sont pas tout à fait des styles, ou qui dépendent des styles pour une pleine efficacité. D’autres sections ne traitent pas des styles, mais sont nécessaires pour un exposé complet sur la conception de documents.

Cependant, sachez que ce livre n’est pas un parcours du combattant pour naviguer avec une rigueur toute militaire à travers les menus. Il examine plutôt les fonctionnalités importantes du point de vue de la conception, comme un ensemble de tactiques que vous pouvez choisir, et présente ce qui semble le plus utile. Il part du principe que les lecteurs sont déjà familiers avec LibreOffice et d’autres suites bureautiques, ou qu’ils sont au moins en mesure naviguer dans les menus par eux-mêmes.

Ce livre peut être vu comme une tentative de créer une seule carte à partir de deux sources d’informations : les fonctionnalités de LibreOffice, et les pratiques typographiques courantes (la composition de texte). 

En comprenant les deux, vous pourrez tirer parti de ce que les éditeurs ont appris depuis plus d’un demi-siècle sur ce qui fonctionne et ce qui ne fonctionne pas. L’alternative est de perdre du temps en faisant des essais et des erreurs, pour finir avec un résultat (et parfois aucun résultat) pire que si vous aviez suivi un conseil.

## Qu’est-ce que la typographie ?

Les gens ont des idées étranges sur la typographie. Beaucoup imaginent qu’il s’agit de composer de manière à attirer l’attention, comme mettre du texte clignotant sur une page Web. Beaucoup supposent aussi que le but principal est de montrer à quel point le rédacteur est intelligent.

En réalité, rien ne pourrait être plus faux. Le véritable but de la typographie est de rendre le texte plus attrayant et plus facile à lire. La typographie n’est pas une conception abstraite, c’est une préoccupation pratique comme savoir faire des marges suffisamment larges pour que les lecteurs puissent tenir confortablement un livre ou faire des améliorations pour faciliter la mise à jour d’un document. 

Loin d’attirer l’attention sur elle, la meilleure typographie se cache, et les lecteurs ordinaires la remarquent seulement de manière vague parce qu’un document est agréable à lire. Les particularités qui produisent ces caractéristiques ne devraient être perceptibles que par les yeux exercés à les observer.

LibreOffice ne dispose pas de tous les outils nécessaires pour  accéder au plus haut niveau de l’art typographique. Cependant, il en a beaucoup, et bien plus que dans toute autre suite bureautique. Plus vous en savez sur la typographie, plus vous pouvez lui faire faire ce que vous voulez.

En fin de compte, cet ouvrage n’est pas un livre théorique, il traite des tâches quotidiennes. Ceux qui veulent en savoir plus sur la typographie et la mise en page peuvent consulter l’annexe C pour obtenir de plus amples renseignements.

## Qu’est-ce que LibreOffice ? OpenOffice ?

LibreOffice est une suite bureautique qui tourne sur les version 32 et 64 bits de GNU/Linux, OS X et Windows. Ce logiciel est publié sous une licence libre qui vous permet de l’utiliser légalement sur autant de machines que vous le souhaitez et de le partager avec d’autres. Comme il s’agit d’un logiciel libre, vous pouvez même y apporter vos propres modifications si vous en avez les compétences.

LibreOffice est issue d’OpenOffice.org, un projet de logiciel libre géré depuis des années par Sun Microsystems. Lorsque Oracle a acheté Sun en 2010, LibreOffice a commencé à développer sa propre version d’OpenOffice.org, comme le permet la licence du code. Finalement, Oracle a cédé ses droits à la Fondation Apache.
 
Aujourd’hui, OpenOffice.org n’est plus, mais Apache OpenOffice existe. Malgré des numéros de versions différents, LibreOffice et Apache OpenOffice sont extrêmement similaires dans la plupart des fonctionnalités. À moins que ce soit signalé dans l’ouvrage, ce qui est affirmé à propos de LibreOffice devrait aussi être vrai pour OpenOffice.

Plusieurs autres organisations reconditionnent LibreOffice et Apache OpenOffice, parfois sous des noms différents. Cette pratique est parfaitement légale, mais implique parfois l’utilisation d’icônes différentes et d’autres changements mineurs. 

Les distributions Linux sont particulièrement versées à effectuer ces changements cosmétiques. Vous pouvez savoir si vous utilisez une version modifiée parce que celles qui sont packagées dans les dépôts des distributions s’installent généralement dans ``/usr/lib/libreoffice/``. En revanche, les installations faites directement depuis LibreOffice ou les téléchargements d’OpenOffice s’installent directement dans le répertoire ``/opt``.
<div style="attention">
Attention

Certaines sources de LibreOffice et d’Apache OpenOffice vous font payer une petite somme (par exemple si vous achetez une version sur CD-Rom). La licence leur permet de le faire, mais si vous payez plus de dix dollars pour l’expédition et le support, ils profitent de vous. LibreOffice et Apache OpenOffice sont toutes les deux  disponibles gratuitement pour toute personne disposant d’une connexion Internet.
</div>

<div style="astuce">
Astuce

Cet ouvrage traite de la mise en page. Il ne mentionne pas toutes les fonctionnalités disponibles. Si vous avez besoin d’informations sur des fonctionnalités ou des options qui ne figurent pas dans ce livre, consultez la page de documentation LibreOffice :

https://wiki.documentfoundation.org/Documentation/fr

</div>

## Qu’est-ce que le format Open Document ?

LibreOffice et Apache OpenOffice utilisent toutes deux le Format Open Document (ODF), un format de fichier constitué de fichiers XML compressés. 

Vous pouvez utiliser ce format pour échanger des documents entre ces deux logiciels ou avec toute autre application qui utilise ODF, comme Calligra Suite ou AbiWord. Dans LibreOffice et OpenOffice, vous pouvez également enregistrer des documents dans la plupart des formats Microsoft Office et ouvrir des fichiers à partir de WordPerfect, Microsoft Works et plusieurs autres formats, même si certaines opérations de formatage sont parfois perdues lorsqu’il s’agit de documents complexes.

## À propos de ce livre

Ce livre se base sur LibreOffice sous Linux, car c’est ce que j’utilise tous les jours. Cependant, la plupart des renseignements que vous y trouverez s’applique autant à Apache OpenOffice qu’à LibreOffice. De même, son contenu s’applique autant aux versions OS X et Windows qu’aux versions Linux.

Là où existent des différences importantes, nous essayerons de les mentionner, mais elles sont étonnamment peu nombreuses. Si vous voyez une différence non mentionnée, veuillez le faire savoir à l’auteur afin d’apporter des corrections.

## Présentation personnelle de l’auteur&nbsp;: Bruce Byfield

J’écris sur les logiciels libres et open source depuis plus d’une décennie. J’ai perdu le compte il y a longtemps, mais à ce jour j’ai écrit plus de 1700 articles. Une minorité non négligeable concerne OpenOffice.org ou LibreOffice.

 Parmi mes articles sur LibreOffice, on trouve :

- « 11 Tips for Moving to OpenOffice.org », qui fit la une du *Linux Journal* de mars 2004 (``http://www.linuxjournal.com/article/7158``). 
- « Replacing FrameMaker with OpenOffice.org Writer » (``http://archive09.linux.com/articles/39406``).
 - « How LibreOffice Writer Tops Microsoft Word » (``http://www.datamation.com/applications/how-libreoffice-writer-tops-ms-word-12-features-1.html``).

Avant de devenir journaliste, j’étais rédacteur technique et graphiste. Durant cette période de ma vie professionnelle, j’ai eu plusieurs occasions de tester OpenOffice.org, notamment en rédigeant des manuels de plus de 700 pages. Il a relevé tous les défis que je lui avais lancés, me faisant prendre conscience que le logiciel pouvait faire davantage de choses que ce qu’on s’imagine couramment.

LibreOffice n’est pas parfait. Certaines applications sont difficiles à utiliser et d’autres sont mal documentées. Quelques fonctions semblent souffrir de bugs de longue date et d’autres sont franchement obsolètes.

Cela dit, LibreOffice n’est pas simplement une copie de Microsoft Office. Il s’agit plutôt d’un logiciel puissant en soi, parfois en deçà de Microsoft Office, mais qui le dépasse tout aussi souvent. Il reste de loin la meilleure suite bureautique disponible aujourd’hui.

Certains utilisateurs se plaignent de la dépendance de LibreOffice vis-à-vis des styles et des modèles. Pourtant, sans eux, LibreOffice ne serait pas aussi puissant. Ils sont vraiment la clé de contact que la plupart d’entre nous a raté dans la descente.


