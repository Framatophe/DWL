# Conclusion

L’utilisation des styles, des modèles et des outils qui leur sont associés facilite la mise en forme et la rédaction d’un document. Cependant, la dernière vérification avant de terminer un document est également une partie importante du processus de composition.

Lorsque vous terminez la rédaction d’un document, même court, vous avez probablement perdu son sens global. Vous êtes si familier avec le document que vous ne remarquez pas ses manquements ou les oublis que vous auriez pu faire. Dans les cas extrêmes, il peut arriver que vous ne supportiez même plus sa vue (de nombreux professionnels considèrent cela comme le signe qu’il est presque prêt).

Pourtant, vous avez besoin de trouver le recul nécessaire pour pouvoir faire une relecture finale. Une solution consiste à faire relire votre document par une personne de confiance et qui s’intéresse à votre sujet, prête à prendre du temps pour vous faire des remarques honnêtes et constructives. Mais en réalité ces personnes sont rares. Vous devrez peut-être vous contenter de lire votre document à haute voix, le lire à partir d’une version imprimée ou de le laisser reposer pendant plusieurs jours. 

Commencez votre relecture systématiquement, en vous concentrant d’abord sur le contenu, puis sur la structure et la mise en forme. Les rubriques de ce chapitre vous donneront une liste de contrôle des étapes à suivre.

## Vérification du contenu

Posez-vous la question suivante&nbsp;: votre rédaction est-elle terminée&nbsp;? Y a-t-il des points de vue opposés ou non que vous pourriez inclure&nbsp;? Des contre-arguments auxquels vous pourriez répondre&nbsp;? Des termes, des hypothèses, des procédures ou des antécédents que vous avez omis&nbsp;?

### Longueur et clarté

Les paragraphes sont-ils de la bonne longueur&nbsp;? Si vous publiez en ligne, les paragraphes ne devraient pas dépasser quatre ou cinq phrases pour faciliter la compréhension. Dans la copie papier, les paragraphes peuvent être plus longs, mais seulement si vous avez besoin de la longueur supplémentaire pour un argument détaillé. Un paragraphe qui prend plus d’une demi-page devrait généralement être divisé, quel que soit le format de publication. Des paragraphes extrêmement volumineux peuvent faire sauter les lecteurs.

Vérifiez aussi la clarté de votre écriture. Le test ultime consiste à lire un passage à haute voix et à un rythme régulier et moyennement rapide. Si vous avez de la difficulté à lire le passage ou si un auditeur a de la difficulté à le comprendre, vous devrez peut-être reformuler ou raccourcir les phrases.

Au fur et à mesure que vous descendez au niveau des mots, faites attention au jargon et demandez-vous s’il est nécessaire ou si vous devriez définir davantage les termes. Est-ce qu’un mot exprime précisément ce que vous voulez dire, ou pourriez-vous en utiliser un meilleur&nbsp;? Utilisez-vous le bon champ lexical&nbsp;?
 
### Vos expressions préférées

Tous les écrivains ont leurs mots et expressions préférés. Au fur et à mesure que vous travaillez, dressez une liste des vôtres et surveillez-les&nbsp;: ils peuvent devenir des tics de langage extrêmement déplaisants. Envisagez des alternatives.

Vous pouvez également constater que vous êtes devenu fier de certaines expressions ou d’un passage en particulier. Certains écrivains croient que «&nbsp;le meilleur ami de l’écrivain est la poubelle&nbsp;» et suppriment tout ce dont ils sont trop fiers parce qu’ils ne peuvent pas être objectifs à ce sujet. C’est probablement une attitude extrême, mais vous devriez certainement envisager cette possibilité.

## Vérification de la structure

Comment se développe votre document&nbsp;? Si vous pensez que les lecteurs ne liront pas tout, insistez sur les points les plus importants, de sorte qu’ils soient au moins lus. Par contre, si vous pouvez compter sur les lecteurs pour une lecture exhaustive, vous pouvez mettre votre point le plus important en dernier, en organisant hiérarchiquement votre argumentaire. 

<div style="astuce">

Astuce

Si le document utilise des titres, sélectionnez ``Affichage > Navigateur`` ou appuyez sur la touche ``F5``. En utilisant les contrôles du Navigateur, vous pouvez déplacer les titres et les sections du texte n’importe où dans le document et beaucoup plus rapidement que vous ne pouvez copier et coller, tout en changeant automatiquement la structure. Ces modifications impliqueront certainement quelques reformulations, mais le Navigateur est un outil essentiel à la relecture.
</div>



La structure de votre document vous montre l’importance de choisir des titres précis. Plus les titres d’un document reflètent fidèlement son contenu, plus vous devriez voir facilement l’ordre dans lequel les passages devraient être présentés. Lorsque l’ordre vous semble clair, il y a de fortes chances qu’il soit logique pour les lecteurs aussi.

Encore un point&nbsp;: LibreOffice nomme automatiquement les tableaux, les images et autres éléments avec des noms peu imaginatifs comme Illustration 1 ou Table12. Un conseil de perfection serait de donner à chaque élément un nom distinctif pour aider à clarifier la structure du document. Par exemple, «&nbsp;Illustration 2&nbsp;: vue du pont sur le Rhin&nbsp;».

## Vérification de la mise en forme

Imprimez une copie ou utilisez ``Fichier > Aperçu avant impression``. Mieux encore, faites les deux. Peu importe le soin avec lequel vous avez développé et modifié votre modèle, il est toujours facile de manquer des problèmes dans la conception à moins que vous ne regardiez les pages dans leur ensemble —&nbsp;ou, mieux, en mode double pages. 

Parmi les choses que vous devriez vérifier&nbsp;:

- Les styles de page suivent l’ordre voulu.
- La numérotation des pages et les différents styles de numérotation commencent là où vous le vouliez.
- Les nouveaux chapitres commencent sur une page de droite.
- Les pieds de page et les en-têtes affichent les informations dans l’ordre prévu. Par exemple, les numéros de page seront plus visibles s’ils sont placés dans la marge extérieure de chaque page.
- Les styles de paragraphes pour les tables des matières ou les index, que vous n’avez peut-être créés qu’à la fin de votre travail, sont compatibles avec le reste de la mise en page.
- Lorsque des documents ont été ajoutés à un document maître ou collés ensemble, la numérotation de la première liste numérotée de chaque document peut nécessiter un redémarrage manuel. 

### Positionnement d’objets

Les graphiques, tableaux, illustrations et tout autre objet doivent être positionnés aussi près que possible du texte auquel ils se réfèrent.

Vérifiez également que le formatage, comme la structure des légendes et l’espace au-dessus et au-dessous des objets, est cohérent tout au long du texte. Les points chauds les plus courants sont les cas où un paragraphe avec un espacement en dessous est suivi d’un paragraphe avec un espacement au-dessus, ce qui cause un espacement plus grand que ce que vous aviez prévu.

Utilisez le Navigateur pour parcourir systématiquement les objets et reconnaître les écarts par rapport aux conventions de mise en forme que vous avez définies.


### Réglage des césures

Pour les césures, LibreOffice fait de son mieux pendant que vous travaillez. Mais l’édition des césures à la volée n’est pas le meilleur rendu de LibreOffice. Des améliorations sont toujours possibles.

Ainsi, lorsque vous effectuez la vérification finale du document, cliquez sur ``Outils > Langue > Coupure des mots``. Cet outil vous permet d’affiner au mieux la gestion des césures.

![Gérer les césures.](Captures/CH12-couperlesmots.png)

### Vérification orthographique

Par défaut, LibreOffice propose un correcteur orthographique dans le menu ``Outils`` pour chaque langue utilisée dans le document. LibreOffice vérifie également la grammaire et la typographie, par exemple en appliquant une espace insécable avant un double point.

![Correcteur de LibreOffice.](Captures/CH12-correcteurLoo.png)

L’extension Grammalecte (voir ``dicollecte.org``) propose d’étendre largement cette fonctionnalité en ajoutant un vérificateur orthographique, grammatical et typographique spécialement adapté pour la langue française. L’installation de cette extension est largement encouragée.

Cependant, n’oubliez jamais qu’une vérification orthographique ne remplace pas votre propre jugement. Ce que la vérification automatique peut faire, c’est réduire la corvée de la saisie des fautes de frappe et des erreurs de routine. Ne croyez surtout pas avoir identifié toutes les coquilles et autres erreurs grammaticales après avoir utilisé les outils de vérification. 

Pendant que vous vérifiez l’orthographe, prenez note de vos coquilles régulières qui ne sont pas identifiées par les correcteurs et utilisez ``Édition > Rechercher & Remplacer`` pour vous aider à corriger chaque occurrence. Trouver et remplacer les erreurs courantes est beaucoup plus rapide que la vérification orthographique et vous permet de voir davantage le contexte dans lequel l’erreur se produit.



![Utilisation de Grammalecte.](Captures/CH12-dicogrammalecte.png)


### Vérification des sauts de page

Réduisez le zoom à 75% ou moins, et voyez où les pages se terminent. À cause des graphiques, tableaux et autres objets, il est probable que vous ayez des pages qui se terminent à plus de 3 ou 4 lignes avant le bas de la page. Souvent, vous pouvez améliorer les sauts de page en réorganisant des éléments, ou en divisant les paragraphes. Peut-être déciderez-vous qu’un commentaire que vous avez omis vaut la peine d’être ajouté après tout, ou qu’un paragraphe peut être coupé, bien que changer le contenu uniquement pour améliorer la conception ne soit pas forcément judicieux.

Dans les cas extrêmes, vous pouvez ajouter des sauts de page manuels. Cependant, comme tout formatage manuel, plus vous en ajoutez, plus le document est difficile à maintenir.

De manière générale, essayez de ne pas avoir plus de quatre lignes vides en bas de page. 


### Actualisation

Sélectionnez ``Outils > Actualiser > Tout actualiser`` pour vous assurer que tous les champs sont à jour. Cette vérification est particulièrement importante pour les références croisées, qui peuvent avoir été repositionnées au fur et à mesure que vous les avez modifiées.

### Vérification finale

Avant de publier, réglez votre zoom sur 75% ou sélectionnez ``Fichier > Aperçu`` et jetez un dernier regard global sur le document. Vous ne devriez voir que très peu de problèmes. Si tel est le cas, essayez d’ajuster encore. Si vous l’avez déjà fait plusieurs fois, demandez-vous à un moment donné si vous bricolez dans l’espoir d’atteindre une perfection impossible.

Lorsque vous ne voyez plus rien à changer, vous savez que votre document est prêt à être imprimé ou mis en ligne. Attendez-vous à faire encore quelques ajustements car il existe toujours des situations que vous n’avez pas anticipées.

À ce stade, votre travail est terminé, tout comme ce livre (ou presque).



