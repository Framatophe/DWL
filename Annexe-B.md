# De la typographie

La version anglaise de l’ouvrage propose dans cette annexe une liste de références au sujet de la typographie anglaise. Pour cette version française, nous pouvons citer&nbsp;:

- *Lexique des règles typographiques en usage à l’Imprimerie nationale*, Paris&nbsp;: Imprimerie nationale, 2002.
- Jean-Pierre Lacroux, *Orthotypographie, Orthographe & Typographie françaises, Dictionnaire raisonné*, Paris&nbsp;: Quintette, 2008 (rééd.). Ressource disponible en ligne sur ``http://www.orthotypographie.fr``.
- Aurel Ramat et Anne-Marie Benoit, *Le Ramat de la typographie*, Montréal: Anne-Marie Benoit Éditrice, 2017. Site web&nbsp;: ``http://www.ramat.ca/``.
- *Code typographique&nbsp;: Choix de règles à l’usage des auteurs et professionnels du livre*, Paris&nbsp;: Syndicat national des cadres et maîtrises du livre, de la presse et des industries graphiques, 1993.
- Marc Augiey, Joseph Christe, Chantal Moraz, Roger Chatelain, *Guide du typographe*, Lausanne&nbsp;: Association suisse des typographes, 2015. Site web&nbsp;: ``http://www.arci.ch/2016/guide.html``.

